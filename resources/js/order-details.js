import Lang from './helpers/Lang';

document.addEventListener('DOMContentLoaded', _ => {
    const orderHistoryItems = document.querySelectorAll('.history__content');
    const orderDetailsPopup = document.querySelector('.modal__order-details');

    orderHistoryItems.forEach(item => {
        const orderData = JSON.parse(item.getAttribute('data-order-data'));

        item.addEventListener('click', _ => {
            orderDetailsPopup.classList.remove('hide');
            renderDetailsToPopup(orderDetailsPopup, orderData);
        });
    });
});

const renderDetailsToPopup = (popup, data) => {
    popup.querySelectorAll('.order-details__total-price-wrapper span')[1].innerText = `${data.price} ₸`;

    const details = data['details'];

    if (details) {
        const container = popup.querySelector('.order-details__items');

        container.innerHTML = '';

        details.forEach((item, index) => {
            const productData = item['product'];
            const variationData = item['product_variation'];

            container.innerHTML += `
                <div class="order-details__item">
                    <p class="order-details__product-name">${++index}. ${productData['name']} – ${variationData['name']} ${Lang.translate('kg')} х ${item['quantity']}</p>
                </div>
            `;
        });
    } else {
        console.warn('Order\'s details are not defined!');
    }
};