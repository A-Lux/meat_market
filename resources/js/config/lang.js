export default {
    'coming-soon': {
        'ru': 'Скоро',
        'en': 'Coming soon'
    },
    'not-in-stock': {
        'ru': 'Нет в наличии',
        'en': 'Not in stock'
    },
    'success': {
        'ru': 'Успех',
        'en': 'Success'
    },
    'error-happen': {
        'ru': 'Произошла ошибка',
        'en': 'Error happen'
    },
    'product-adding-to-cart-success': {
        'ru': 'Товар добавлен в корзину',
        'en': 'Product was added to cart'
    },
    'add-to-cart': {
        'ru': 'В корзину',
        'en': 'Add to cart'
    },
    'total': {
        'ru': 'Всего',
        'en': 'Total'
    },
    'cost': {
        'ru': 'Стоимость',
        'en': 'Cost'
    },
    'order-more': {
        'ru': 'Заказать ещё',
        'en': 'Order more'
    },
    'bonus': {
        'ru': 'Бонус',
        'en': 'Bonus'
    },
    'weight': {
        'ru': 'Вес',
        'en': 'Weight'
    },
    'kg': {
        'ru': 'кг',
        'en': 'kg'
    },
    'products': {
        'ru': 'Товаров',
        'en': 'Products'
    },
    'main-address': {
        'ru': 'Основной адрес',
        'en': 'Main address'
    },
    'searching': {
        'ru': 'Идёт поиск',
        'en': 'Searching'
    },
    'invalid-address': {
        'ru': 'Некорректный адрес',
        'en': 'Invalid address'
    },
    'not-specified-street': {
        'ru': 'Не указана улица!',
        'en': 'Not specified street!'
    },
    'not-specified-house-number': {
        'ru': 'Не указан номер дома!',
        'en': 'Not specified house number!'
    },
    'address-adding-failed': {
        'ru': 'Не удалось создать адрес!',
        'en': 'Adding of address was failed!'
    },
    'address-adding-success': {
        'ru': 'Адрес успешно создан!',
        'en': 'Address was created successfully!'
    },
    'address-is-not-included-to-delivery-zone': {
        'ru': 'Выбранный адрес не входит в зону доставки! Выберите другой адрес!',
        'en': 'Chosen address is not included to delivery zone! Choose another address!'
    },
    'must-specify-address': {
        'ru': 'Необходимо указать адрес доставки!',
        'en': 'Must specify delivery address!'
    },
    'in-developing': {
        'ru': 'Сайт в разработке',
        'en': 'Site in the developing'
    },
    'in-developing-message': {
        'ru': '<p style="margin-top: 15px">Уважаемые клиенты, обновленный сайт сейчас находится в тестовом ' +
            'режиме и <b style="font-size: 18px; color: #B41F21">вы можете заказывать продукцию</b> как и раньше предварительно ' +
            'согласовав все с кол центром, во избежания ошибок при заказе. В ближайшее ' +
            'время сайт будет доступен с полной автоматизацией заказа, что бы сэкономить ' +
            'ваше время. С уважением команда meatmarket.kz</p>',
        'en': '<p style="margin-top: 15px">Dear customers, the updated website is now in test mode and <b style="font-size: 18px; color: #B41F21">you can order products</b> ' +
            'as before, having previously agreed on everything with the call center, in order ' +
            'to avoid mistakes when ordering. In the near future, the site will be available with ' +
            'full order automation to save your time. Best regards meatmarket.kz team</p>'
    },
    'address-required': {
        'ru': 'Укажите адрес!',
        'en': 'Must specify address!'
    },
    'delivery-type-required': {
        'ru': 'Выберите тип доставки!',
        'en': 'Choose delivery type!'
    },
    'package-type-required': {
        'ru': 'Выберите тип упаковки!',
        'en': 'Choose package type!'
    },
    'delivery-person-required': {
        'ru': 'Выберите для кого доставка!',
        'en': 'Choose person for delivering!'
    },
    'delivery-time-required': {
        'ru': 'Выберите время доставки!',
        'en': 'Choose delivery time!'
    },
    'first-name-required': {
        'ru': 'Укажите имя!',
        'en': 'Specify first name!'
    },
    'last-name-required': {
        'ru': 'Укажите фамилию!',
        'en': 'Specify last name!'
    },
    'email-required': {
        'ru': 'Укажите E-mail!',
        'en': 'Specify E-mail!'
    },
    'phone-required': {
        'ru': 'Укажите номер телефона!',
        'en': 'Specify phone number!'
    },
    'delivery-date-required': {
        'ru': 'Выберите дату доставки!',
        'en': 'Specify delivery date!'
    }
};
