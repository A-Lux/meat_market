import axios from 'axios';
import Swal from 'sweetalert2';
import Lang from './helpers/Lang';

export const addProductToCart = async (productId, variationId, onSuccess = null, onError = null) => {
    const response = await axios.post('/basket/add', {
        id: productId,
        variation: variationId
    });
    console.log(response);
    if (response.status === 201) {
        addProductToCartOnSuccessHandlerBefore(Lang.translate('product-adding-to-cart-success'));

        let totalQuantity = 0;

        response.data.basket.forEach(item => {
            totalQuantity += item.quantity;
        });

        document.getElementById('main_total_basket').innerHTML = totalQuantity;

        if (onSuccess) {
            onSuccess();
        }
    } else {
        addProductToCartOnErrorHandlerBefore();

        if (onError) {
            onError();
        }
    }
};

const addProductToCartOnErrorHandlerBefore = message => {
    Swal.fire({
        icon: 'error',
        title: Lang.translate('error-happen'),
        text: message,
        toast: true,
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        position: 'top-start'
    }).then();
};

const addProductToCartOnSuccessHandlerBefore = message => {
    Swal.fire({
        icon: 'success',
        title: Lang.translate('success'),
        text: message,
        toast: true,
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        position: 'top-start'
    }).then();
};