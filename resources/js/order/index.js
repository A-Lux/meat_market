import axios from 'axios';
import Swal from 'sweetalert2';
import Lang from '../helpers/Lang';

let submitButton = document.getElementById('submit_btn');
let form = {
    delivery_type_id: null
};
let AuthCheck = document.querySelector('.AuthCheck');
let orderForm = document.getElementById('order_form');
let promocode__info = document.querySelector('.promocode__info');
let promocodeValue = document.querySelector('.promocodeValue');
let promocodeBtn = document.querySelector('.promocodeBtn');
let myDelivery = document.querySelector('.delivery-my');
let youDelivery = document.querySelector('.delivery-you');


let DeliveryInput = document.querySelectorAll('input[name="delivery-info"]');

let contactName = document.getElementById('name');
let contactLastname = document.getElementById('last_name');
let contactEmail = document.getElementById('email');
let contactPhone = document.getElementById('phone');
let contactDeliveryDate = document.getElementById('date_of_delivery');

const deliveryTimeInputs = document.querySelectorAll('input[name="delivery-time"]');

deliveryTimeInputs.forEach(node => {
    node.addEventListener('change', _ => {
        form['delivery-time'] = $('input[name="delivery-time"]:checked').val();
    });
});

DeliveryInput.forEach(node => {
    node.addEventListener('change', _ => {
        form['delivery-info'] = $('input[name="delivery-info"]:checked').val();
    });
});


if (myDelivery) {
    myDelivery.addEventListener('click', () => {
        axios.get('/home/user')
            .then(res => {
                contactName.value = res.data.user.name;
                contactLastname.value = res.data.user.last_name;
                contactEmail.value = res.data.user.email;
                contactPhone.value = res.data.user.phone;
                // contactDeliveryDate.value = res.data.user.birthday;
            })
            .finally(_ => {
                setTimeout(() => {

                }, 500);
            });
        form['delivery-info'] = $('input[name="delivery-info"]:checked').val();
    });
}

if (youDelivery) {
    youDelivery.addEventListener('click', () => {
        contactName.value = '';
        contactLastname.value = '';
        contactEmail.value = '';
        contactPhone.value = '';
        // contactDeliveryDate.value = '';

        form['delivery-info'] = $('input[name="delivery-info"]:checked').val();
    });
}

if (promocodeValue) {
    promocodeValue.addEventListener('input', e => {
        promocodeBtn.checked = false;
    });
}

if (promocodeBtn) {
    promocodeBtn.addEventListener('change', e => {
        e.preventDefault();
        if (e.target.checked == true) {
            axios({
                method: 'post',
                url: '/discount',
                data: {
                    code: promocodeValue.value
                }
            }).then(res => {
                promocode__info.innerText = res.data.discount_code.discount_points;
                Swal.fire({
                    icon: 'success',
                    text: 'Промокод успешно применен',
                    toast: true,
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    position: 'top-start',
                });
            }).catch(e => {
                Swal.fire({
                    icon: 'error',
                    text: 'Промокод не существует',
                    toast: true,
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    position: 'top-start',
                });
            });
        }
    });
}

let send_control = false;

if (submitButton) {
    let orderCreating = false;

    $("[id*='submit_btn']").on('click', function (e) {
        // e.preventDefault();

        if (send_control) {
            // alert("Udate1")
            Swal.fire({
                icon: 'error',
                title: Lang.translate('error-happen'),
                text: "Ожидайте идет оформление заказа",
                toast: true,
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                position: 'top-start',
            });
        } else {
            if (orderCreating || !checkOrderFormBeforeSending()) {
                return;
            }

            orderCreating = true;
            form.payment_type_id = document.querySelector('select[name="payment_type_id"]').value;
            // alert(form.payment_type_id);

            if (form.payment_type_id === '1') {
                pay(parseInt(totalSum.getAttribute('data-total')) + deliveryCost + packageCost - bonusesUsed);
            } else {
                saveOrder();
            }
        }
        return false;
    });

    const saveOrder = async _ => {

        if (!send_control) {
            send_control = true;
            for (let i = 0; i < orderForm.length; i++) {
                if (orderForm[i].getAttribute('name') === 'delivery_type_id') {
                    if (orderForm[i].checked) {
                        form[orderForm[i].getAttribute('name')] = orderForm[i].value;
                    }
                } else if (orderForm[i].getAttribute('name') === 'package_type_id') {
                    if (orderForm[i].checked) {
                        form[orderForm[i].getAttribute('name')] = orderForm[i].value;
                    }
                } else if (orderForm[i].getAttribute('name') === 'delivery_address_id') {
                    if (orderForm[i].checked) {
                        form[orderForm[i].getAttribute('name')] = orderForm[i].value;
                    }
                } else {
                    form[orderForm[i].getAttribute('name')] = orderForm[i].value;
                }
            }

            form['delivery-info'] = $('input[name="delivery-info"]:checked').val();
            form['delivery-time'] = $('input[name="delivery-time"]:checked').val();

            $(".modal__waite").removeClass("hide");

            await axios.post('/order', form)
                .then(_ => {
                    $(".modal__waite").addClass("hide");
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: 'Заказ успешно оформлен',
                        toast: true,
                        showConfirmButton: false,
                        timer: 1000,
                        timerProgressBar: true,
                        position: 'top-start',
                    }).then(() => {
                        if (AuthCheck) {
                            window.location.replace('/home/orders');
                        } else {
                            window.location.replace('/');
                        }
                    });
                })
                .catch(error => {
                    $(".modal__waite").addClass("hide");
                    send_control = false;
                    Swal.fire({
                        icon: 'error',
                        title: Lang.translate('error-happen'),
                        text: error.response.data.message,
                        toast: true,
                        showConfirmButton: false,
                        timer: 5000,
                        timerProgressBar: true,
                        position: 'top-start',
                    });
                })
                .finally(_ => {

                    orderCreating = false;
                    send_control = false;
                });
        }

    };


    const pay = amount => {
        const widget = new cp.CloudPayments();
        widget.pay('charge',
            {
                publicId: 'pk_ae3ce2932b248962864579015f118',
                description: 'Оплата товаров в meatmarket.kz',
                amount,
                currency: 'KZT',
                // invoiceId: '1234567',
                skin: "classic",
                // data: {
                //     myProp: 'myProp value'
                // }
            },
            {
                onSuccess: function (options) {
                    saveOrder().then();
                },
                onFail: function (reason, options) {
                    Swal.fire({
                        icon: 'error',
                        title: Lang.translate('error-happen'),
                        text: reason,
                        toast: true,
                        showConfirmButton: false,
                        timer: 5000,
                        timerProgressBar: true,
                        position: 'top-start',
                    });

                    orderCreating = false;
                }
            }
        );
    };

    const checkOrderFormBeforeSending = _ => {
        let error = null;

        if (!document.querySelector('input[name="address"]:checked')) {
            error = Lang.translate('address-required');
        }

        if (!form.delivery_type_id && !error) {
            error = Lang.translate('delivery-type-required');
        }

        if (!form.package_type_id && !error && !document.querySelector('input[name="is_free_package"]')) {
            error = Lang.translate('package-type-required');
        }

        if (!form['delivery-info'] && !error) {
            error = Lang.translate('delivery-person-required');
        }

        if (!form['delivery-time'] && !error) {
            error = Lang.translate('delivery-time-required');
        }

        if (!contactName.value && !error) {
            error = Lang.translate('first-name-required');
        }

        if (!contactLastname.value && !error) {
            error = Lang.translate('last-name-required');
        }

        if (!contactEmail.value && !error) {
            error = Lang.translate('email-required');
        }

        if (!contactPhone.value && !error) {
            error = Lang.translate('phone-required');
        }

        if (!contactDeliveryDate.value && !error) {
            error = Lang.translate('delivery-date-required');
        }

        if (error) {
            Swal.fire({
                icon: 'error',
                title: Lang.translate('error-happen'),
                text: error,
                toast: true,
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                position: 'top-start',
            });

            return false;
        }

        return true;
    };
}

const delivery = document.querySelectorAll('.delivery__click');
const priceTotalString = document.getElementById('basket__totals');

let deliveryCost = 0,
    packageCost = 0,
    bonusesUsed = 0;

form.delivery_price = 0;

delivery.forEach(delivery => {
    delivery.addEventListener('click', async function (e) {
        form.delivery_type_id = delivery.value;

        let deliveryName = document.getElementById('delivery-name');
        let deliveryPrice = document.querySelector('input[name="delivery_type_id"]:checked');

        form.delivery_price = parseInt(deliveryPrice.getAttribute('data-price'));

        deliveryName.innerHTML = 'Доставка';
        document.getElementById('delivery-price').innerHTML = `${delivery.getAttribute('data-price').toLocaleString()} тг`;

        delivery.setAttribute('clicked', true);

        deliveryCost = parseInt(delivery.getAttribute('data-price'));

        const priceTotal = parseInt(priceTotalString.getAttribute('data-total'))
            + deliveryCost + packageCost - bonusesUsed;
        priceTotalString.innerHTML = `${priceTotal.toString()} тг`;

        document.getElementById('map').setAttribute('style', 'display:block;height:480px;margin-top: 40px;');

        await showYandexMap(deliveryPrice.getAttribute('data-need-to-show-map'));
    });
});

export default async function showYandexMap(needToShowMap) {
    if (needToShowMap === '1') {
        const currentAddressHiddenInput = document.querySelector('input[name="address"]:checked');

        // Очередной костыль, без которого ничего не будет работать :(
        if (!currentAddressHiddenInput) {
            document.querySelectorAll('input[name="delivery_type_id"]').forEach(node => {
                if (node.getAttribute('data-need-to-show-map') === '0') {
                    node.setAttribute('checked', 'clicked');
                }
            });

            document.querySelector('input[data-need-to-show-map="0"]').removeAttribute('checked');

            // alert(Lang.translate('must-specify-address'));
            return;
        }

        const map = document.getElementById('basket__map');
        const inputNode = document.createElement('input');

        inputNode.setAttribute('id', 'area_of_delivery_id');
        inputNode.setAttribute('name', 'area_of_delivery_id');
        inputNode.setAttribute('type', 'hidden');
        map.appendChild(inputNode);

        ymaps.ready(_ => init());
    } else {
        document.getElementById('map').innerHTML = '';
        document.getElementById('map').setAttribute('style', 'display:none;');
    }
}

let packageType = document.querySelectorAll('.package__click');

packageType.forEach(node => {
    const additionCost = parseInt(node.getAttribute('data-addition-cost'));

    node.addEventListener('click', function () {
        form.package_type_id = node.value;
        packageCost = additionCost;

        let $packClick = $(this);
        let namePack = $packClick.data("name");
        let namePackPrice = $packClick.data("price");
        $("#packName").text(namePack);
        $("#packPrice").text(namePackPrice);
        console.log($packClick, namePack);
        const priceTotal = parseInt(priceTotalString.getAttribute('data-total'))
            + deliveryCost + packageCost - bonusesUsed;

        priceTotalString.innerHTML = `${priceTotal.toString()} тг`;
    });
});

const yandexMapGeoJson = JSON.parse(document.querySelector('input[name="yandex-map-geo-json"]')?.value || '[]');
const yandexMapCenterJson = JSON.parse(document.querySelector('input[name="yandex-map-center-json"]')?.value || '[]');

// Init Yandex map for general delivery type
function init() {
    const myMap = new ymaps.Map('map', {
        center: yandexMapCenterJson,
        zoom: 13,
        controls: []
    });

    var searchControl = new ymaps.control.SearchControl({
        options: {
            provider: 'yandex#search'
        }
    });

    myMap.controls.add(searchControl);
    // searchControl.search('Шоколадница');

    const deliveryZones = ymaps.geoQuery(yandexMapGeoJson).addToMap(myMap);

    const currentAddressHiddenInput = document.querySelector('input[name="address"]:checked');

    if (!currentAddressHiddenInput) {
        console.error('Не удалось получить currentAddressHiddenInput!');
        return;
    }

    const currentAddressCoords = JSON.parse(currentAddressHiddenInput.getAttribute('data-coords') || '[]');

    if (currentAddressCoords.length === 0) {
        console.error('Не удалось получить координаты выбранного адреса!');
        return;
    }

    yandexMapGeoJson.features.forEach(item => {
        const myGeoObject = new ymaps.GeoObject({
            geometry: item.geometry,
            properties: {
                balloonContent: item.properties.description
            }
        }, {
            fillColor: item.properties.fill,
            strokeColor: item.properties.stroke,
            fillOpacity: item.properties['fill-opacity'],
            strokeOpacity: item.properties['stroke-opacity'],
            strokeWidth: item.properties['stroke-width']
        });

        myMap.geoObjects.add(myGeoObject);
    });

    myMap.geoObjects.add(new ymaps.Placemark(currentAddressCoords, {}, {
        preset: 'islands#redDotIconWithCaption'
    }));

    const currentDeliveryZone = deliveryZones.searchContaining(currentAddressCoords).get(0);

    if (currentDeliveryZone === undefined) {
        // alert(Lang.translate('address-is-not-included-to-delivery-zone'));
    } else {
        const zoneDescription = currentDeliveryZone.properties.get('description');

        form.delivery_price = parseInt(zoneDescription.split(' ')[0]);
        deliveryCost = parseInt(zoneDescription.split(' ')[0]);

        document.getElementById('delivery-price').innerHTML = `${deliveryCost.toLocaleString()} тг`;

        const priceTotal = parseInt(priceTotalString.getAttribute('data-total'))
            + deliveryCost + packageCost - bonusesUsed;

        priceTotalString.innerHTML = `${priceTotal.toString()} тг`;
    }
}

const yandexMapNonAuthAddressContainer = document.querySelector('#yandex-map-non-auth-address-container');


if (yandexMapNonAuthAddressContainer) {
    ymaps.ready(_ => {
        initYandexMapNonAuthAddress(
            ymaps,
            'yandex-map-non-auth-address-container'
        );
    });
}


// Init Yandex map for adding address, if user is not authorized
const initYandexMapNonAuthAddress = (yandexMapApi, yandexMapContainerId) => {
    let placeMark = null;

    const streetHiddenInput = document.querySelector('input[name="street"].non-auth-address');
    const houseHiddenInput = document.querySelector('input[name="house"].non-auth-address');
    const addressHiddenInput = document.querySelector('input[name="address"]');

    if (!streetHiddenInput || !houseHiddenInput) {
        console.error('Failed initialing Yandex Map for non auth address!');
        return;
    }

    const yandexMapInstance = new yandexMapApi.Map(yandexMapContainerId, {
        center: Cookies.get('city_id') === '1' ? [76.889709, 43.238949] : [71.425134, 51.149605],
        zoom: 11,
        controls: ['zoomControl']
    });

    const generatePlaceMark = coords => {
        return new yandexMapApi.Placemark(coords, {
            iconCaption: `${Lang.translate('searching')} ...`
        }, {
            preset: 'islands#redDotIconWithCaption',
            draggable: true
        });
    };

    function search() {
        //

        // Подключаем поисковые подсказки к полю ввода.
        let suggest_box = new ymaps.SuggestView('suggest', {
            provider: {
                suggest: (function (request, options) {
                    return ymaps.suggest("Казахстан " + (Cookies.get('city_id') == '1' ? 'Алматы' : 'Нур-Султан') + ", " + request);
                })
            }
        });


        var
            mapyandex;


        function geocode() {
            // Забираем запрос из поля ввода.

            var request = "Казахстан " + (Cookies.get('city_id') == '1' ? 'Алматы' : 'Нур-Султан') + ", " + $('#suggest').val();
            // Геокодируем введённые данные.
            ymaps.geocode(request).then(function (res) {
                var obj = res.geoObjects.get(0),
                    error, hint;


                if (obj) {
                    // Об оценке точности ответа геокодера можно прочитать тут: https://tech.yandex.ru/maps/doc/geocoder/desc/reference/precision-docpage/
                    switch (obj.properties.get('metaDataProperty.GeocoderMetaData.precision')) {
                        case 'exact':
                            break;
                        case 'number':
                        case 'near':
                        case 'range':
                            error = 'Неточный адрес, требуется уточнение';
                            hint = 'Уточните номер дома';
                            break;
                        case 'street':
                            error = 'Неполный адрес, требуется уточнение';
                            hint = 'Уточните номер дома';
                            break;
                        case 'other':
                        default:
                            error = 'Неточный адрес, требуется уточнение';
                            hint = 'Уточните адрес';
                    }
                } else {
                    error = 'Адрес не найден';
                    hint = 'Уточните адрес';
                }
                // Если геокодер возвращает пустой массив или неточный результат, то показываем ошибку.
                if (error) {

                } else {


                    if (placeMark) {
                        placeMark.geometry.setCoordinates(obj["geometry"]["_coordinates"]);
                    } else {
                        placeMark = generatePlaceMark(obj["geometry"]["_coordinates"]);
                        yandexMapInstance.geoObjects.add(placeMark);

                        placeMark.events.add('dragend', function () {
                            setAddressToPlaceMark(placeMark.geometry.getCoordinates());
                        });
                    }

                    setAddressToPlaceMark(obj["geometry"]["_coordinates"]);

                }
            }, function (e) {

            })

        }


        suggest_box.events.add("select", function (e) {
            // console.log(e.get('item').value);
            var valsa = e.get('item').value.split(",");
            valsa.splice(0, 2);
            $("#suggest").val(valsa.join(","))

            geocode();
            if (suggest_box != null) {
                suggest_box.destroy();
                suggest_box = null;
            }
        })

// При клике по кнопке запускаем верификацию введёных данных.
        $('#suggest').on('keyup', function (e) {
            if (suggest_box == null) {
                suggest_box = new ymaps.SuggestView('suggest', {
                    provider: {
                        suggest: (function (request, options) {
                            return ymaps.suggest("Казахстан " + (Cookies.get('city_id') == '1' ? 'Алматы' : 'Нур-Султан') + ", " + request);
                        })
                    }
                });
                suggest_box.events.add("select", function (e) {
                    // console.log(e.get('item').value);
                    var valsa = e.get('item').value.split(",");
                    valsa.splice(0, 2);
                    $("#suggest").val(valsa.join(","))
                    geocode();
                    if (suggest_box != null) {
                        suggest_box.destroy();
                        suggest_box = null;
                    }
                })
            }
            geocode();

        });

    }

    const setAddressToPlaceMark = coords => {
        placeMark.properties.set('iconCaption', `${Lang.translate('searching')} ...`);

        streetHiddenInput.value = '';
        houseHiddenInput.value = '';
        addressHiddenInput.removeAttribute('checked');
        addressHiddenInput.setAttribute('data-coords', '[]');

        const hideYandexMapWithDeliveryZones = _ => {
            const map = document.querySelector('#map');

            if (map.style.display === 'block') {
                map.style.display = 'none';

                document.querySelectorAll('input[name="delivery_type_id"]').forEach(node => {
                    node.checked = false;
                });

                form.delivery_price = 0;
                deliveryCost = 0;
                form.delivery_type_id = '';

                const priceTotal = parseInt(priceTotalString.getAttribute('data-total'))
                    + deliveryCost + packageCost - bonusesUsed;

                priceTotalString.innerHTML = `${priceTotal.toString()} тг`;
            }
        };

        yandexMapApi.geocode(coords).then(function (res) {
            const geoObject = res.geoObjects.get(0);

            const house = geoObject.getPremiseNumber();
            const street = geoObject.getThoroughfare();

            if (!house) {
                // alert(Lang.translate('not-specified-house-number'));
                placeMark.properties.set('iconCaption', `${Lang.translate('invalid-address')}`);
                hideYandexMapWithDeliveryZones();
                return;
            }

            if (!street) {
                // alert(Lang.translate('not-specified-street'));
                placeMark.properties.set('iconCaption', `${Lang.translate('invalid-address')}`);
                hideYandexMapWithDeliveryZones();
                return;
            }

            placeMark.properties.set('iconCaption', street + ', ' + house);
            $("#suggest").val(street + ', ' + house);
            $('[name="house"]').val(house);
            streetHiddenInput.value = street;
            houseHiddenInput.value = house;

            addressHiddenInput.setAttribute('checked', 'checked');
            addressHiddenInput.setAttribute('data-coords', JSON.stringify(coords));

            if (document.querySelector('#map').style.display === 'block') {
                document.querySelector('#map').innerHTML = '';
                init();
            }
        });

    };

    yandexMapInstance.events.add('click', function (e) {
        const coords = e.get('coords');

        if (placeMark) {
            placeMark.geometry.setCoordinates(coords);
        } else {
            placeMark = generatePlaceMark(coords);
            yandexMapInstance.geoObjects.add(placeMark);

            placeMark.events.add('dragend', function () {
                setAddressToPlaceMark(placeMark.geometry.getCoordinates());
            });
        }

        setAddressToPlaceMark(placeMark.geometry.getCoordinates());
    });

    search();

};

let totalSum = document.getElementById('basket__totals');
let useBonusButton = document.getElementById('use_bonus');

if (useBonusButton) {
    useBonusButton.addEventListener('click', function () {
        const total = parseInt(totalSum.getAttribute('data-total')) + deliveryCost + packageCost - bonusesUsed;
        let used = useBonusButton.getAttribute('data-used'),
            mainTotal = 0;

        if (used === 'false') {
            mainTotal = total - parseInt(useBonusButton.getAttribute('data-bonus'));

            bonusesUsed = parseInt(useBonusButton.getAttribute('data-bonus'));

            form.use_bonus = 1;
            useBonusButton.setAttribute('data-used', 'true');
            totalSum.innerHTML = `${mainTotal} ₸`;
        } else {
            mainTotal = total + parseInt(useBonusButton.getAttribute('data-bonus'));

            bonusesUsed = 0;

            delete form.use_bonus;

            useBonusButton.setAttribute('data-used', 'false');
        }

        totalSum.innerHTML = `${mainTotal} ₸`;
    });
}
