import langConfig from '../config/lang';

class Lang {
    static getLocale() {
        return document.documentElement.getAttribute('lang');
    }

    static translate(key) {
        if (!langConfig[key]) {
            return null;
        }

        if (!langConfig[key][Lang.getLocale()]) {
            return null;
        }

        return langConfig[key][Lang.getLocale()];
    }
}

export default Lang;
