import Lang from './helpers/Lang';

export const redrawProductCard = (node, data) => {
    // Redrawing visible content
    node.querySelector('.card__title').innerText = data.name;

    node.querySelector('.card__mid').innerHTML = `
        <span class="card__cost">${data.variations[0].price.toLocaleString()} ₸</span>
        <span class="card__weight">${data.variations[0].name} ${Lang.translate('kg')}</span>
        <span class="card__bonus">${data.variations[0].bonus != null ? data.variations[0].bonus + ' ₸' : ''}</span>
    `;

    // Redrawing attributes
    node.querySelector('.linked_wrapper').setAttribute('href', `/product/${data.id}`);
    node.querySelector('.card__img img').setAttribute('src', `/storage/${data.main_image}`);
    node.querySelector('.card__mid').setAttribute('id', `product-price-table-${data.id}`);
    node.querySelector('.card__favorite span').setAttribute('data-product', data.id);
    node.querySelector('.card__selects').setAttribute('id', `product-${data.id}`);
    node.querySelector('.card__add-basket').setAttribute('id', `product_add_button-${data.id}`);
    node.querySelector('.card__add-basket').setAttribute('data-product', data.id);
    node.querySelector('.card__add-basket').setAttribute('data-variation', data.variations[0].id);
    node.querySelector('.card__top-linked-wrapper').setAttribute('href', `/product/${data.id}`);

    // Redrawing variation switchers
    document.getElementById(`product-${data.id}`).innerHTML = '';

    data.variations.forEach((variation, index) => {
        if (index < 3) {
            document.getElementById(`product-${data.id}`).innerHTML += `
                <button class="card__select"
                        data-variation='${JSON.stringify(variation)}'
                        data-variation-id="${variation.id}"
                        data-product="${data.id}">${variation.name} кг</button>
            `;
        }
    });
};

export const redrawProductCard_v2 = (productNode, isFrozen = false) => {
    let productData = productNode.getAttribute('data-product');

    if (!productData) {
        console.error('Product data retrieving from data attribute was failed!');
        return;
    }

    productData = JSON.parse(productData);

    if (isFrozen) {
        if (!productData['pair']) {
            console.error('Was specified frozen mode, but product\'s pair was not represented!');
            return;
        }

        productData = productData['pair'];
    }

    productNode.querySelector('.card__title').innerText = productData.name.replace(/охл|зам/gi, '');

    if (productData.variations.length > 0) {
        productNode.querySelector('.card__mid').style.display = 'flex';
        productNode.querySelector('.card__mid').innerHTML = `
            <span class="card__cost">${productData.variations[0].price.toLocaleString()} ₸</span>
            <span class="card__weight">${productData.variations[0].name} кг</span>
            <span class="card__bonus">${productData.variations[0].bonus != null ? productData.variations[0].bonus + ' ₸' : ''}</span>
        `;
    } else {
        productNode.querySelector('.card__mid').style.display = 'none';
    }


    // Redrawing attributes
    productNode.querySelector('.linked_wrapper').setAttribute('href', `/product/${productData.id}`);
    productNode.querySelector('.card__img img').setAttribute('src', `/storage/${productData.main_image}`);
    productNode.querySelector('.card__mid').setAttribute('id', `product-price-table-${productData.id}`);
    productNode.querySelector('.card__favorite span').setAttribute('data-product', productData.id);
    productNode.querySelector('.card__selects').setAttribute('id', `product-${productData.id}`);
    productNode.querySelector('.card__top-linked-wrapper').setAttribute('href', `/product/${productData.id}`);

    // Redrawing add to card button
    if (productData.variations.length > 0) {
        productNode.querySelector('.card__add-basket').style.display = 'block';
        productNode.querySelector('.card__add-basket').setAttribute('id', `product_add_button-${productData.id}`);
        productNode.querySelector('.card__add-basket').setAttribute('data-product', productData.id);
        productNode.querySelector('.card__add-basket').setAttribute('data-variation', productData.variations[0].id);

        productNode.querySelector('.not-in-stock').style.display = 'none';
    } else {
        productNode.querySelector('.card__add-basket').style.display = 'none';
        productNode.querySelector('.not-in-stock').style.display = 'block';
    }

    // Redrawing variation switchers
    productNode.querySelector('.card__selects').innerHTML = '';

    productData.variations.forEach((variation, index) => {
        if (index < 3) {
            productNode.querySelector('.card__selects').innerHTML += `
                <button class="card__select ${index === 0 ? 'active' : ''}"
                        data-variation='${JSON.stringify(variation)}'
                        data-variation-id="${variation.id}"
                        data-product="${productData.id}">${variation.name} кг</button>
            `;
        }
    });
};
