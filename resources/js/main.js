'use strict';
import axios from 'axios';
import Swiper from 'swiper/bundle';
import Swal from 'sweetalert2';
import Cookies from 'js-cookie';
import Inputmask from 'inputmask/lib/inputmask';
import Lang from './helpers/Lang';
import addToBasketEvent, {clickButtonSelect, favoriteClick, initSwitching} from './product/main';

let cowSlider = null;

if (!localStorage.getItem('warning_closed') && false) {
    Swal.fire({
        icon: 'warning',
        title: Lang.translate('in-developing'),
        html: Lang.translate('in-developing-message')
    });
    localStorage.setItem('warning_closed', '1');
}

document.addEventListener('DOMContentLoaded', () => {
    const openLoginPopupButton = document.querySelector('.userModal');

    if (openLoginPopupButton) {
        openLoginPopupButton.addEventListener('click', _ => {
            document.querySelector('.modal__login').classList.remove('hide');
            document.querySelector('.modal__login').classList.add('show');
        });

        if (document.querySelector('.modal__login .close')) {
            document.querySelector('.modal__login .close').addEventListener('click', _ => {
                document.querySelector('.modal__login').classList.add('hide');
                document.querySelector('.modal__login').classList.remove('show');
            });
        }
    }

    document.querySelectorAll('.modal__tabs-two').forEach(item => {
        item.addEventListener('click', _ => {
            document.querySelector('.modal__request').classList.add('show');
            document.querySelector('.modal__request').classList.remove('hide');
            document.querySelector('.modal__sentence').classList.add('hide');
            document.querySelector('.modal__sentence').classList.remove('show');
            document.querySelector('.modal__tabs-one').classList.remove('modal__tabs-click');
            document.querySelector('.modal__tabs-two').classList.add('modal__tabs-click');
        });
    });

    document.querySelectorAll('.modal__tabs-one').forEach(item => {
        item.addEventListener('click', _ => {
            document.querySelector('.modal__request').classList.add('hide');
            document.querySelector('.modal__request').classList.remove('show');
            document.querySelector('.modal__sentence').classList.add('show');
            document.querySelector('.modal__sentence').classList.remove('hide');
            document.querySelector('.modal__tabs-two').classList.remove('modal__tabs-click');
            document.querySelector('.modal__tabs-one').classList.add('modal__tabs-click');
        });
    });

    // Input masks initializing
    const phoneInputs = document.querySelectorAll('input[name="phone"]');

    addToBasketEvent();
    initSwitching();

    phoneInputs.forEach(node => {
        Inputmask({
            'mask': '+7 (999) 999 9999'
        }).mask(node);
    });

    // Выход из аккаунта
    const logoutButton = document.querySelector('.logout__button');

    if (logoutButton) {
        logoutButton.addEventListener('click', _ => {
            Cookies.remove('laravel_session', {path: '', domain: location.hostname});
        });
    }

    //Табы на внутренней странице товара
    const tab = document.querySelectorAll('.gen__tab'),
        tabs = document.querySelector('.gen__tabs'),
        tabContent = document.querySelectorAll('.tabcontent');

    const search = document.querySelector('.icon-search');
    const searchInput = document.querySelector('.search-input');
    const popup_search = document.querySelector('.popup_search');

    searchInput.classList.add('hide_search');

    if (search) {
        search.addEventListener('click', () => {
            if (searchInput.classList.contains('hide_search')) {
                searchInput.classList.add('show_search');
                searchInput.classList.remove('hide_search');
            } else {
                searchInput.classList.add('hide_search');
                searchInput.classList.remove('show_search');
            }
            if (popup_search.classList.contains('hide')) {
                // popup_search.classList.add('show')
                // popup_search.classList.remove('hide')
            } else {
                popup_search.classList.add('hide');
                popup_search.classList.remove('show');
            }
        });
    }

    const subscribe = document.querySelector('.subscribe');
    if (subscribe) {
        subscribe.addEventListener('click', () => {
            Swal.fire({
                icon: 'warning',
                title: Lang.translate('coming-soon')
            });
        });
    }

    if (searchInput) {
        searchInput.addEventListener('keyup', e => {
            if (e.keyCode === 13) {
                if (popup_search.classList.contains('hide')) {
                    popup_search.classList.add('show');
                    popup_search.classList.remove('hide');

                    axios({
                        method: 'POST',
                        url: '/catalog',
                        data: {
                            sort_type: 'price',
                            sort_value: 'desc',
                            search: searchInput.value
                        }
                    }).then(res => {
                        renderBasketPage(res.data.products);
                    });
                } else {
                    popup_search.classList.add('hide');
                    popup_search.classList.remove('show');
                }
            }
        });
    }

    const hideTabContent = () => {
        // tabContent.forEach(item => {
        //     item.style.display = 'none';
        // });
        //
        // tab.forEach(item => {
        //     item.classList.remove('gen__tab-active');
        // });
    };

    const showTabContent = (i = 0) => {
        if (showTabContent[i] != undefined) {
            tabContent[i].style.display = 'block';
            tab[i].classList.add('gen__tab-active');
        }
    };

    hideTabContent();
    showTabContent();
    if (tabs != undefined) {
        tabs.addEventListener('click', (event) => {
            const target = event.target;

            if (target && target.classList.contains('.gen__tab')) {
                tab.forEach((item, i) => {
                    if (target == item) {
                        hideTabContent();
                        showTabContent(i);
                    }
                });
            }
        });
    }

    // Табы на главной
    const bundlesName = document.querySelectorAll('.bundles__name'),
        bundlesTabs = document.querySelector('.bundles__tabs'),
        bundlesTab = document.querySelectorAll('.bundles__tab'),
        bundleContent = document.querySelectorAll('.bundles__info'),
        bundleBg = document.querySelector('.bundles__bg'),
        setsInfo = JSON.parse(bundleBg?.getAttribute('data-sets') || '[]');

    const hideBundleContent = () => {
        bundleContent.forEach(item => {
            item.style.display = 'none';
        });

        bundlesTab.forEach(item => {
            item.classList.remove('tab-active');
        });
    };

    const showBundleContent = (i = 0) => {
        if (bundleContent[i]) {
            bundleContent[i].style.display = 'block';
            bundlesTab[i].classList.add('tab-active');
            bundleBg.style.backgroundImage = `url(/storage/${setsInfo[i].image || ''})`;
        }
    };

    hideBundleContent();
    showBundleContent();

    if (bundlesTabs) {
        bundlesTabs.addEventListener('click', (event) => {
            const target = event.target;

            if (target && target.classList.contains('bundles__name')) {
                bundlesName.forEach((item, i) => {
                    if (target == item) {
                        hideBundleContent();
                        showBundleContent(i);
                    }
                });
            }
        });
    }

    // Слайдер на главной странице
    new Swiper('.main-slider', {
        direction: 'horizontal',
        slidesPerView: 1,
        breakpoints: {
            576: {
                slidesPerView: 1.5
            }
        },
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    });

    const adviceSlider = new Swiper('.advice-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 1.5,
        // centeredSlides: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.advice-next',
            prevEl: '.swiper-button-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    });

    // Tips and Recipes Slider
    new Swiper('.recipe .swiper-container', {
        autoplay: true,
        speed: 3000,
        loop: true,
        slidesPerView: 'auto',
        centeredSlides: true,
        spaceBetween: 30
    });

    // Similar products slider
    new Swiper('.similar .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 30,
        breakpoints: {
            640: {
                slidesPerView: 3.5
            }
        }
    });

    // Рейтинг


    // Модальное окно при создании кнопки
    const open = document.querySelectorAll('[data-modal]'),
        close = document.querySelectorAll('[data-close]'),
        modal = document.querySelector('.modal'),
        register = document.querySelector('.modal__register'),
        modalRegister = document.querySelector('.modal-register'),
        modalLogin = document.querySelector('.login-modal'),
        modalFeedback = document.querySelector('.modalFeedback'),
        modalSentence = document.querySelector('.modalSentence'),
        modalRequest = document.querySelector('.modalRequest'),
        forgetPas = document.querySelector('.password__forget'),
        modalForget = document.querySelector('.modal-forget_password'),
        modal__feedback = document.querySelector('.modal__feedback'),
        modal__sentence = document.querySelector('.modal__sentence'),
        modal__request = document.querySelector('.modal__request'),
        modalJoin = document.querySelector('.referal__more');

    function openModal() {
        modalRegister.classList.add('show');
        modalRegister.classList.remove('hide');
        document.body.style.overflow = 'hidden';
    }

    if (location.hash === '#login') {
        window.history.replaceState({}, '', '/');
        openModal();
    }

    function closeModal() {
        modalRegister.classList.add('hide');
        modalRegister.classList.remove('show');
        document.body.style.overflow = '';
    }

    open.forEach(btn => {
        btn.addEventListener('click', openModal);
    });

    close.forEach(btn => {
        btn.addEventListener('click', () => {
            closeModal();
            modalRegister.classList.add('hide');
            modalRegister.classList.remove('show');
            modalForget.classList.remove('show');
            modalForget.classList.add('hide');
            modal__feedback.classList.remove('show');
            modal__feedback.classList.add('hide');
            modal__sentence.classList.remove('show');
            modal__sentence.classList.add('hide');
            modal__request.classList.remove('show');
            modal__request.classList.add('hide');
            document.body.style.overflow = '';
            if (boxTaskModal) {
                boxTaskModal.classList.remove('show');
                boxTaskModal.classList.add('hide');
            }
        });
    });


    modal.addEventListener('mousedown', (event) => {
        if (event.target === modal || event.target.classList.contains('close')) {
            closeModal();
            modalRegister.classList.remove('show');
            modalRegister.classList.add('hide');
            modalForget.classList.remove('show');
            modalForget.classList.add('hide');
        }
    });

    document.addEventListener('keydown', (event) => {
        if (event.code === 'Escape' && modal.classList.contains('show')) {
            closeModal();
        }
    });

    document.addEventListener('keydown', (event) => {
        if (event.code === 'Escape' && modalRegister.classList.contains('show')) {
            modalRegister.classList.remove('show');
            modalRegister.classList.add('hide');
            document.body.style.overflow = '';
        }
    });

    register.addEventListener('click', () => {
        closeModal();
        modalRegister.classList.add('show');
        modalRegister.classList.remove('hide');
        document.body.style.overflow = 'hidden';
    });

    modalLogin.addEventListener('click', () => {
        document.querySelector('.modal__login').classList.remove('hide');
    });

    if (modalJoin) {
        modalJoin.addEventListener('click', () => {
            openModal();
            modalRegister.classList.add('hide');
            modalRegister.classList.remove('show');
            // document.body.style.overflow = 'hidden';
        });
    }

    modalFeedback.addEventListener('click', () => {
        closeModal();
        modal__feedback.classList.remove('hide');
        modal__feedback.classList.add('show');
        document.body.style.overflow = 'hidden';
    });

    modalSentence.addEventListener('click', () => {
        closeModal();
        modal__sentence.classList.remove('hide');
        modal__sentence.classList.add('show');
        document.body.style.overflow = 'hidden';
    });

    // modalRequest.addEventListener('click', () => {
    //     closeModal();
    //     modal__request.classList.remove('hide');
    //     modal__request.classList.add('show');
    //     document.body.style.overflow = 'hidden';
    // });

    const showResetPasswordModal = _ => {
        closeModal();
        modalForget.classList.add('show');
        modalForget.classList.remove('hide');
        document.body.style.overflow = 'hidden';
    };

    forgetPas.addEventListener('click', showResetPasswordModal);

    if (location.hash === '#reset-password-modal') {
        showResetPasswordModal();
        window.history.replaceState({}, '', '/');
    }

    // Клик на корзину
    const basket = document.querySelector('.nav__cart'),
        popup = document.querySelector('.popup');

    basket.addEventListener('click', () => {
        if (popup.classList.contains('hide')) {
            popup.classList.add('show');
            popup.classList.remove('hide');
        } else {
            popup.classList.add('hide');
            popup.classList.remove('show');
        }
    });

    document.addEventListener('click', (e) => {
        const target = e.target;
        const itspopup = target == popup || popup.contains(target);
        const itsbasket = target == basket;
        const menuisactive = popup.classList.contains('show');

        if (!itspopup && !itsbasket && menuisactive) {
            popup.classList.add('hide');
            popup.classList.remove('show');
        }
    });

    // Выделение реферальной ссылки

    const btnRef = document.querySelector('.ref-btn'),
        input = document.getElementById('ref-url'),
        tooltip = document.querySelector('.tooltiptext');

    if (btnRef) {
        btnRef.addEventListener('click', selectText);

    }
    if (input) {
        input.addEventListener('focus', selectText);

    }
    if (btnRef) {
        btnRef.addEventListener('mouseout', outFunc);

    }

    function selectText() {
        input.focus();
        input.select();
        document.execCommand('copy');
        tooltip.innerHTML = 'Скопировано';
    }

    function outFunc() {
        tooltip.innerHTML = 'Скопировать';
    }

    // Модалки в личном кабинете

    const addReview = document.querySelector('.order__rev-add');
    if (addReview) {
        addReview.addEventListener('click', openModal);
    }

    // Модалка соц сети

    const socialTask = document.querySelector('#social-task');
    if (socialTask) {
        socialTask.addEventListener('click', () => {
            openModal();
        });
    }

    //Модалка для соц сети (задание)

    const socBtn = document.querySelector('#social-task');
    if (socBtn) {
        socBtn.addEventListener('click', openModal);
    }

    // Модалка отдать коробку

    const boxTaskBtn = document.querySelector('#social-task');
    const boxTaskModal = document.querySelector('.modal__box-task');
    const boxCloseModal = document.querySelector('.modal__close-btn');
    if (boxTaskBtn) {
        boxTaskBtn.addEventListener('click', () => {
            if (boxTaskModal) {
                boxTaskModal.classList.add('show');
                boxTaskModal.classList.remove('hide');
            }
            document.body.style.overflow = 'hidden';
        });
    }
    if (boxCloseModal) {
        boxCloseModal.addEventListener('click', () => {
            if (boxTaskModal) {
                boxTaskModal.classList.add('hide');
                boxTaskModal.classList.remove('show');
            }
            document.body.style.overflow = '';
        });
    }

    // Бургер
    const burger = document.querySelector('.hamburger-box'),
        mobileMenu = document.querySelector('.mobile__menu');
    burger.addEventListener('click', () => {
        mobileMenu.classList.toggle('open');
        document.querySelector('.hamburger--slider').classList.toggle('is-active');
        document.body.style.overflow = 'hidden';
        if (!document.querySelector('.hamburger--slider.is-active')) {
            document.body.style.overflow = '';
        }
    });

    // // Кнопка на верх
    // const goTop = document.querySelector('.footer__up');
    //
    // goTop.addEventListener('click', backToTop);
    //
    // function backToTop() {
    //     const scrollStep = window.pageYOffset / 40;
    //
    //     console.log(scrollStep);
    //
    //     if (window.pageYOffset > 0) {
    //         window.scrollBy(0, -scrollStep);
    //         setTimeout(backToTop, 0);
    //     }
    // }
});

function renderBasketPage(data) {
    "use strict";
    let basketRow = document.querySelector('.popup_search__items');
    basketRow.innerHTML = '';
    // undefined !== a && a.length
    // data.length === 0
    if (data.length == 0 || typeof data == "undefined") {
        basketRow.innerHTML = 'Не найдено';
    } else {
        data.forEach(item => {

            basketRow.innerHTML += `
                <a href="/product/${item.id}" class="basket__column">
                    <div class="basket__item basket-card" style=" flex-direction: row;padding-bottom: 45px; ">
                        <div class="basket-card__img">
                            <img src="/storage/${item.main_image}" alt="">
                        </div>
                        <div class="basket-card__content">
                            <h2 class="basket-card__title">${item.name}</h2>
                            <div class="basket-card__cost">
                                <p class="basket-card__link">Перейти к товару</p>
                            </div>
                            <div class="basket-card__sale"></div>
                        </div>
                    </div>
                </a>
            `;
        });
    }

    eventRemoveClose();
    eventAddButton();
}


let offer = document.querySelector('.offer');
let offer__name = document.querySelector('.offer__name');
let offer__email = document.querySelector('.offer__email');
let offer__comment = document.querySelector('.offer__comment');

let application = document.querySelector('.application');
let application__name = document.querySelector('.application__name');
let application__email = document.querySelector('.application__email');
let application__comment = document.querySelector('.application__comment');

let feedback = document.querySelector('.feedback');
let feedback__name = document.querySelector('.feedback__name');
let feedback__email = document.querySelector('.feedback__email');
let feedback__comment = document.querySelector('.feedback__comment');

let modal__feedback = document.querySelector('.modal__feedback');
let modal__sentence = document.querySelector('.modal__sentence');
let modal__request = document.querySelector('.modal__request');


if (offer) {
    offer.addEventListener('submit', e => {
        e.preventDefault();
        axios({
            method: 'POST',
            url: '/offer',
            data: {
                name: offer__name.value,
                email: offer__email.value,
                comment: offer__comment.value,
            }
        })
            .then(res => {
                modal__feedback.classList.remove('show');
                modal__feedback.classList.add('hide');
                document.body.style.overflow = 'hidden';
                Swal.fire({
                    icon: 'success'
                });
            });
    });
}

if (application) {
    application.addEventListener('submit', e => {
        e.preventDefault();
        axios({
            method: 'POST',
            url: '/application',
            data: {
                name: application__name.value,
                email: application__email.value,
                comment: application__comment.value,
            }
        })
            .then(res => {
                modal__sentence.classList.remove('show');
                modal__sentence.classList.add('hide');
                document.body.style.overflow = 'hidden';
                Swal.fire({
                    icon: 'success'
                });
            });
    });
}

if (feedback) {
    feedback.addEventListener('submit', e => {
        e.preventDefault();
        axios({
            method: 'POST',
            url: '/feedback',
            data: {
                name: feedback__name.value,
                email: feedback__email.value,
                comment: feedback__comment.value,
            }
        })
            .then(res => {
                modal__request.classList.remove('show');
                modal__request.classList.add('hide');
                document.body.style.overflow = 'hidden';
                Swal.fire({
                    icon: 'success'
                });
            });
    });
}

let cowPartAll = document.querySelectorAll('.cowPart');
let main_panel = document.getElementById('main_category_panel');

let productsFromCaw;

cowPartAll.forEach(item => {
    const onCowPartClick = _ => {
        main_panel.innerHTML = '';

        axios.get(`/api/product/by-caw-part/${item.getAttribute('data-id')}`).then(res => {
            productsFromCaw = res.data;

            productsFromCaw.forEach((product, index) => {
                // if (product.variations.length === 0) {
                //     return;
                // }

                let switcher = '';

                if (product.pair) {
                    switcher = `
                        <label class="switcher">
                            <input type="checkbox" name="is_frozen" data-product-index="${index}">
                            <span class="switcher-slide"></span>
                        </label>
                    `;
                }

                let primeIcon = '';

                if (product.tag !== 'none') {
                    primeIcon = `<img src="/img/${product.tag}_icon.png" alt="">`;
                }

                main_panel.innerHTML += `
                    <div class="category__column swiper-slide" style="height: auto !important;">
                        <div class="card card_category" data-product-id="${product.id}" data-product='${JSON.stringify(product)}'>
                            <a class="linked_wrapper" href="/product/${product.id}">
                                <div class="card__img">
                                    <img src="/storage/${product.main_image}" alt="Meat Market" title="Meat Market">
                                    <div class="card__brand">
                                        ${primeIcon}
                                    </div>
                                </div>
                            </a>

                            <div class="card__content">
                                <div class="card__top">
                                    <a class="card__top-linked-wrapper" href="/product/${product.id}">
                                        <h3 class="card__title">${product.name.replace(/охл|зам/gi, '')}</h3>
                                    </a>
                                    ${switcher}
                                    <div class="card__favorite"><span class="icon-star" data-product="${product.id}"></span></div>
                                </div>

                                <div class="card__mid" id="product-price-table-${product.id}"
                                    style="display: ${product.variations.length === 0 ? 'none' : 'flex'}">
                                    <span class="card__cost">${product.variations[0]?.price?.toLocaleString() || ''} ₸</span>
                                    <span class="card__weight">${product.variations[0]?.name || ''} кг</span>
                                    <span class="card__bonus">${product.variations[0]?.bonus != null ? product.variations[0]?.bonus + ' ₸' : ''}</span>
                               </div>
                               <div class="card__selects" id="product-selects-${product.id}"></div>
                               <div class="card__add-basket btn-basket"
                                    id="product_add_button-${product.id}"
                                    data-product="${product.id}"
                                    data-variation="${product.variations[0]?.id}"
                                    style="display: ${product.variations.length === 0 ? 'none' : 'block'}">
                                    <span class="icon-shopping-bag"></span>В корзину</div>
                               <div class="btn-basket not-in-stock" style="display: ${product.variations.length > 0 ? 'none' : 'block'}">
                                    ${Lang.translate('not-in-stock')}</div>

                            </div>
                        </div>
                    </div>
                `;

                // product.variations.forEach(variation => {
                //     document.getElementById(`product-selects-${product.id}`).innerHTML += `
                //         <button class="card__select"
                //                 data-product-card-index="${index}"
                //                 data-variation='${JSON.stringify(variation)}'
                //                 data-variation-id="${variation.id}"
                //                 data-product="${product.id}">${variation.name} кг
                //         </button>
                //     `;
                // });

                if (product.variations.length > 0) {
                    product.variations.forEach(variation => {
                        document.getElementById(`product-selects-${product.id}`).innerHTML += `
                            <button class="card__select"
                                    data-product-card-index="${index}"
                                    data-variation='${JSON.stringify(variation)}'
                                    data-variation-id="${variation.id}"
                                    data-product="${product.id}">${variation.name} кг
                            </button>
                        `;
                    });
                }
            });

            clickButtonSelect();
            favoriteClick();
            initSwitching();
            addToBasketEvent(replaceOnNestedVariationForCawParts);

            new Swiper('.main_category_panel__slider', {
                slidesPerView: 1,
                breakpoints: {
                    640: {
                        slidesPerView: 3
                    }
                }
            });
        });
    };

    // item.removeEventListener('click', onCowPartClick);
    item.addEventListener('click', _ => {
        onCowPartClick();
    });
});

const replaceOnNestedVariationForCawParts = async (productId, variationId) => {
    const response = await axios.get(`${location.origin}/api/product/nested-variation?product_id=${productId}`);
    let hasNested = false;

    if (response.status === 200) {
        const variationSelectButton = document.querySelector(`.card__select[data-variation-id="${variationId}"]`);

        let addToCartButton = document.querySelector(`.main_category_panel__slider #product_add_button-${productId}`);

        const productContentContainerNode = addToCartButton.parentNode;

        if (response.data.length === 0) {
            if (variationSelectButton) {
                const firstVariationNode = variationSelectButton.parentElement.querySelector('.card__select');
                const firstVariation = JSON.parse(firstVariationNode.getAttribute('data-variation'));

                addToCartButton.setAttribute(
                    'data-variation',
                    firstVariationNode?.getAttribute('data-variation-id')
                );

                firstVariationNode.classList.add('active');

                if (/product\/\d+/.test(location.pathname)) {
                    document.querySelector('.product__content .product__price')
                        .innerHTML = `${firstVariation.price.toLocaleString()} ₸`;

                    document.querySelector('.product__content .product__weight')
                        .innerHTML = `${firstVariation.name} кг`;

                    document.querySelector('.product__price-black')
                        .innerHTML = `${firstVariation.price.toLocaleString()} ₸`;
                } else {
                    productContentContainerNode.querySelector('.card__cost').innerText = `${firstVariation.price.toLocaleString()} ₸`;
                    productContentContainerNode.querySelector('.card__weight').innerText = `${firstVariation.name} кг`;
                }

                variationSelectButton.remove();
            }
        } else if (response.data.length === 1) {
            hasNested = true;

            variationSelectButton.setAttribute('data-variation', JSON.stringify(response.data[0]));
            variationSelectButton.setAttribute('data-variation-id', response.data[0].id);
            variationSelectButton.innerText = `${response.data[0].name} кг`;

            if (/product\/\d+/.test(location.pathname)) {
                document.querySelector('.product__content .product__price')
                    .innerHTML = `${response.data[0].price.toLocaleString()} ₸`;

                document.querySelector('.product__content .product__weight')
                    .innerHTML = `${response.data[0].name} кг`;

                document.querySelector('.product__price-black')
                    .innerHTML = `${response.data[0].price.toLocaleString()} ₸`;
            } else {
                productContentContainerNode.querySelector('.card__cost').innerText = `${response.data[0].price.toLocaleString()} ₸`;
                productContentContainerNode.querySelector('.card__weight').innerText = `${response.data[0].name} кг`;
            }

            variationSelectButton.classList.add('active');

            addToCartButton.setAttribute('data-variation', response.data[0].id);
        } else {
            console.error('Программист сам не понял, что произошло :(');
        }

        productsFromCaw.forEach(product => {
            if (product.variations) {
                product.variations.forEach((variation, index) => {
                    if (variation.id == variationId && hasNested) {
                        variation.id = response.data[0].id;
                        variation.name = response.data[0].name;
                        variation.price = response.data[0].price;
                    } else if (variation.id == variationId && !hasNested) {
                        product.variations.splice(index, 1);
                    }
                });
            }

            const pair = product.pair;

            if (pair) {
                if (pair.variations) {
                    pair.variations.forEach((variation, index) => {
                        if (variation.id == variationId && hasNested) {
                            variation.id = response.data[0].id;
                            variation.name = response.data[0].name;
                            variation.price = response.data[0].price;
                        } else if (variation.id == variationId && !hasNested) {
                            pair.variations.splice(index, 1);
                        }
                    });
                }
            }
        });
    }
};
