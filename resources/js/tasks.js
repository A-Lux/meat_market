import axios from 'axios';

document.addEventListener('DOMContentLoaded', _ => {
    if (/home\/quests/.test(location.pathname)) {
        initRateOrderModal();
        initSharingToSocialNetworks();
    }
});

const initRateOrderModal = _ => {
    const sendOrderRateButton = document.querySelector('.modal__order-rate .send-order-rate');
    const rateOrderModal = document.querySelector('.modal__order-rate');
    const markStars = document.querySelectorAll('.modal__order-rate-star-icon');
    const orderIdHiddenInput = document.querySelector('.modal__order-rate input[name="order_id"]');
    const questIdHiddenInput = document.querySelector('.modal__order-rate input[name="quest_id"]');
    const markHiddenInput = document.querySelector('.modal__order-rate input[name="mark"]');
    const errorBox = document.querySelector('.modal__order-rate .error-message');
    const successBox = document.querySelector('.modal__order-rate .success-message');
    const commentTextarea = document.querySelector('.modal__order-rate textarea[name="comment"]');

    if (markStars.length > 0 && markHiddenInput) {
        initRateOrderMarkSwitching(markStars, markHiddenInput);
    }

    if (sendOrderRate) {
        let loading = false;

        const onError = message => {
            errorBox.classList.remove('hide');
            errorBox.innerText = message;
        };

        const onSuccess = message => {
            successBox.classList.remove('hide');
            successBox.innerText = message;

            setTimeout(_ => location.reload(), 1000);
        };

        sendOrderRateButton.addEventListener('click', _ => {
            if (!loading) {
                loading = true;

                successBox.classList.add('hide');
                errorBox.classList.add('hide');

                sendOrderRate(
                    orderIdHiddenInput.value,
                    markHiddenInput.value,
                    commentTextarea.value,
                    questIdHiddenInput.value,
                    onError,
                    onSuccess
                ).finally(_ => {
                    loading = false;
                });
            }
        });
    }

    initTogglingOrderRateModal(rateOrderModal, orderIdHiddenInput, questIdHiddenInput);
};

const initRateOrderMarkSwitching = (starIcons, hiddenInput) => {
    starIcons.forEach(node => {
        const onClickHandler = node => {
            const clickedMark = parseInt(node.getAttribute('data-mark'));

            starIcons.forEach((starIcon, index) => {
                if (index + 1 <= clickedMark) {
                    starIcon.setAttribute('src', `${location.origin}/img/active-star-icon.svg`);
                } else {
                    starIcon.setAttribute('src', `${location.origin}/img/star-icon.svg`);
                }
            });

            hiddenInput.value = clickedMark;
        };

        node.addEventListener('click', _ => onClickHandler(node));
    });
};

const sendOrderRate = async (orderId, mark, comment, questId, onError, onSuccess) => {
    const response = await axios.post(
        `${location.origin}/order/create-review`,
        {'order_id': orderId, mark, comment, 'quest_id': questId});

    if (response.data.error) {
        onError(response.data.message);
    } else {
        onSuccess(response.data.message);
    }
};

const initTogglingOrderRateModal = (rateOrderModal, orderIdHiddenInput, questIdHiddenInput) => {
    const showHandler = (orderId, questId) => {
        rateOrderModal.classList.remove('hide');
        orderIdHiddenInput.value = orderId;
        questIdHiddenInput.value = questId;
    };

    // Init closing popup
    if (rateOrderModal.querySelector('.close')) {
        rateOrderModal.querySelector('.close').addEventListener('click', _ => {
            rateOrderModal.classList.add('hide');
        });
    }

    const openModalButtons = document.querySelectorAll('.order-rate-task');

    openModalButtons.forEach(node => {
        node.addEventListener('click', _ => {
            showHandler(
                node.getAttribute('data-order-id'),
                node.getAttribute('data-quest-id')
            );
        });
    });
};

const initSharingToSocialNetworks = _ => {
    const shareLinks = document.querySelectorAll('.share-link');
    const moveToShareButtons = document.querySelectorAll('.move-to-share');
    const shareModal = document.querySelector('.modal__soc');

    if (shareLinks.length > 0) {
        initTogglingShareModal(shareModal, moveToShareButtons, shareLinks);

        moveToShareButtons.forEach(node => {
            node.addEventListener('click', e => {
                e.preventDefault();

                moveToShare(
                    node.getAttribute('data-social-network'),
                    node.getAttribute('data-quest-id')
                );
            });
        });
    }
};

const initTogglingShareModal = (modal, moveToShareButtons, shareLinks) => {
    const showHandler = questId => {
        modal.classList.remove('hide');

        moveToShareButtons.forEach(node => {
            node.setAttribute('data-quest-id', questId);
        });
    };

    modal.querySelector('.close').addEventListener('click', _ => {
        modal.classList.add('hide');
    });

    shareLinks.forEach(linkNode => {
        linkNode.addEventListener('click', _ => {
            showHandler(linkNode.getAttribute('data-quest-id'));
        });
    });
}

const moveToShare = (socialNetwork, questId) => {
    location.replace(`${location.origin}/share?quest_id=${questId}&social_network=${socialNetwork}`);
}