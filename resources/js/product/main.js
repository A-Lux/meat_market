import axios from 'axios';
import Swal from 'sweetalert2';
import { redrawProductCard_v2 } from '../product-mode-switching';
import Lang from '../helpers/Lang';
import { addProductToCart } from '../product-adding-to-cart';
import { cardSelectsAddEventListeners } from '../basket/main';
// import {replaceOnNestedVariationForCawParts} from '../main';


let filter = {
    sort_type: 'frozen_price',
    sort_value: 'asc',
    category: document.getElementById('category_id') ? document.getElementById('category_id').value : '',
    cow_part: document.getElementById('part_id') ? document.getElementById('part_id').value : '',
    limit: 15,
};

let moreButton = document.getElementById('show_more'), products = null;

document.addEventListener('DOMContentLoaded', async function () {
    if (location.pathname !== '/' && location.pathname !== '/sets') {
        await renderingProductsInCatalog();
    }
});

let main_panel = document.getElementById('main_category_panel');

async function renderingProductsInCatalog(paginate = 0) {
    let response = await axios.post(`/catalog`, filter);

    if (!main_panel) {
        return;
    }

    main_panel.innerHTML = '';

    response.data.products.forEach(product => {
        product.isFrozen = false;
    });

    if (response.data.products.length < filter.limit) {
        moreButton.style.display = 'none';
    }

    response.data.products.forEach((product, index) => {
        let switcher = '';

        if (product.pair) {
            if (product.pair.variations.length > 0) {
                switcher = `
                    <label class="switcher">
                        <input type="checkbox" name="is_frozen" data-product-index="${index}" data-need-change="${product.variations?.length === 0 ? '1' : '0'}">
                        <span class="switcher-slide">
                        </span>
                    </label>
                `;
            }
        }

        let tagIcon = '';

        if (product.tag !== 'none') {
            tagIcon = `<img src="/img/${product.tag}_icon.png" alt="">`;
        }

        main_panel.innerHTML += `
             <div class="category__column">
                <div class="card card_category" data-product-id="${product.id}" data-product='${JSON.stringify(product)}'>
                    <a class="linked_wrapper" href="/product/${product.id}">
                        <div class="card__img">
                            <img src="/storage/${product.main_image}"
                                 alt="Meat Market – ${product.name}"
                                 title="Meat Market – ${product.name}">
                            <div class="card__brand">
                                ${tagIcon}
                            </div>
                        </div>
                    </a>
                    <div class="card__content">
                        <div class="card__top">
                            <a class="card__top-linked-wrapper" href="/product/${product.id}">
                                <h3 class="card__title">${product.name.replace(/охл|зам/gi, '')}</h3>
                            </a>
                            ${switcher}
                            <div class="card__favorite"><span class="icon-star" data-product="${product.id}"></span></div>
                        </div>
                        <div class="card__mid" id="product-price-table-${product.id}" style="display: ${product.variations.length === 0 ? 'none' : 'flex'}">
                            <span class="card__cost">${product.variations[0]?.price?.toLocaleString() || ''} ₸</span>
                            <span class="card__weight">${product.variations[0]?.name || ''} кг</span>
                            <span class="card__bonus">${product.variations[0]?.bonus != null ? product.variations[0]?.bonus + ' ₸' : ''}</span>
                       </div>
                       <div class="card__selects" id="product-${product.id}"></div>
                       <div class="card__add-basket btn-basket"
                            id="product_add_button-${product.id}"
                            data-product="${product.id}"
                            data-variation="${product.variations[0]?.id}"
                            style="display: ${product.variations.length === 0 ? 'none' : 'block'}">
                            <span class="icon-shopping-bag"></span>${Lang.translate('add-to-cart')}</div>
                       <div class="btn-basket not-in-stock" style="display: ${product.variations.length > 0 ? 'none' : 'block'}">${Lang.translate('not-in-stock')}</div>
                    </div>
                </div>
             </div>
        `;

        if (product.variations.length > 0) {
            document.getElementById(`product-${product.id}`).innerHTML = '';

            for (let index = 0; index < 3; index++) {
                const variation = product.variations[index];

                if (variation) {
                    document.getElementById(`product-${product.id}`).innerHTML += `
                        <button class="card__select ${index === 0 ? 'active' : ''}"
                                data-variation='${JSON.stringify(variation)}'
                                data-variation-id="${variation.id}"
                                data-product="${product.id}">${variation.name} кг
                        </button>
                    `;
                }
            }
        }
    });

    clickButtonSelect();
    favoriteClick();
    initSwitching();
    addToBasketEvent();
}

export function initSwitching() {
    const switchers = document.querySelectorAll('label.switcher input');
    const getProductCardNode = checkboxNode => {
        return checkboxNode.parentNode.parentNode.parentNode.parentNode;
    };

    switchers.forEach(node => {
        const handler = checked => {
            redrawProductCard_v2(getProductCardNode(node), checked);
            clickButtonSelect();
        };

        node.addEventListener('change', _ => {
            handler(node?.checked);
        });

        if (node.getAttribute('data-need-change') === '1') {
            node.checked = true;
            handler(true);
        }
    });
}

export function clickButtonSelect() {
    const variableButtons = document.querySelectorAll('.card__select');

    const onClickHandler = node => {
        const variation = JSON.parse(node.getAttribute('data-variation'));

        const addButtons = document.querySelectorAll(`#product_add_button-${variation.product_id}`);
        const productTables = document.querySelectorAll(`#product-price-table-${variation.product_id}`);

        addButtons.forEach(node => {
            node.setAttribute('data-variation', variation.id);
        });

        productTables.forEach(node => {
            node.innerHTML = `
                <span class="card__cost">${variation.price.toLocaleString()} ₸</span>
                <span class="card__weight">${variation.name} кг</span>
                <span class="card__bonus">${variation.bonus !== null ? variation.bonus + ' ₸' : ''}</span>
            `;
        });

        node.parentNode.querySelectorAll('.card__select').forEach(item => {
            item.classList.remove('active');
        });

        node.classList.add('active');
    };

    variableButtons.forEach(node => {
        const listener = _ => onClickHandler(node);

        node.removeEventListener('click', listener);
        node.addEventListener('click', listener);
    });
}

export function favoriteClick() {
    let favoriteButtons = document.getElementsByClassName('icon-star');
    if (favoriteButtons.length) {
        for (let i = 0; i < favoriteButtons.length; i++) {
            favoriteButtons[i].addEventListener('click', async (e) => {
                e.preventDefault();
                let productId = favoriteButtons[i].getAttribute('data-product');
                const response = await axios.get(`/home/favourite/add/${productId}`);
                if (response.status === 201) {
                    let buttonFavorite = favoriteButtons[i];
                    buttonFavorite.classList.add('icon-star-bold');

                    // swal('Успешно', response.data.message, 'success')
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-start',
                    });
                } else {
                    let buttonFavorite = favoriteButtons[i];

                    buttonFavorite.classList.remove('icon-star-bold');

                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-start',
                    });
                }
            });
        }
    }


}


let sort_input = document.getElementById('sort');

if (sort_input) {
    sort_input.addEventListener('change', async function (e) {
        filter.sort_value = e.target.value;
        await renderingProductsInCatalog();
    });
}


let search = document.getElementById('search');

if (search) {
    search.addEventListener('input', async function (e) {
        if (e.target.value !== '') {
            filter.search = e.target.value;
            await renderingProductsInCatalog();
        } else {
            delete filter.search;
            await renderingProductsInCatalog();
        }
    });
}

if (moreButton) {
    moreButton.addEventListener('click', async function (e) {
        e.preventDefault();

        filter.limit += 15;

        await renderingProductsInCatalog(1);
    });
}

export default function addToBasketEvent() {
    const addToBasketButtons = document.querySelectorAll('.card__add-basket');
    const addToBasketButtonInProductPage = document.querySelector('#basket_add_button');

    const onSuccessHandler = async (productCardNode, variationId) => {
        await replaceOnNestedVariation_v2(
            productCardNode,
            variationId
        );

        await redrawProductCard_v2(
            productCardNode,
            productCardNode?.querySelector('input[name="is_frozen"]')?.checked
        );

        clickButtonSelect();
    };

    const onClickHandler = async (node) => {
        await addProductToCart(
            node.getAttribute('data-product'),
            node.getAttribute('data-variation'),
            _ => {
                onSuccessHandler(
                    node.parentElement.parentElement,
                    node.getAttribute('data-variation')
                );
            }
        );
    };

    addToBasketButtons.forEach(node => {
        const listener = _ => onClickHandler(node);

        node.removeEventListener('click', listener);
        node.addEventListener('click', listener);
    });

    if (addToBasketButtonInProductPage) {
        const getProductCardNode = addToCartButton => {
            return addToCartButton.parentElement.parentElement.parentElement
                .parentElement.parentElement.parentElement;
        };

        addToBasketButtonInProductPage.addEventListener('click', async _ => {
            const productId = addToBasketButtonInProductPage.getAttribute('data-product');
            const variationId = addToBasketButtonInProductPage.getAttribute('data-variation');

            await addProductToCart(
                productId,
                variationId,
                async _ => {
                    await replaceOnNestedVariation_MainCard(
                        getProductCardNode(addToBasketButtonInProductPage),
                        variationId,
                        productId
                    );

                    await redrawMainProductCard(getProductCardNode(addToBasketButtonInProductPage));

                    cardSelectsAddEventListeners();
                }
            );
        });
    }
}

const redrawMainProductCard = cardNode => {
    const data = JSON.parse(cardNode.getAttribute('data-product'));

    console.log('Product data for redrawing: ', data);

    const variations = data['variations'];

    console.log('Variations data for redrawing: ', variations);

    cardNode.querySelector('.card__selects').innerHTML = '';

    if (variations.length === 0) {
        cardNode.querySelector('.product__data').style.display = 'none';
        cardNode.querySelector('.product__pay').style.display = 'none';
        cardNode.querySelector('.btn-basket.not-in-stock').style.display = 'block';
    } else {
        cardNode.querySelector('.product__data').style.display = 'flex';
        cardNode.querySelector('.product__pay').style.display = 'flex';
        cardNode.querySelector('.btn-basket.not-in-stock').style.display = 'none';

        cardNode.querySelector('.product__price').innerHTML = `${variations[0].price.toLocaleString()} ₸`;
        cardNode.querySelector('.product__weight').innerHTML = `${variations[0].name} ${Lang.translate('kg')}`;
        cardNode.querySelector('.product__price-black').innerHTML = `${variations[0].price.toLocaleString()} ₸`;

        cardNode.querySelector('.btn-basket.product__add-basket').setAttribute(
            'data-variation',
            variations[0]['id']
        );

        variations.forEach((variation, index) => {
            cardNode.querySelector('.card__selects').innerHTML += `
                <button class="card__select ${index === 0 ? 'active' : ''}"
                        data-variation='${JSON.stringify(variation)}'
                        data-variation-id="${variation.id}"
                        data-product="${data.id}">${variation.name} ${Lang.translate('kg')}</button>
            `;
        });
    }
};

const replaceOnNestedVariation_MainCard = async (productNode, variationId, productId) => {
    const productData = JSON.parse(productNode.getAttribute('data-product'));

    const response = await axios.get(`${location.origin}/api/product/nested-variation?product_id=${productId}`);

    if (response.status === 200) {
        const updatedVariations = getUpdatedVariationProductData(productData, variationId, response.data[0] || null);

        console.log('Updated variations: ', updatedVariations);

        productNode.setAttribute('data-product', JSON.stringify({
            ...productData,
            variations: updatedVariations
        }));
    }
};

const replaceOnNestedVariation_v2 = async (productNode, variationId) => {
    const productId = productNode.getAttribute('data-product-id');
    const addToCartButton = productNode.querySelector('.card__add-basket');
    const productData = JSON.parse(productNode.getAttribute('data-product'));

    let chosenModeProductData = productData;

    if (productNode?.querySelector('input[name="is_frozen"]')?.checked) {
        chosenModeProductData = productData.pair;
    }

    if (!productId) {
        console.error('Не удалось получить ID товара!');
        return;
    }

    if (!productData) {
        console.error('Не удалось получить данный о товаре!');
        return;
    }

    if (!addToCartButton) {
        console.error('Не удалось получить кнопку добавления товара в корзину!');
        return;
    }

    const response = await axios.get(`${location.origin}/api/product/nested-variation?product_id=${productId}`);

    if (response.status === 200) {
        const data = response.data;

        if (data.length === 0) { // если с сервера не пришли вариации
            const updatedVariations = getUpdatedVariationProductData(chosenModeProductData, variationId);

            if (productNode?.querySelector('input[name="is_frozen"]')?.checked) {
                productData['pair']['variations'] = updatedVariations;
            } else {
                productData['variations'] = updatedVariations;
            }

            productNode.setAttribute('data-product', JSON.stringify(productData));
        } else if (data.length > 0) {
            const newVariation = data[0];

            if (!newVariation.id) {
                console.error('Не удалось получить ID ближайшей вариации, пришедшей с сервера!');
                return;
            }

            const updatedVariations = getUpdatedVariationProductData(chosenModeProductData, variationId, newVariation);
            console.log("");

            if (productNode?.querySelector('input[name="is_frozen"]')?.checked) {
                productData['pair']['variations'] = updatedVariations;
            } else {
                productData['variations'] = updatedVariations;
            }

            productNode.setAttribute('data-product', JSON.stringify(productData));
        }
    } else {
        console.error('Произошла ошибка: ', response);
    }
};

const getUpdatedVariationProductData = (initProductData, replacedVariationId, nextVariation = null) => {
    if (nextVariation) {
        return initProductData['variations'].map(variation => {
            if (parseInt(variation['id']) === parseInt(replacedVariationId)) {
                return {
                    ...variation,
                    id: nextVariation['id'],
                    name: nextVariation['name'],
                    price: nextVariation['price']
                };
            }

            return variation;
        });
    } else {
        console.log('Variation was removed!');
        return initProductData['variations'].filter(variation => {
            return parseInt(variation['id']) !== parseInt(replacedVariationId);
        });
    }
};
