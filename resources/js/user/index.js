// import swal from "sweetalert";
import Swal from 'sweetalert2'

import axios from "axios";
let submitButton = document.getElementById('update_user_data')
let genderValue = document.getElementById('sex')
let femaleButton = document.getElementById('female')
if (femaleButton){
    femaleButton.addEventListener('click',function () {
        genderValue.value = 0
    })
}

let maleButton = document.getElementById('male')
if (maleButton){
    maleButton.addEventListener('click',function(){
        genderValue.value = 1
    })
}
let userDataPhone = document.getElementById('user_data_form')

userDataPhone.addEventListener('submit',async function(e){
    e.preventDefault()
    let form = {}
    for (let i = 0; i<userDataPhone.length;i++){
        form[userDataPhone[i].getAttribute('name')] = userDataPhone[i].value

    }
    const response = await axios.post('/home/update',form)
    if (response.status === 200){
        // swal('Успешно','Ваш профиль был обновлен','success')
        Swal.fire({
            icon: 'success',
            title: 'Успешно',
            text: 'Ваш профиль был обновлен',
            toast: true,
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            position: 'top-end',
        })
    }
})
