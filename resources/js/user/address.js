import axios from 'axios';
// import swal from "sweetalert";
import Swal from 'sweetalert2';
import Lang from '../helpers/Lang';


let address_panel = document.getElementById('address_panel');
const modal = document.querySelector('.modal');

document.addEventListener('DOMContentLoaded', async function () {
    await getAddresses();
});

async function getAddresses() {
    const response = await axios.get('/home/addresses/index');
    if (response.status === 200) {
        address_panel.innerHTML = '';

        response.data.addresses.forEach(address => {
            address_panel.innerHTML += `
                 <div class="addresses__column">
                    <div class="addresses__item">
                        <h3>${address.title}</h3>
                        <p>${address.name}</p>
                        <p>${address.comment || ''}</p>
                        <div class="addresses__func">
                            <input ${address.is_main ? 'checked' : ''} type="radio" name="address" class="addressMainChooseButton" id="address-${address.id}" value="${address.id}" data-coords="${address.coords}">
                            <label for="address-${address.id}"><span>${Lang.translate('main-address')}</span></label>
                            <div class="addresses__icons">
                                <div class="addresses__delete" data-address="${address.id}">
                                    <span class="icon-delete"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        });

        await deleteButtons();
        mainChooseButton();
    }
}

function closeModal() {
    modal.classList.add('hide');
    modal.classList.remove('show');
    document.body.style.overflow = '';
}

let addressForm = document.getElementById('add_address_form');

if (addressForm) {
    addressForm.addEventListener('submit', async function (e) {
        e.preventDefault();

        let form = {};
        for (let i = 0; i < addressForm.length; i++) {
            form[addressForm[i].getAttribute('name')] = addressForm[i].value;
        }
        form.is_main = 0;

        const response = await axios.post('/home/addresses', form);
        if (response.status === 201) {
            Swal.fire({
                icon: 'success',
                title: 'Успешно',
                text: 'Адрес был успешно добавлен',
                toast: true,
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                position: 'top-end',
            });
            await getAddresses();
            let form = {};
            for (let i = 0; i < addressForm.length; i++) {
                form[addressForm[i].getAttribute('name')] = '';
            }
            closeModal();
        }
    });
}

function deleteButtons() {

    let iconDeleteButtons = document.getElementsByClassName('addresses__delete');
    for (let i = 0; i < iconDeleteButtons.length; i++) {
        iconDeleteButtons[i].addEventListener('click', async function () {
            let addressId = iconDeleteButtons[i].getAttribute('data-address');
            const response = await axios.get(`/home/addresses/delete/${addressId}`);
            if (response.status === 200) {
                // swal('Успешно','Адрес был успешно удален','success')
                Swal.fire({
                    icon: 'success',
                    title: 'Успешно',
                    text: 'Адрес был успешно удален',
                    toast: true,
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    position: 'top-end',
                });
                await getAddresses();
            }
        });
    }
}


function mainChooseButton() {
    let chooseButtons = document.getElementsByClassName('addressMainChooseButton');
    for (let i = 0; i < chooseButtons.length; i++) {
        chooseButtons[i].addEventListener('click', async () => {
            let addressId = chooseButtons[i].value;
            const response = await axios.post(`/home/addresses/update/${addressId}`, {
                is_main: 1
            });
            if (response.status === 200) {
                // swal('Успешно','Адрес выбран','success')
                Swal.fire({
                    icon: 'success',
                    title: 'Успешно',
                    text: 'Адрес выбран',
                    toast: true,
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    position: 'top-end',
                });
            }
            await getAddresses();
            location.reload();
        });

    }
}
