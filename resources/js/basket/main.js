import axios from 'axios';
import Swal from 'sweetalert2';
import Lang from '../helpers/Lang';


let basket_icon = document.getElementById('basket_icon');

document.addEventListener('DOMContentLoaded', async function () {
    await getBasket();
});

basket_icon.addEventListener('click', async function (e) {
    e.preventDefault();

    await getBasket();
});

export function cardSelectsAddEventListeners() {
    const cardSelects = document.querySelectorAll('.card__select');

    cardSelects.forEach(node => {
        const listener = _ => cardSelectOnClickHandler(node);

        node.removeEventListener('click', listener);
        node.addEventListener('click', listener);
    });
}

cardSelectsAddEventListeners();

const cardSelectOnClickHandler = node => {
    const variation = JSON.parse(node.getAttribute('data-variation'));

    if (/product\/\d+/.test(location.pathname)) {
        const addButton = document.getElementById('basket_add_button');

        if (/product\/\d+/.test(location.pathname)) {
            addButton.setAttribute('data-variation', variation.id);
        } else {
            addButton.setAttribute('data-variation-id', variation.id);
        }

        document.querySelector('.product__pay .product__price-black')
            .innerHTML = `${variation.price.toLocaleString()} ₸`;

        document.querySelector('.product__content .product__price')
            .innerHTML = `${variation.price.toLocaleString()} ₸`;

        document.querySelector('.product__content .product__weight')
            .innerHTML = `${variation.name} ${Lang.translate('kg')}`;
    } else {
        const addButton = document.getElementById(`product_add_button-${variation.product_id}`);
        const productTable = document.getElementById(`product-price-table-${variation.product_id}`);
        const productTables = document.getElementsByClassName(`product-price-table-${variation.product_id}`);

        if (productTables.length) {
            for (let z = 0; z < productTables.length; z++) {
                productTables[z].innerHTML = '';
                productTables[z].innerHTML += `
                    <span class="card__cost">${variation.price.toLocaleString()} ₸</span>
                    <span class="card__weight">${variation.name} ${Lang.translate('kg')}</span>
                    <span class="card__bonus">${variation.bonus !== null ? variation.bonus + ' ₸' : ''} </span>
                `;
            }
        }

        addButton.setAttribute('data-variation', variation.id);
        productTable.innerHTML = '';
        productTable.innerHTML += `
            <span class="card__cost">${variation.price.toLocaleString()} ₸</span>
            <span class="card__weight">${variation.name} ${Lang.translate('kg')}</span>
            <span class="card__bonus">${variation.bonus !== null ? variation.bonus + ' ₸' : ''} </span>
        `;
    }

    node.parentNode.querySelectorAll('.card__select').forEach(item => {
        item.classList.remove('active');
    });

    node.classList.add('active');
};


async function getBasket() {
    const response = await axios.get('/basket');
    let basket_panel = document.getElementById('basket_panel');

    basket_panel.innerHTML = '';
    let listArrayPanel = {};
    response.data.grouped_basket.forEach((items) => {


        let weightNode = '', price = 0, weight = 0, bonusNode = '';

        let id_remove = [];
        let count = 0;
        items.forEach(item => {
            if (item['variation_id']) {
                id_remove.push(item['variation_id']["id"]);
                price += parseInt(item['variation_id']['price']) * parseInt(item['quantity']);
                weight += parseFloat(item['variation_id']['name']) * parseInt(item['quantity']) * 100 / 100;
            } else {
                price += parseInt(item['product']['price']);
            }
            count++;
        });

        let item = items[0];

        if (typeof listArrayPanel[item.product.id] == "undefined") {
            listArrayPanel[item.product.id] = true;

            basket_panel.innerHTML += `
            <div class="popup__item">
                <div class="popup__img">
                    <img src="${item.product.main_image}" alt="">
                </div>
                <div class="popup__text">
                    <p class="popup__name">${item.product.name}</p>
                    <div class="popup__cost">
                        <p class="popup__price">${price.toLocaleString()} ₸</p>
                        <p class="popup__weight">${weight.toFixed(3)} ${Lang.translate('kg')} x ${count}</p>
                    </div>
                </div>
                <div class='popup__etc'>


                    <div class='popup__remove' data-remove="` + JSON.stringify(id_remove) + `" id='remove${item.product.id}'>
                    <?xml version="1.0"?>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg"><path d="m424 64h-88v-16c0-26.51-21.49-48-48-48h-64c-26.51 0-48 21.49-48 48v16h-88c-22.091 0-40 17.909-40 40v32c0 8.837 7.163 16 16 16h384c8.837 0 16-7.163 16-16v-32c0-22.091-17.909-40-40-40zm-216-16c0-8.82 7.18-16 16-16h64c8.82 0 16 7.18 16 16v16h-96z" fill="#b41f21" data-original="#000000" style="" class=""/><path d="m78.364 184c-2.855 0-5.13 2.386-4.994 5.238l13.2 277.042c1.22 25.64 22.28 45.72 47.94 45.72h242.98c25.66 0 46.72-20.08 47.94-45.72l13.2-277.042c.136-2.852-2.139-5.238-4.994-5.238zm241.636 40c0-8.84 7.16-16 16-16s16 7.16 16 16v208c0 8.84-7.16 16-16 16s-16-7.16-16-16zm-80 0c0-8.84 7.16-16 16-16s16 7.16 16 16v208c0 8.84-7.16 16-16 16s-16-7.16-16-16zm-80 0c0-8.84 7.16-16 16-16s16 7.16 16 16v208c0 8.84-7.16 16-16 16s-16-7.16-16-16z" fill="#b41f21" data-original="#000000" style="" class=""/></g></g></svg>
                    </div>

                    <div class='popup__minus' id='minus${item.product.id}' style="margin-top: 10px">
                    <button class="" data-product-id="${item.product.id}" data-variation-id="${item.variation_id ? item.variation_id.id : '-1'}">
                    <?xml version="1.0"?>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
                    <circle xmlns="http://www.w3.org/2000/svg" style="" cx="256" cy="256" r="256" fill="#b41f21" data-original="#e04f5f" class=""/>
                    <rect xmlns="http://www.w3.org/2000/svg" x="119.68" y="240" style="" width="272" height="32" fill="#ffffff" data-original="#ffffff"/>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    <g xmlns="http://www.w3.org/2000/svg">
                    </g>
                    </g></svg>
                    </button>
                    </div>


                    <div class='popup__plus' id='plus${item.product.id}' style="margin-top: 10px">
                    <button class="basket__add_button basket__add_nested_variation_button" data-product-id="${item.product.id}" data-variation-id="${item.variation_id ? item.variation_id.id : '-1'}">
                    <?xml version="1.0"?>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="20" height="20" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><path xmlns="http://www.w3.org/2000/svg" d="m256 0c-141.164062 0-256 114.835938-256 256s114.835938 256 256 256 256-114.835938 256-256-114.835938-256-256-256zm112 277.332031h-90.667969v90.667969c0 11.777344-9.554687 21.332031-21.332031 21.332031s-21.332031-9.554687-21.332031-21.332031v-90.667969h-90.667969c-11.777344 0-21.332031-9.554687-21.332031-21.332031s9.554687-21.332031 21.332031-21.332031h90.667969v-90.667969c0-11.777344 9.554687-21.332031 21.332031-21.332031s21.332031 9.554687 21.332031 21.332031v90.667969h90.667969c11.777344 0 21.332031 9.554687 21.332031 21.332031s-9.554687 21.332031-21.332031 21.332031zm0 0" fill="#b41f21" data-original="#000000" style="" class=""/></g></svg>
                    </button>
                    </div>
                </div>
            </div>
        `;
        }

    });
    eventRemoveClose();

    eventAddButton();

    let listArray = {};
    response.data.basket.forEach(item => {

        if (typeof listArray[item.product.id] == "undefined") {
            listArray[item.product.id] = true;
            const {plus, minus, remove} = {
                plus: document.querySelector(`#plus${item.product.id}`),
                minus: document.querySelector(`#minus${item.product.id}`),
                remove: document.querySelector(`#remove${item.product.id}`)
            }

            const request = function (url = '/basket/change/quantity') {
                const requestParam = {
                    id: item.product.id,
                    variation: item.variation_id ? item.variation_id.id : '-1',
                    quantity: 1
                }

                return {
                    url: url,
                    body: requestParam
                }
            }

            function axiosRequest(requestParam) {
                return async function () {
                    let obj = await axios.post(requestParam.url, requestParam.body);
                }
            }

            // plus.addEventListener('click', axiosRequest(request()));
            minus.addEventListener('click', function () {
                const product_id = item.product.id;
                console.log("asda");
                const variation_id = item.variation_id ? item.variation_id.id : '-1'
                const variationsPopup = document.querySelector('.modal__variations');
                const variationsContainer = variationsPopup.querySelector('.variations__container');


                variationsPopup.classList.remove('hide');
                variationsContainer.innerHTML = '';

                variationsPopup.querySelector('.close').addEventListener('click', _ => {
                    variationsPopup.classList.add('hide');
                });
                console.log(response.data.basket);
                response.data.basket.forEach(variation => {
                    variationsPopup.querySelector('.variations__container').innerHTML += `
                    <button class="variation__item"
                            data-product="${product_id}"
                            data-variation="${variation.variation_id.id}">
                        ${variation.variation_id.name} кг</button>
                `;
                });

                variationsPopup.querySelectorAll('.variation__item').forEach(node => {
                    node.addEventListener('click', async _ => {
                        const response = await axios.post('/basket/remove', {
                            id: node.getAttribute('data-product'),
                            variation: node.getAttribute('data-variation')
                        });
                        console.log(response);
                        if (response.status === 200) {
                            location.reload();
                        }
                    });
                });
            });

            remove.addEventListener('click', () => {

                const responseBasket = axios.get('/basket');
                responseBasket.then(function (responsData) {

                    let idProduct = $(remove);
                    idProduct = idProduct.prop("id").split("remove");
                    idProduct = idProduct[1];

                    $.each(responsData.data.basket, function (index, item) {
                        if (item.product_id == idProduct) {
                            axios.post('/basket/remove', {
                                id: idProduct,
                                variation: item.variation_id.id
                            });
                        }
                    });


                });


            });
        }
    })


    let totalSum = document.querySelector('.total_sum');
    totalSum.innerHTML = response.data.totals.total_sum + ' ₸';

    let main_total_basket = document.getElementById('main_total_basket');
    main_total_basket.innerHTML = response.data.totals.total_quantity;

    await eventRemoveClose();

    let basketRow = document.getElementById('basket-row');

    if (basketRow) {
        console.log(response.data);
        renderBasketPage(response.data.basket, response.data['grouped_basket']);
    }

    let basketInfo = document.getElementById('basket-info');

    if (basketInfo) {
        renderBasketInfo(response.data.totals);
    }
}

function renderBasketInfo(data) {
    let basketInfo = document.getElementById('basket-info');
    basketInfo.innerHTML = `
        <p>(${data.total_quantity}) ${Lang.translate('products')}</p>
        <p>${data.total_sum.toLocaleString()} ₸</p>

        <p>${Lang.translate('total')}</p>
        <p>${data.total_sum.toLocaleString()} ₸</p>
    `;
}

function renderBasketPage(data, groupedBasket = null) {
    let basketRow = document.getElementById('basket-row');

    basketRow.innerHTML = '';

    groupedBasket.forEach(items => {
        let weightNode = '', price = 0, weight = 0, bonusNode = '';

        const item = items[0];

        items.forEach(item => {
            if (item['variation_id']) {
                price += parseInt(item['variation_id']['price']) * parseInt(item['quantity']);
                weight += parseFloat(item['variation_id']['name']) * parseInt(item['quantity']) * 100 / 100;
            } else {
                price += parseInt(item['product']['price']);
            }
        });

        if (item.variation_id) {
            weightNode = `
                <div class="basket-card__weight">
                    <p>${Lang.translate('weight')}</p>
                    <p>${weight.toFixed(3)} ${Lang.translate('kg')}</p>
                </div>
            `;
        }

        if (item.variation_id) {
            if (item.variation_id.bonus) {
                bonusNode = `
                    <div class="basket-card__bonus">
                        <p>${Lang.translate('bonus')}</p>
                        <span>+ ${item.variation_id.bonus} ₸</span>
                    </div>
                `;
            }
        }

        // ${Lang.translate('order-more')}

        basketRow.innerHTML += `
            <div class="basket__column">
                <div class="basket__item basket-card">
                    <div class="basket-card__img">
                        <img src="${item.product.main_image}" alt="">
                    </div>
                    <div class="basket-card__content">
                        <h2 class="basket-card__title">${item.product.name}</h2>
                        <div class="basket-card__total">
                            <p>${Lang.translate('total')}</p>
                            <p>${items.length}</p>
                        </div>
                        ${weightNode}
                        <div class="basket-card__cost">
                            <p>${Lang.translate('cost')}</p>
                            <p>${price.toLocaleString()} ₸</p>
                        </div>
                        <div class="basket-card__sale">

                        </div>
                        ${bonusNode}
                        <div class="basket__func">

                            <button class="basket__add_button basket__add_nested_variation_button"
                            data-product-id="${item.product.id}"
                            data-variation-id="${item.variation_id ? item.variation_id.id : -1}" >
                            <span>+</span>
                            </button>

                            <button class="basket__delete"
                            data-product-id="${item.product.id}"
                            data-variation-id="${item.variation_id ? item.variation_id.id : -1}"
                            ><span class="icon-delete"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        `;
    });
    eventRemoveClose();
    eventAddButton();
}

eventAddButton();
favoriteClick();

function favoriteClick() {
    let favoriteButtons = document.getElementsByClassName('icon-star');
    if (favoriteButtons.length) {
        for (let i = 0; i < favoriteButtons.length; i++) {
            favoriteButtons[i].addEventListener('click', async (e) => {
                e.preventDefault()
                let productId = favoriteButtons[i].getAttribute('data-product')
                const response = await axios.get(`/home/favourite/add/${productId}`)
                if (response.status === 201) {
                    let buttonFavorite = favoriteButtons[i]
                    buttonFavorite.classList.add('icon-star-bold')

                    let stars = document.getElementsByClassName(`star__${productId}`)
                    if (stars.length) {
                        for (let i = 0; i < stars.length; i++) {
                            stars[i].classList.add('icon-star-bold')
                        }
                    }
                    // swal('Успешно', response.data.message, 'success')
                    Swal.fire({
                        icon: 'success',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-start',
                    })
                } else {
                    let buttonFavorite = favoriteButtons[i];

                    buttonFavorite.classList.remove('icon-star-bold')
                    let stars = document.getElementsByClassName(`star__${productId}`)
                    if (stars.length) {
                        for (let i = 0; i < stars.length; i++) {
                            stars[i].classList.remove('icon-star-bold')
                        }
                    }
                    Swal.fire({
                        icon: 'success',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-start',
                    })
                }
            })
        }
    }


}

async function eventAddButton() {
    const basketAddButtons = document.querySelectorAll('.basket__add_nested_variation_button');

    const onClickHandler = async node => {
        const product_id = node.getAttribute('data-product-id');
        const variation_id = node.getAttribute('data-variation-id');
        const variationsPopup = document.querySelector('.modal__variations');
        const variationsContainer = variationsPopup.querySelector('.variations__container');
        const nestedVariations = await axios.get(
            `/api/product/nested-variations?product_id=${product_id}&excluded_variation_id=${variation_id}`
        );

        variationsPopup.classList.remove('hide');
        variationsContainer.innerHTML = '';

        variationsPopup.querySelector('.close').addEventListener('click', _ => {
            variationsPopup.classList.add('hide');
        });

        nestedVariations.data.forEach(variation => {
            variationsPopup.querySelector('.variations__container').innerHTML += `
                <button class="variation__item"
                        data-product="${product_id}"
                        data-variation="${variation.id}">
                    ${variation.name} кг</button>
            `;
        });

        variationsPopup.querySelectorAll('.variation__item').forEach(node => {
            node.addEventListener('click', async _ => {
                const response = await axios.post('/basket/add', {
                    id: node.getAttribute('data-product'),
                    variation: node.getAttribute('data-variation')
                });

                if (response.status === 201) {
                    location.reload();
                }
            });
        });
    };

    basketAddButtons.forEach(node => {
        node.addEventListener('click', e => {
            e.preventDefault();
            onClickHandler(node);
        });
    });
}

async function eventRemoveClose() {
    let closeButtons = document.getElementsByClassName('popup__close');
    for (let i = 0; i < closeButtons.length; i++) {
        closeButtons[i].addEventListener('click', async function (e) {
            let product_id = closeButtons[i].getAttribute('data-product-id');
            let variation_id = closeButtons[i].getAttribute('data-variation-id');

            const response = await axios.post('/basket/remove', {
                id: product_id,
                variation: variation_id
            });

            if (response.status === 200) {
                await getBasket();
            }

        });
    }

    let removeButtons = document.getElementsByClassName('basket__delete');
    for (let i = 0; i < removeButtons.length; i++) {
        removeButtons[i].addEventListener('click', async function (e) {
            e.preventDefault();
            let product_id = removeButtons[i].getAttribute('data-product-id');
            let variation_id = removeButtons[i].getAttribute('data-variation-id');

            const response = await axios.post('/basket/remove', {
                id: product_id,
                variation: variation_id
            });

            if (response.status === 200) {
                await getBasket();
            }

        });
    }
}

let additionalProductAddButtonEvent = document.getElementsByClassName('product_btn_add');

if (additionalProductAddButtonEvent.length) {
    for (let i = 0; i < additionalProductAddButtonEvent.length; i++) {
        additionalProductAddButtonEvent[i].addEventListener('click', async () => {
                let id = additionalProductAddButtonEvent[i].getAttribute('data-product')
                let variation = additionalProductAddButtonEvent[i].getAttribute('data-variation')
                let quantity = additionalProductAddButtonEvent[i].getAttribute('data-quantity')
                const response = await axios.post('/basket/change/quantity', {
                    id,
                    variation,
                    quantity
                })
                if (response.status === 200) {
                    // swal('Успешно ', 'Товар был добавлен в корзину', 'success')
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: 'Товар был добавлен в корзину',
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-start',
                    })
                    await getBasket()

                }
            }
        )
    }
}

let minusButtons = document.getElementsByClassName('product__minus');
if (minusButtons.length) {
    for (let i = 0; i < minusButtons.length; i++) {
        minusButtons[i].addEventListener('click', async () => {
            let productId = minusButtons[i].getAttribute('data-product');
            let addToBasketBtn = document.getElementById(`product-btn-add-${productId}`);
            let currentCount = parseInt(addToBasketBtn.getAttribute('data-quantity'));
            let quantityText = document.getElementById(`product-counter-${productId}`);
            currentCount -= 1;
            if (currentCount > 0) {
                addToBasketBtn.setAttribute('data-quantity', currentCount);
                quantityText.innerHTML = currentCount;
            }
        });
    }
}

let plusButtons = document.getElementsByClassName('product__plus');
if (plusButtons.length) {
    for (let i = 0; i < plusButtons.length; i++) {
        plusButtons[i].addEventListener('click', async () => {
            let productId = plusButtons[i].getAttribute('data-product');
            let addToBasketBtn = document.getElementById(`product-btn-add-${productId}`);
            let currentCount = parseInt(addToBasketBtn.getAttribute('data-quantity'));
            let quantityText = document.getElementById(`product-counter-${productId}`);
            currentCount += 1;
            if (currentCount > 0) {
                addToBasketBtn.setAttribute('data-quantity', currentCount);
                quantityText.innerHTML = currentCount;
            }
        });
    }
}
