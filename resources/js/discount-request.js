import axios from 'axios';
import Swal from 'sweetalert2';
import Lang from './helpers/Lang';

document.addEventListener('DOMContentLoaded', _ => {
    const discountRequestForm = document.querySelector('#discount-request-form');

    if (discountRequestForm) {
        const emailInput = discountRequestForm.querySelector('input[name="email"]');

        let loading = false;

        discountRequestForm.addEventListener('submit', e => {
            e.preventDefault();

            if (!loading && emailInput.value) {
                loading = true;
                sendDiscountRequest(emailInput.value, _ => {
                    loading = false;
                });
            }
        });
    }
});

const sendDiscountRequest = (email, stopLoading) => {
    axios.post('/api/discount-request/create', {email})
        .then(result => {
            Swal.fire({
                icon: 'success',
                title: Lang.translate('success'),
                text: result.data.message,
                toast: true,
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                position: 'top-start'
            }).then(_ => {
                location.reload();
            });
        })
        .catch(({response}) => {
            stopLoading();
            Swal.fire({
                icon: 'error',
                title: Lang.translate('error-happen'),
                text: response.data.errors['email'][0],
                toast: true,
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                position: 'top-start'
            }).then();
        });
};