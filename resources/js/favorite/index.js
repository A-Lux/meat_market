import axios from "axios";
// import swal from "sweetalert";
import Swal from 'sweetalert2'
import addToBasketEvent, {initSwitching} from '../product/main';

// initSwitching();
// addToBasketEvent();

async function getFavourites(){
    const response = await axios.get('/home/favourite/show')
    if (response.status === 200){
        let favoriteBlock = document.getElementById('favorite_row')
        favoriteBlock.innerHTML = ''
        response.data.products.forEach(product=>{
            favoriteBlock.innerHTML += `
                                         <div class="category__column">
                    <div class="card card_category">
                        <a href="/product/${product.id}">
                            <div class="card__img">
                                <img src="${product.main_image}" alt="Название товара" title="Название товара">
                                <div class="card__brand">
                                    <img src="/img/brand_1.png" alt="">
                                </div>
                            </div>
                        </a>

                        <div class="card__content">
                                <div class="card__top">
                                <a href="/product/${product.id}">
                                    <h3 class="card__title">${product.name}</h3>
                                </a>

                                    <div class="card__favorite" ><span class="icon-star" data-product="${product.id}"> </span></div>
                                </div>
                            <div class="card__mid" id="product-price-table-${product.id}">
                                <span class="card__cost">${product.variations[0].price.toLocaleString()} ₸</span>
                                <span class="card__weight">${product.variations[0].name} кг</span>
                                <span class="card__bonus">${product.variations[0].bonus != null ? product.variations[0].bonus + ' ₸' : ''} </span>
                                <button><span class="icon-reload"></span></button>
                            </div>
                            <div class="card__selects" id="product-${product.id}">

                            </div>
                            <div class="card__add-basket"
                              id="product_add_button-${product.id}"
                              data-product="${product.id}"
                              data-variation="${product.variations[0].id}"><span class="icon-shopping-bag" ></span> В
                              корзину
                            </div>
                        </div>
                    </div>
                </div>


            `
        })
    }
    favoriteClick()
}

favoriteClick()

function favoriteClick() {
    let favoriteButtons = document.getElementsByClassName('icon-star')
    if (favoriteButtons.length) {
        for (let i = 0; i < favoriteButtons.length; i++) {
            favoriteButtons[i].addEventListener('click', async (e) => {
                e.preventDefault()
                let productId = favoriteButtons[i].getAttribute('data-product')
                const response = await axios.get(`/home/favourite/add/${productId}`)
                if (response.status === 201) {
                    // swal('Успешно', response.data.message, 'success')
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-end',
                    })
                }else{
                    // swal('Успешно', response.data.message, 'success')
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-end',
                    })

                    await getFavourites();
                }
            })
        }
    }
    let clickedButtons = document.getElementsByClassName('icon-star-bold')
    if (clickedButtons.length) {
        for (let i = 0; i < clickedButtons.length; i++) {
            clickedButtons[i].addEventListener('click', async (e) => {
                e.preventDefault()
                let productId = clickedButtons[i].getAttribute('data-product')
                const response = await axios.get(`/home/favourite/add/${productId}`)
                if (response.status === 201) {
                    let buttonFavorite = clickedButtons[i]
                    buttonFavorite.classList.remove('icon-star')
                    buttonFavorite.classList.add('icon-star-bold')

                    // swal('Успешно', response.data.message, 'success')
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-end',
                    })
                }else{
                    let buttonFavorite = clickedButtons[i]

                    buttonFavorite.classList.remove('icon-star-bold')
                    buttonFavorite.classList.add('icon-star')

                    // swal('Успешно', response.data.message, 'success')
                    Swal.fire({
                        icon: 'success',
                        title: 'Успешно',
                        text: `${response.data.message}`,
                        toast: true,
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        position: 'top-end',
                    })
                    await getFavourites();

                }
            })
        }
    }
}
