import Lang from './helpers/Lang';
import axios from 'axios';
import Swal from 'sweetalert2';

document.addEventListener('DOMContentLoaded', _ => {
    const showAddDeliveryAddressPopupButton = document.querySelector('.show-add-delivery-address-popup-button');
    const addDeliveryAddressPopup = document.querySelector('.add-delivery-address-popup');
    const saveAddressButton = document.querySelector('.add-delivery-address-popup input[type="submit"]');

    const fullAddressHiddenInput = addDeliveryAddressPopup.querySelector('input[name="full_address"]');
    const coordsHiddenInput = addDeliveryAddressPopup.querySelector('input[name="coords"]');
    const streetHiddenInput = addDeliveryAddressPopup.querySelector('input[name="street"]');
    const houseHiddenInput = addDeliveryAddressPopup.querySelector('input[name="house"]');

    const flatNumberInput = addDeliveryAddressPopup.querySelector('input[name="flat"]');
    const floorNumberInput = addDeliveryAddressPopup.querySelector('input[name="floor"]');
    const entranceNumberInput = addDeliveryAddressPopup.querySelector('input[name="entrance"]');
    const commentInput = addDeliveryAddressPopup.querySelector('textarea[name="comment"]');
    const titleInput = addDeliveryAddressPopup.querySelector('input[name="title"]');
    const userIdHiddenInput = addDeliveryAddressPopup.querySelector('input[name="user_id"]');

    if (!showAddDeliveryAddressPopupButton || !addDeliveryAddressPopup) {
        return;
    }

    if (!isAddAddressFormHasAllFields(
        fullAddressHiddenInput,
        flatNumberInput,
        floorNumberInput,
        entranceNumberInput,
        commentInput,
        titleInput,
        userIdHiddenInput,
        coordsHiddenInput,
        streetHiddenInput,
        houseHiddenInput
    )) { return; }

    /** @var ymaps */
    if (typeof ymaps === 'undefined') {
        console.error('Для работы модуля add-delivery-address необходима переменная ymaps!');
        return;
    }

    showAddDeliveryAddressPopupButton.addEventListener('click', _ => {
        showAddDeliveryAddressPopup(addDeliveryAddressPopup);
    });

    addDeliveryAddressPopup.querySelector('.close').addEventListener('click', _ => {
        hideAddDeliveryAddressPopup(addDeliveryAddressPopup);
    });

    ymaps.ready(_ => {
        initYandexMapForAddingAddress(
            addDeliveryAddressPopup,
            ymaps,
            fullAddressHiddenInput,
            coordsHiddenInput,
            streetHiddenInput,
            houseHiddenInput
        );
    });

    if (saveAddressButton) {
        saveAddressButton.addEventListener('click', async _ => {
            await saveAddress({
                user_id: userIdHiddenInput.value,
                address: fullAddressHiddenInput.value,
                flat: flatNumberInput.value,
                entrance: entranceNumberInput.value,
                floor: floorNumberInput.value,
                comment: commentInput.value,
                title: titleInput.value,
                coords: coordsHiddenInput.value,
                street: streetHiddenInput.value,
                house: houseHiddenInput.value
            });
        });
    } else {
        console.error('saveAddressButton is not defined!');
    }
});

const showAddDeliveryAddressPopup = popupNode => {
    popupNode.classList.add('show');
    popupNode.classList.remove('hide');
};

const hideAddDeliveryAddressPopup = popupNode => {
    popupNode.classList.remove('show');
    popupNode.classList.add('hide');
};

const initYandexMapForAddingAddress = (popupNode, yandexMapApi, addressHiddenInput, coordsHiddenInput, streetHiddenInput, houseHiddenInput) => {
    const yandexMapContainerId = 'add-delivery-yandex-map';

    let placeMark = null;

    if (!popupNode.querySelector(`#${yandexMapContainerId}`)) {
        console.error('Не удалось получить контейнер карты при инициализации!');
        return;
    }

    const yandexMapInstance = new yandexMapApi.Map(yandexMapContainerId, {
        center: Cookies.get('city_id') === '1' ? [76.889709, 43.238949] : [71.425134, 51.149605],
        zoom: 11,
        controls: ['zoomControl']
    });

    yandexMapInstance.events.add('click', function (e) {
        const coords = e.get('coords');

        if (placeMark) {
            placeMark.geometry.setCoordinates(coords);
        } else {
            placeMark = generatePlaceMark(yandexMapApi, coords);
            yandexMapInstance.geoObjects.add(placeMark);

            placeMark.events.add('dragend', function () {
                setAddressToPlaceMark(
                    yandexMapApi,
                    placeMark.geometry.getCoordinates(),
                    placeMark,
                    addressHiddenInput,
                    coordsHiddenInput,
                    streetHiddenInput,
                    houseHiddenInput
                );
            });
        }

        setAddressToPlaceMark(
            yandexMapApi,
            coords,
            placeMark,
            addressHiddenInput,
            coordsHiddenInput,
            streetHiddenInput,
            houseHiddenInput
        );
    });
};

const generatePlaceMark = (yandexMapApi, coords) => {
    return new yandexMapApi.Placemark(coords, {
        iconCaption: `${Lang.translate('searching')} ...`
    }, {
        preset: 'islands#redDotIconWithCaption',
        draggable: true
    });
};

const setAddressToPlaceMark = (yandexMapApi, coords, placeMark, addressHiddenInput, coordsHiddenInput, streetHiddenInput, houseHiddenInput) => {
    placeMark.properties.set('iconCaption', `${Lang.translate('searching')} ...`);

    addressHiddenInput.value = '';
    coordsHiddenInput.value = '';

    yandexMapApi.geocode(coords).then(function (res) {
        const geoObject = res.geoObjects.get(0);

        const fullAddress = geoObject.getAddressLine();
        const house = geoObject.getPremiseNumber();
        const street = geoObject.getThoroughfare();

        if (!house) {
            alert(Lang.translate('not-specified-house-number'));
            placeMark.properties.set('iconCaption', `${Lang.translate('invalid-address')}`);
            return;
        }

        if (!street) {
            alert(Lang.translate('not-specified-street'));
            placeMark.properties.set('iconCaption', `${Lang.translate('invalid-address')}`);
            return;
        }

        streetHiddenInput.value = street;
        houseHiddenInput.value = house;

        placeMark.properties.set('iconCaption', street + ', ' + house);
        addressHiddenInput.value = fullAddress;
        coordsHiddenInput.value = JSON.stringify(coords);
    });
};

const isAddAddressFormHasAllFields = (
    fullAddressHiddenInput,
    flatInput,
    floorInput,
    entranceInput,
    commentTextarea,
    titleInput,
    userIdHiddenInput,
    coordsHiddenInput,
    streetHiddenInput,
    houseHiddenInput
) => {
    if (!fullAddressHiddenInput) {
        console.error('fullAddressHiddenInput field is not defined!');
        return false;
    }

    if (!flatInput) {
        console.error('flatInput field is not defined!');
        return false;
    }

    if (!floorInput) {
        console.error('floorInput field is not defined!');
        return false;
    }

    if (!entranceInput) {
        console.error('entranceInput field is not defined!');
        return false;
    }

    if (!commentTextarea) {
        console.error('commentTextarea field is not defined!');
        return false;
    }

    if (!titleInput) {
        console.error('titleInput field is not defined!');
        return false;
    }

    if (!userIdHiddenInput) {
        console.error('userIdHiddenInput field is not defined!');
        return false;
    }

    if (!coordsHiddenInput) {
        console.error('coordsHiddenInput field is not defined!');
        return false;
    }

    if (!streetHiddenInput) {
        console.error('streetHiddenInput field is not defined!');
        return false;
    }

    if (!houseHiddenInput) {
        console.error('houseHiddenInput field is not defined!');
        return false;
    }

    return true;
};

const saveAddress = async data => {
    axios.post('/home/addresses', data)
        .then(async _ => {
            await Swal.fire({
                icon: 'success',
                title: Lang.translate('success'),
                text: Lang.translate('address-adding-success'),
                toast: true,
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true,
                position: 'top-start',
            });

            location.reload();
        })
        .catch(error => {
            Swal.fire({
                icon: 'error',
                title: Lang.translate('error-happen'),
                text: error.response.data.message,
                toast: true,
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                position: 'top-start',
            });
        });
};