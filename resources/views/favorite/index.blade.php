
@extends('layout.app')
@section('content')
    <section class="category">
        <div class="category__top">
            <div class="container">
                <div class="category__breads">
                    <a class="category__bread" href="{{route('home')}}">{{ __('content.home') }}</a>
                    <p>></p>
                    <p class="category__here">{{ __('content.favorite-list') }}</p>
                </div>
                <h1 class="category__name">{{ __('content.favorite-list') }}</h1>
                <div class="container">
                    <div class="category__row" id="favorite_row">
                        @foreach($products as $product)
                            @php
                                $variations = $product->variations()->orderBy('expired_at')
                                    ->where('1c_region_id', get_current_region_1c_id())
                                    ->where('in_stock', true)
                                    ->where('is_reserved', false)
                                    ->limit(3)->get();

                                $product->variations = $variations;

                                $product = $product->translate(App::getLocale());
                            @endphp

                            <div class="category__column">
                                <div class="card card_category" data-product-id="{{ $product->id }}" data-product="{{ json_encode($product) }}">
                                    <a href="{{ route('product', $product->id) }}" class="card__img linked_wrapper">
                                        <img src="{{ asset('storage/' . $product->main_image) }}" alt="Meat Market – {{ $product->name }}"
                                             title="Meat Market – {{ $product->name }}">
                                        <div class="card__brand">
                                            @if($product->tag !== 'none')
                                                <img src="/img/{{ $product->tag }}_icon.png" alt="">
                                            @endif
                                        </div>
                                    </a>
                                    <div class="card__content">
                                        <div class="card__top">
                                            <a class="card__top-linked-wrapper" href="{{ route('product', $product->id) }}"><h3 class="card__title">{{str_replace(['охл', 'зам', 'ОХЛ', 'ЗАМ'], '', $product->name)}}</h3></a>
                                            @if($product->group_id)
                                                <label class="switcher">
                                                    <input type="checkbox" name="is_frozen">
                                                    <span class="switcher-slide"></span>
                                                </label>
                                            @endif

                                            <div class="card__favorite">
                                                <span class="icon-star star__{{$product->id}}" data-product="{{ $product->id }}"></span>
                                            </div>
                                        </div>

                                        <div class="card__mid product-price-table-{{$product->id}}" style="display: {{ count($product['variations']) > 0 ? 'flex' : 'none' }}" id="product-price-table-{{$product->id}}">
                                            <span class="card__cost">{{ number_format($product->variations[0] ?? false ? $product->variations[0]->price : 0) }} ₸</span>
                                            <span class="card__weight">{{ $product->variations[0] ?? false ? $product->variations[0]->name : '' }} кг</span>
                                            @if(($product->variations[0] ?? false ? $product->variations[0]->bonus : 0) != 0)
                                                <span class="card__bonus">+ {{ $product->variations[0]->bonus ?? ''}} ₸</span>
                                            @else
                                                <span class="card__bonus"></span>
                                            @endif
                                            {{--                                            <button><span class="icon-reload"></span></button>--}}
                                        </div>
                                        <div class="card__selects">
                                            @foreach($variations as $variation)
                                                <button class="card__select"
                                                        data-product="{{$product->id}}"
                                                        data-variation="{{ json_encode($variation) }}">{{ $variation->name }} кг</button>
                                            @endforeach
                                        </div>

                                        <div class="card__add-basket btn-basket basket__add_button product_add_button-{{$product->id}}"
                                             data-product="{{ $product->id }}"
                                             id="product_add_button-{{$product->id}}"
                                             style="display: {{ count($product['variations']) > 0 ? 'block' : 'none' }}"
                                             data-variation='{{ $product->variations[0] ?? false ? $product->variations[0]->id : '' }}'>
                                            <span class="icon-shopping-bag"></span>
                                            {{ __('content.in-basket') }}
                                        </div>

                                        <div class="btn-basket not-in-stock" style="display: {{count($product['variations']) > 0 ? 'none' : 'block'}}">
                                            {{ __('content.not-in-stock') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{asset('js/favorite/index.js')}}"></script>
@endsection
