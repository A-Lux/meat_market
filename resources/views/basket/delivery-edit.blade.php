@extends('layout.app')
@section('content')
<input type="hidden" name="yandex-map-geo-json" value="{{ $yandexMapGeoJson }}">
<input type="hidden" name="yandex-map-center-json" value="{{ $yandexMapCenterJson }}">

<div class="basket">
    <div class="container">
        <h1 class="basket__title">{{ __('content.delivery-address') }}</h1>
        <div class="basket__content">
            <div class="basket__left">
                <form action="#" id="order_form">
                    <div class="basket__address">
                        <div class="basket__address-form">
                            @if(\Illuminate\Support\Facades\Auth::check() &&
                            count(\Illuminate\Support\Facades\Auth::user()->addresses))
                            <div class="addresses__row" id="address_panel"></div>
                            <div
                                class="addresses__column addresses__column__new show-add-delivery-address-popup-button">
                                <div class="addresses__item" style="border: 2px dashed #CECECE;">
                                    <h3>{{ __('content.add-new-address') }}</h3>
                                </div>
                            </div>
                            @else
                            <div class="contacts__form-item">
                                <div class="contacts__form-item2"></div>
                                <input minlength="1" autocomplete="new-password" required="" type="text" name="suggest"
                                       id="suggest" style=" width: 100%; ">
                                <label for="suggest">Адреса доставки</label>
                            </div>

                            <div id="yandex-map-non-auth-address-container"
                                 style="height: 400px; margin-bottom: 20px"></div>

                            <input type="hidden" name="street" class="non-auth-address">
                            <input type="hidden" name="house" class="non-auth-address">

                            <!--

                            This field needs only for correct work of delivery zone map.
                            You should not process it on the back-side.

                            -->
                            <input type="radio"
                                   name="address"
                                   class="non-auth-address"
                                   data-coords="[]"
                                   hidden>
                            <div class="addresses__form-item">
                                <input required minlength="1" type="text" name="house" id="house">
                                <label>{{ __('content.house') }}</label>
                            </div>
                            <div class="addresses__form-item">
                                <input required minlength="1" type="text" name="apartment" id="apartment">
                                <label>{{ __('content.flat') }}</label>
                            </div>
                            <div class="addresses__form-item">
                                <input required minlength="1" type="text" name="entrance" id="entrance">
                                <label>{{ __('content.entrance') }}</label>
                            </div>
                            <div class="addresses__form-item">
                                <input required minlength="1" type="text" name="floor">
                                <label>{{ __('content.floor') }}</label>
                            </div>
                            @endif
                        </div>
                    </div>
                    <section class="basket__method">
                        <h2 class="basket__method-title">{{ __('content.choose-delivery-type') }}</h2>
                        <div class="basket__method-row">
                            @foreach($delivery_types as $delivery_type)
                            <div class="basket__method-column">
                                <div class="basket__method-card">
                                    <p class="basket__method-address">
                                        {{$delivery_type->translate(App::getLocale())->name}}
                                    </p>
                                    <p class="basket__method-time2" style="    margin-top: 6px;
    font-family: Montserrat;
    font-weight: 500;
    font-size: 14px;
    color: #9D9D9D;">
                                        {{$delivery_type->start_time}} - {{$delivery_type->end_time}}
                                        <span>
                                          {{ $delivery_type->is_fast ? 'В день доставки' : 'На следующий день' }}
                                        </span>
                                    </p>
                                    <div class="basket__method-bottom">
                                        <input type="radio" name="delivery_type_id"
                                               data-price="{{ $delivery_type->id === 1 ? (request()->cookie('city_id', 1) == '1' ? setting('ekspress-dostavka.almaty-express-delivery-cost') : setting('ekspress-dostavka.nursultan-express-delivery-cost')) : 0 }}"
                                               data-need-to-show-map="{{ $delivery_type->is_fast ? '0' : '1' }}"
                                               class="delivery__click" required
                                               id="delivery-{{$loop->index}}" value="{{$delivery_type->id}}">
                                        <label for="delivery-{{$loop->index}}"></label>

                                        @if($delivery_type->is_fast)
                                        <div class="basket__method-price">
                                            {{ $delivery_type->id === 1 ? (request()->cookie('city_id', 1) == '1' ? setting('ekspress-dostavka.almaty-express-delivery-cost') : setting('ekspress-dostavka.nursultan-express-delivery-cost')) : 0
                                            }} тг
                                        </div>
                                        @else
                                        <div class="basket__method-price">
                                            {{request()->cookie('city_id', 1) == '1' ? '500-1500 тг' : '1000 тг'}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="basket__map" id="basket__map">
                            <div id="map" class="ymap"></div>
                        </div>
                    </section>

                    <section class="package">
                        <h2>{{ __('content.choose-packaging') }}</h2>

                        @php

                        $currentRegion = get_current_region_1c_id();

                        if ($currentRegion === config('regions_1c.almaty')) {
                        $basket = session()->get('basket_almaty');
                        } else if ($currentRegion === config('regions_1c.nur-sultan')) {
                        $basket = session()->get('basket_nur_sultan');
                        } else {
                        throw new Exception('Регион не найден!');
                        }

                        $freePackage = (new App\Services\OrderService())->getFreePackage($basket);

                        @endphp

                        @if($freePackage)
                        <p style="margin-bottom: 20px; font-size: 15px; line-height: 18px; color: #4fb41f">
                            {!! __('content.free-package-message', ['package_name' => $freePackage->name]) !!}
                        </p>

                        <input type="hidden" name="is_free_package" value="1">
                        @endif

                        <div class="package__row">
                            @foreach($package_type as $type)
                            @php $type = $type->translate(App::getLocale()) @endphp
                            <div class="package__column">
                                <div class="package__item">
                                    <p>{{$type->name}}</p>
                                    <div class="package__img">
                                        <img src="{{ asset('storage/' . $type->image) }}" alt="">
                                    </div>
                                    <div class="package__radio">
                                        <input type="radio" name="package_type_id" data-name="{{$type->name}}" data-price="{{$type->additional_price}}" required class="package__click"
                                               data-addition-cost="{{ $type->additional_price }}"
                                               id="package-{{$loop->index}}" value="{{$type->id}}">
                                        <label for="package-{{$loop->index}}"></label>
                                        <p>{{$type->additional_price == 0 ? '0 ₸' : $type->additional_price. ' ₸'}} </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </section>

                    <section class="basket__method">
                        <h2 class="basket__method-title">{{ __('content.choose-who-to-deliver') }}</h2>
                        <div class="basket__method-row">
                            <div class="basket__method-column">
                                <div class="basket__method-card">
                                    <p class="basket__method-address">
                                        {{ __('content.for-self') }}
                                    </p>
                                    <div class="basket__method-bottom">
                                        <input required type="radio" name="delivery-info" id="delivery-my"
                                               value="Для себя">
                                        <label class="delivery-my" for="delivery-my"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="basket__method-column">
                                <div class="basket__method-card">
                                    <p class="basket__method-address">
                                        {{ __('content.not-for-self') }}
                                    </p>
                                    <div class="basket__method-bottom">
                                        <input required type="radio" name="delivery-info" id="delivery-you"
                                               value="Не для себя">
                                        <label class="delivery-you" for="delivery-you"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="basket__method">
                        <h2 class="basket__method-title">{{ __('content.choose-time') }}</h2>
                        <div class="basket__method-row">
                            <div class="basket__method-column">
                                <div class="basket__method-card">
                                    <p class="basket__method-address">12:00 – 15:00</p>
                                    <div class="basket__method-bottom">
                                        <input type="radio" required name="delivery-time" value="12:00 – 15:00"
                                               id="first-part-of-day">
                                        <label class="first-part-of-day" for="first-part-of-day"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="basket__method-column">
                                <div class="basket__method-card">
                                    <p class="basket__method-address">16:00 – 19:00</p>
                                    <div class="basket__method-bottom">
                                        <input type="radio" required name="delivery-time" value="16:00 – 19:00"
                                               id="second-part-of-day">
                                        <label class="second-part-of-day" for="second-part-of-day"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="basket__method">
                        <h2 class="basket__method-title">{{ __('content.choose-payment-type') }}</h2>

                        <select name="payment_type_id">
                            @foreach(\App\Models\PaymentType::all() as $item)
                            <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                            @endforeach
                        </select>
                    </section>

                    <section class="contacts">
                        <h2>{{ __('content.contact-info') }}</h2>
                        <div class="contacts__form">
                            <div class="contacts__form-item">
                                <div class="contacts__form-item2"></div>
                                <input minlength="1" required type="text" name="name" id="name">
                                <label for="name">{{ __('content.name') }}</label>
                            </div>
                            @if(Illuminate\Support\Facades\Auth::check())
                            <input class="AuthCheck" type="hidden" name="user_id"
                                   value="{{\Illuminate\Support\Facades\Auth::id()}}">
                            @endif
                            <div class="contacts__form-item">
                                <input minlength="1" required type="text" name="last_name" id="last_name">
                                <label for="lastname">{{ __('content.last-name') }}</label>
                            </div>
                            <div class="contacts__form-item">
                                <input minlength="1" required type="text" name="email" id="email">
                                <label for="email">{{ __('content.email') }}</label>
                            </div>
                            <div class="contacts__form-item">
                                <input minlength="1" required name="phone" id="phone" type="text"
                                       aria-invalid="tel">
                                <label for="tel">{{ __('content.phone') }}</label>
                            </div>
                            <div class="contacts__form-item">
                                <input required type="date" name="date_of_delivery" id="date_of_delivery"
                                       value="{{ Carbon\Carbon::now()->addDay()->format('Y-m-d') }}">
                                <label for="delivery">{{ __('content.delivery-date') }}</label>
                            </div>
                            <div class="contacts__form-item">
                                <input type="text" name="comment" id="comment">
                                <label for="delivery">{{ __('content.comment') }}</label>
                            </div>
                        </div>
                        <div class="contacts__btn desc">
                            @if(session()->has('discount_code'))
                            <input type="hidden" name="discount_code_id"
                                   value="{{session()->get('discount_code')->id}}">
                            @endif
                            <div class="contacts__btn">
                                <button id="submit_btn">{{ __('content.continue-with-order') }}</button>
                            </div>
                        </div>
                    </section>

                    <input hidden type="text" name="city_id" value="{{ request()->cookie('city_id', 1) }}">
                </form>
            </div>

            <div class="basket__right">
                <h2>{{ __('content.order') }}</h2>
                <div class="basket__info" id="basket-info-new">
                    <p>({{$totals['total_quantity']}}) {{ __('content.products') }}</p>
                    <p>{{$totals['total_sum']}} ₸</p>
                    <p id="packName"></p>
                    <p id="packPrice"></p>
                    <p id="delivery-name"></p>
                    <p id="delivery-price"></p>

                    <p style="display: none">
                        @auth
                        {{ __('content.spend-bonuses') }} <span>{{\Illuminate\Support\Facades\Auth::user()->balance}} ₸
                            </span>
                        @endauth
                    </p>



                    <p class="basket__slider" style="display: none">
                        @auth
                        <label class="switch">
                            <input type="checkbox" id="use_bonus"
                                   data-bonus="{{ \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->balance : 0 }}"
                                   data-used="false">
                            <span class="slider"></span>
                        </label>
                        @endauth
                    </p>
                    <p style="display: none">{{ __('content.total-amount') }}</p>
                    @if(session()->has('discount_code'))
                    @php $discount_points = session()->get('discount_code')->discount_points @endphp
                    <p><span class="basket__total"
                             id="basket__totals"
                             data-current-price="{{ $totals['total_sum'] - $discount_points }}"
                             data-total="{{ $totals['total_sum'] - $discount_points }}">
                                {{ $totals['total_sum'] - $discount_points }} ₸</span>
                    </p>
                    @else
                    <p><span class="basket__total"
                             id="basket__totals"
                             data-current-price="{{ $totals['total_sum'] }}"
                             data-total="{{ $totals['total_sum'] }}">
                                {{ $totals['total_sum']}} ₸</span>
                    </p>
                    @endif
                </div>
                <div class="contacts__btn mob">
                    <button id="submit_btn">{{ __('content.continue-with-order') }}</button>
                </div>
                <div class="basket__bottom">
                    <div class="basket__url">
                        <a href="#">{{ __('content.online-payments-rules') }}</a>
                        <a href="#">{{ __('content.privacy-policy') }}</a>
                        <a href="#">{{ __('content.public-offer') }}</a>
                    </div>
                    <div class="basket__logo">
                        <img src="{{asset('img/icons/kaspi.png')}}" alt="">
                        <img src="{{asset('img/icons/visa.png')}}" alt="">
                        <img src="{{asset('img/icons/mastercard.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('/js/address/address.js')}}"></script>
    <script
        src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;coordorder=longlat&amp;apikey=07311927-f7d1-4db9-8e33-25781f62904c"></script>
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
    @endsection
