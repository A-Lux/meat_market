<div class="basket">
    <div class="container">
        <h1 class="basket__title">Адрес доставки</h1>
        <div class="basket__content">
        <div class="basket__left">
           <div class="basket__address">
            <form action="#">
                <div class="addresses__row">
                    <div class="addresses__column">
                        <div class="addresses__item">
                            <h3>Казахстан, с. Сарыозек,
                                ул. айту 18/8</h3>
                                <div class="addresses__func">
                                        <input checked type="radio" name="address" id="address-1" value="address-1">
                                        <label for="address-1"><span>Основной адрес</span></label>
                                        <div class="addresses__icons">
                                            <div class="addresses__edit">
                                                <span class="icon-edit"></span>
                                            </div>
                                            <div class="addresses__delete">
                                                <span class="icon-delete"></span>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                    <div class="addresses__column">
                        <div class="addresses__item">
                            <h3>Казахстан, с. Сарыозек,
                                ул. айту 18/8</h3>
                                <div class="addresses__func">
                                        <input type="radio" name="address" id="address-2" value="address-2">
                                        <label for="address-2"><span>Основной адрес</span></label>
                                        <div class="addresses__icons">
                                            <div class="addresses__edit">
                                                <span class="icon-edit"></span>
                                            </div>
                                            <div class="addresses__delete">
                                                <span class="icon-delete"></span>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="addresses__btn">
                <button class="addresses__add">
                    Создать новый адрес
                </button>
            </div>
            </form>
           </div>
           <section class="basket__method">
               <h2 class="basket__method-title">ВЫберите способ доставки</h2>
               <div class="basket__method-row">
                   <div class="basket__method-column">
                       <div class="basket__method-card">
                           <p class="basket__method-address">
                            Стандартная доставка
                            по г. Нур-Султан, г. Алматы
                           </p>
                           <p class="basket__method-time">
                            c 11:00 до 18:00
                            <span>На следующий день</span>
                           </p>
                           <div class="basket__method-bottom">
                               <input type="radio" name="delivery" id="delivery">
                               <label for="delivery"></label>
                               <div class="basket__method-price">540 ₸</div>
                           </div>
                       </div>
                   </div>
                   <div class="basket__method-column">
                    <div class="basket__method-card">
                        <p class="basket__method-address">
                        Экспресс доставка
                        по г. Нур-Султан, г. Алматы
                        </p>
                        <p class="basket__method-time">
                         2 часа
                        </p>
                        <div class="basket__method-bottom">
                            <input type="radio" name="delivery" id="delivery-2">
                            <label for="delivery-2"></label>
                            <div class="basket__method-price">1990 ₸</div>
                        </div>
                    </div>
                </div>
               </div>
               <div class="basket__map">
                <div id="map" class="ymap" ></div>
            </div>
           </section>
           <section class="package">
               <h2>ВЫберите Упаковку</h2>
               <div class="package__row">
                   <div class="package__column">
                       <div class="package__item">
                           <p>Стандартная упаковка</p>
                           <div class="package__img">
                               <img src="img/package-1.png" alt="">
                           </div>
                           <div class="package__radio">
                               <input type="radio" name="package" id="package-1">
                               <label for="package-1"></label>
                               <p>Бесплатно</p>
                           </div>
                       </div>
                   </div>
                   <div class="package__column">
                    <div class="package__item">
                        <div class="package__item-top">
                        <p>Подарочная упаковка</p>
                        <p class="package__num"><span>01</span></p>
                    </div>
                        <div class="package__img">
                            <img src="img/package-2.png" alt="">
                        </div>
                        <div class="package__radio">
                            <input type="radio" name="package" id="package-2">
                            <label for="package-2"></label>
                            <p class="package__cost">1 990 ₸</p>
                        </div>
                    </div>
                </div>
               </div>
           </section>
           <section class="contacts">
               <h2>КОНТАКТНАЯ ИНФОРМАЦИЯ</h2>
               <div class="contacts__form">
                   <div class="contacts__form-item">
                   <input minlength="1" required name="lastname" id="lastname" type="text" name="name">
                   <label for="name">Имя</label>
                </div>
                <div class="contacts__form-item">
                   <input minlength="1" required name="lastname" id="lastname" type="text" name="lastname">
                   <label for="lastname">Фамилия</label>
                </div>
                    <div class="contacts__form-item">
                   <input minlength="1" required name="lastname" id="lastname" type="text" name="email">
                   <label for="email">Эл. почта</label>
                </div>
                <div class="contacts__form-item">
                    <input minlength="1" required name="tel" id="tel" type="text" name="tel">
                    <label for="tel">Номер телефона</label>
                 </div>
                    <div class="contacts__form-item">
                   <input required type="date" name="delivery">
                   <label for="delivery">Дата доставки</label>
                </div>
                <div class="contacts__form-item">
                   <input type="text" name="comment">
                   <label for="delivery">Комментарий</label>
                </div>
               </div>
               <div class="contacts__btn desc">
                    <button>Продолжить оформление</button>
               </div>
           </section>
        </div>
        <div class="basket__right">
            <h2>Заказ</h2>
            <div class="basket__info">
                <p>(6) Товара</p>
                <p>3 700 ₸</p>
                <p>Доставка</p>
                <p>- 700 ₸</p>
                <p>Потратить бонусы <span>180 ₸</span></p>
                <p class="basket__slider" style="display: none;">
                    <label class="switch">
                        <input type="checkbox" checked>
                        <span class="slider"></span>
                    </label>
                </p>
                <p>К оплате</p>
                <p><span class="basket__total">4 200 ₸</span></p>
            </div>
            <div class="contacts__btn mob">
                <button>Продолжить оформление</button>
           </div>
            <div class="basket__bottom">
                <div class="basket__url">
                    <a href="#">Правила онлайн-оплаты</a>
                    <a href="#">Политика конфиденциальности</a>
                    <a href="#">Публичная оферта</a>
                </div>
                <div class="basket__logo">
                    <img src="img/icons/kaspi.png" alt="">
                    <img src="img/icons/visa.png" alt="">
                    <img src="img/icons/mastercard.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
</div>
