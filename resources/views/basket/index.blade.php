@extends('layout.app')
@section('content')
    <div class="basket">
        <div class="container">
            <h1 class="basket__title">{{ __('content.basket') }}</h1>
            <div class="basket__content">
                <div class="basket__left">
                    <div class="basket__row" id="basket-row"></div>
                    <div class="basket__row desc" >
                        <div class="basket__promo">
                            <form action="/order/delivery-edit">
                                <input type="text" name="promocode" id="promocode" class="promocodeValue" placeholder="{{ __('content.promocode') }}">
                                  <label class="promocode">
                                    <input type="checkbox" class="promocodeBtn" >
                                    <span class="checkmark"></span>
                                  </label>
                                <a href="{{route('order')}}">
                                    <button type="submit">{{ __('content.checkout') }}</button>
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="basket__right">
                    <h2>{{ __('content.order') }}</h2>
                    <div class="basket__info" id="basket-info">
                        <p>(6) {{ __('content.products') }}</p>
                        <p>3 700 ₸</p>
                        <p>{{ __('content.sale') }}</p>
                        <p>- 700 ₸</p>
                        <p>{{ __('content.bonuses') }}</p>
                        <p>+ 180 ₸</p>
                        <p>{{ __('content.total-amount') }}</p>
                        <p>3 000 ₸</p>
                    </div>
                    <div class="basket__info">
                        <p>{{ __('content.promocode') }}</p>
                        <p class="promocode__info"></p>
                    </div>
                    <div class="basket__row mob" >
                        <div class="basket__promo">
                            <form action="/order/delivery-edit">
                                <input type="text" name="promocode" id="promocode" placeholder="{{ __('content.promocode') }}">
                                <a href="{{route('order')}}">
                                    <button type="submit">{{ __('content.checkout') }}</button>
                                </a>
                            </form>
                        </div>
                    </div>
                    <div class="basket__bottom">
                        <div class="basket__url">
                            <a href="/public-offer" target="_blank">{{ __('content.public-offer') }}</a>
                            <a href="/privacy-policy" target="_blank">{{ __('content.privacy-policy') }}</a>
                            <a href="/online-payments" target="_blank">{{ __('content.online-payments-rules') }}</a>
                        </div>
                        <div class="basket__logo">
                            <img src="img/icons/kaspi.png" alt="">
                            <img src="img/icons/visa.png" alt="">
                            <img src="img/icons/mastercard.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="basket__footer">
            <div class="basket__footer-price">
                <p class="basket__total-price">21 700 ₸</p>
                <span>1.70 кг</span>
                <div class="basket__score">+ 70 ₸</div>
            </div>
            <div class="basket__weight-select">
                <div>1.32 кг</div>
                <div>1.70 кг</div>
                <div>3.84 кг</div>
                <div><span class="icon-reload"></span></div>
            </div>
            <button class="basket__footer-btn"><span class="icon-shopping-bag"></span>{{ __('content.basket') }}</button>
        </div>
    </div>
@endsection
