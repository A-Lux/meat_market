@extends('layout.app')
@section('content')
    <section class="password__recovery">
        <div class="container">
            <h1>{{ __('content.password-recovery') }}</h1>
            <p class="password__recovery-description">Если вы забыли пароль от своего аккаунта, то мы отправим
                на Вашу эл. почту ссылку на восстановление доступа.</p>
        </div>
    </section>
@endsection