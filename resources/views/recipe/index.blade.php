@extends('layout.app')
@section('content')
    <section class="recipe">
        <div class="category__top recipe__top">

            <div class="container">
                <div class="category__breads recipe__breads">
                    <a class="category__bread" href="{{route('home')}}">{{ __('content.home') }}</a>
                    <p>></p>
                    <a class="category__bread" href="{{route('tipsAndRecipes')}}">{{ __('content.tips-and-recipes') }}</a>
                    <p>></p>
                    <p class="category__here">{{$article->name}}</p>
                </div>

                @if($article->ingredients)
                    <div class="ingredients__content">
                        <div class="container" style="font-size: 1.2em;">
                            <strong style="text-transform: uppercase">{{ __('content.ingredients') }}:</strong>

                            <div class="ingredients__content-wrapper">
                                @foreach(json_decode($article->ingredients) as $item)
                                    <img style="width: 110px" src="{{ asset('storage/' . $item) }}" alt="Meat Market">
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

                <div class="recipe__content">
                    <div class="container" style="font-size: 1.2em; line-height: 25px">
                        {!! $article->content !!}
                    </div>
                </div>
            </div>

            @if($article->images)
                <div class="swiper-container" style="margin-bottom: 40px">
                    <div class="swiper-wrapper">
                        @foreach(json_decode($article->images) as $item)
                            <img style="width: 80%" src="{{ asset('storage/' . $item) }}" alt="Meat Market" class="swiper-slide">
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
