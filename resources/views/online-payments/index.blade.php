@extends('layout.app')
@section('content')
    <section class="public-offer">
        <div class="container">
            <h1 class="basket__title" style="margin-bottom: 15px !important;">{{ __('content.online-payments-rules') }}</h1>
            {!! $content !!}
        </div>
    </section>
@endsection