@extends('layout.app')
@section('content')
    <section class="category">
        <div class="category__banner">
            <div class="container">
                <div class="category__breads">
                    <a class="category__bread" href="{{route('home')}}">{{ __('content.home') }}</a>
                    <p>></p>
                    @if($isSale)
                        <p class="category__here">{{ __('content.sale-products') }}</p>
                    @else
                        <p class="category__here">
                            {{isset($id) ? $id->translate(\Illuminate\Support\Facades\App::getLocale())->name : $part->name}}
                        </p>
                    @endif
                </div>
                @if($isSale)
                    <h1 class="category__name">{{ __('content.sale-products') }}</h1>
                @else
                    <h1 class="category__name">
                        {{isset($id) ? $id->translate(\Illuminate\Support\Facades\App::getLocale())->name : $part->name}}
                    </h1>
                @endif
                <div class="category__line">
                    <div class="category__inputs">
                        <form action="#">
                            <input type="search" name="search" id="search" placeholder="{{ __('content.search') }}...">
                            <button type="submit"><span class="icon-search"></span></button>
                        </form>
                    </div>
                    <div class="category__sort" style="display: none">
                        <div class="placeholder">{{ __('content.sort-by') }}:</div>
                        <select name="sort" id="sort">
                            <option value="asc">{{ __('content.high-price') }}</option>
                            <option value="desc">{{ __('content.low-price') }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="category__row" id="main_category_panel"></div>
            <div class="category__row">
                <a href="" class="category__more more" id="show_more">{{ __('content.show-more-products') }}<span class="icon-line"></span></a>
            </div>
        </div>
    </section>

    <input type="hidden" id="category_id" value="{{ $id ? $id->id : 'sale' }}">

    <script src="{{asset('js/product/main.js')}}"></script>
    <script src="{{ asset('js/product-mode-switching.js') }}"></script>
@endsection
