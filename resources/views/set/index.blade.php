@extends('layout.app')
@section('content')
    <section class="category">
        <div class="category__banner">
            <div class="container">
                <div class="category__breads">
                    <a class="category__bread" href="{{route('home')}}">{{ __('content.home') }}</a>
                    <p>></p>
                    <p class="category__here">{{ __('content.all-sets-title') }}</p>
                </div>
                <h1 class="category__name">{{ __('content.all-sets-title') }}</h1>
{{--                <div class="category__line">--}}
{{--                    <div class="category__inputs">--}}
{{--                        <form action="#" >--}}
{{--                            <input type="search" name="search" id="search" placeholder="{{ __('content.search') }}...">--}}
{{--                            <button type="submit"><span class="icon-search"></span></button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                    <div class="category__sort">--}}
{{--                        <div class="placeholder">{{ __('content.sort-by') }}:</div>--}}
{{--                        <select name="sort" id="sort">--}}
{{--                            <option value="asc">{{ __('content.high-price') }}</option>--}}
{{--                            <option value="desc">{{ __('content.low-price') }}</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="container">
            <div class="category__row" id="main_category_panel">
                @foreach($sets as $set)
                    <div class="category__column">
                        <div class="card card_category">
                            <a href="{{ route('set', $set->id) }}">
                                <div class="card__img">
                                    <img src="{{ asset('storage/' . $set->preview_image) }}" alt="Meat Market – {{ $set->name }}" title="{{ $set->name }}">
{{--                                    <div class="card__brand">--}}
{{--                                        <img src="/img/prime_icon.png" alt="">--}}
{{--                                    </div>--}}
                                </div>
                            </a>

                            <div class="card__content">
                                <div class="card__top">
                                    <a href="{{ route('set', $set->id) }}"><h3 class="card__title">{{ $set->name }}</h3></a>
{{--                                    <div class="card__favorite" ><span class="icon-star" data-product="${product.id}"> </span></div>--}}
                                </div>

                                <p class="card__content-description">{{ $set->short_description }}</p>

                                <div class="card__mid" id="product-price-table-${product.id}">
                                    <span class="card__cost">{{ number_format($set->price) }}</span>
{{--                                    <span class="card__weight">${product.variations[0].name} кг</span>--}}
{{--                                    <span class="card__bonus">${product.variations[0].bonus != null ? product.variations[0].bonus + ' ₸' : ''} </span>--}}
{{--                                    <button><span class="icon-reload"></span></button>--}}
                                </div>
{{--                                <div class="card__selects" id="product-${product.id}">--}}

{{--                                </div>--}}
                                <div class="card__add-basket"
                                     id="product_add_button"
                                     data-product="{{ $set->fakeProduct['id'] }}"
                                     data-variation="-1">
                                    <span class="icon-shopping-bag"></span> {{ __('content.in-basket') }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
{{--            <div class="category__row">--}}
{{--                <a href="" class="category__more more" id="show_more">{{ __('content.show-more') }}<span class="icon-line"></span></a>--}}
{{--            </div>--}}
        </div>
    </section>

    <script src="{{asset('js/set/main.js')}}"></script>
@endsection
