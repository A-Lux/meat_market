@extends('layout.app')
@section('content')
    <section class="product">
        <div class="category__top product__top">
            <div class="container">
                <div class="category__breads product__breads">
                    <a class="category__bread" href="{{route('home')}}">{{ __('content.home') }}</a>
                    <p>></p>
                    <a class="category__bread" href="{{ route('sets') }}">
                        {{ __('content.all-sets') }}
                    </a>
                    <p>></p>
                    <p class="category__here">{{ $set->name }}</p>
                </div>
                <div class="product__content">
                    <div class="product__images">
                        <div class="product__main-img">
                            <img src="/storage/{{ $set->preview_image }}" alt="Товар" title="Товар">
{{--                            <div class="product__buttons">--}}
{{--                                <div class="product__like"><img class="product__like-img"--}}
{{--                                                                src="{{asset('img/icons/heart.svg')}}"--}}
{{--                                                                alt=""><span>120</span></div>--}}
{{--                                <div class="product__main-favorite"><span class="icon-star"></span></div>--}}
{{--                            </div>--}}
                        </div>
{{--                        <div class="product-additionaly">--}}
{{--                            @if($product->images != null )--}}
{{--                                @foreach(json_decode($product->images) as $image)--}}
{{--                                    <img src="{{asset('storage/'.$image)}}" alt="">--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
{{--                        </div>--}}
                    </div>
                    <div class="product__info">
                        <h1 class="product__title">{{ $set->name }}</h1>
{{--                        <div class="product__data">--}}
{{--                            @if(count($product->variations))--}}
{{--                                <p class="product__price">{{number_format($product->variations[0]->price,0)}} ₸</p>--}}
{{--                                <p class="product__weight">{{$product->variations[0]->name}} {{ __('content.kilogram') }}</p>--}}
{{--                            @else--}}
{{--                                <p class="product__price">{{number_format($product->price,0)}} ₸</p>--}}

{{--                            @endif--}}
{{--                        </div>--}}
                        <p class="product__descr">
                            {{ $set->description }}
                        </p>
{{--                        <div class="product__tables">--}}
{{--                            <div class="product__table">--}}
{{--                                <p class="product__key">{{ __('content.breed') }}</p>--}}
{{--                                <p class="product__value">{{$product->breed->name}}</p>--}}
{{--                            </div>--}}
{{--                            <div class="product__table">--}}
{{--                                <p class="product__key">{{ __('content.feed') }}</p>--}}
{{--                                <p class="product__value">{{$product->fattening}}</p>--}}
{{--                            </div>--}}
{{--                            <div class="product__table">--}}
{{--                                <p class="product__key">{{ __('content.storing') }}</p>--}}
{{--                                <p class="product__value">{{$product->storage->name}}</p>--}}
{{--                            </div>--}}
                            {{--                            <div class="product__table">--}}
                            {{--                                <p class="product__key">СЕТ</p>--}}
                            {{--                                <p class="product__value-set">1 сет</p>--}}
                            {{--                            </div>--}}
{{--                        </div>--}}
                        <div class="product__pay">
                                <p class="product__price-black">
                                    {{ number_format($set->price) }} ₸
                                </p>

                                @php
                                    /** @var App\Models\Set|null $set */
                                    $set = App\Models\Set::find($set->id);

                                    /** @var App\Models\Product|null $fakeProduct */
                                    if ($fakeProduct = $set->fakeProduct()->first()) {
                                        $fakeProductId = $fakeProduct->id;
                                    }
                                @endphp

                                <button class="btn-basket product__add-basket"
                                        id="basket_add_button"
                                        data-product="{{ $fakeProductId ?? null }}"
                                        data-variation="-1"
                                >{{ __('content.add-to-basket') }}</button>
                        </div>
{{--                        <div class="product__more">--}}
{{--                            <h2 class="product__subtitle">{{ __('content.extra-products') }}</h2>--}}
{{--                            <div class="product__items">--}}
{{--                                @foreach($product->additional_products as $additional_product)--}}

{{--                                    <div class="product__item">--}}
{{--                                        <div class="product__left-additional">--}}
{{--                                            <div class="product__mini">--}}
{{--                                                <img src="{{$additional_product->main_image}}" alt="">--}}
{{--                                            </div>--}}
{{--                                            <div class="product__mini-info">--}}
{{--                                                <h3 class="product__name">--}}
{{--                                                    {{$additional_product->name}}--}}
{{--                                                </h3>--}}
{{--                                                @if($additional_product->variations != [])--}}
{{--                                                    <div class="product__mini-price">--}}
{{--                                                        {{number_format($additional_product->variations[0]->price,0)}} ₸--}}
{{--                                                        <span>{{$additional_product->variations[0]->name}} кг</span>--}}
{{--                                                    </div>--}}
{{--                                                @endif--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="product__right-additional">--}}
{{--                                            <div class="product__counts">--}}
{{--                                                <span class="product__minus" data-product="{{$additional_product->id}}">−</span>--}}
{{--                                                <p id="product-counter-{{$additional_product->id}}">1</p>--}}
{{--                                                <span class="product__plus"--}}
{{--                                                      data-product="{{$additional_product->id}}">+</span>--}}
{{--                                            </div>--}}
{{--                                            @if(count($product->variations))--}}

{{--                                                <button class="product__btn product_btn_add"--}}
{{--                                                        id="product-btn-add-{{$additional_product->id}}"--}}
{{--                                                        data-product="{{$additional_product->id}}"--}}
{{--                                                        data-quantity="1"--}}
{{--                                                        data-variation="{{$additional_product->variations[0]->id}}">--}}
{{--                                                    {{ __('content.add') }}</button>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Состав сета -->
    <section class="similar">
        <div class="container">
            <h2 class="similar__title title">
                {{ __('content.included-products') }}
            </h2>
            <div class="similar__row swiper-container">
                <div class="swiper-wrapper">
                    @foreach($set->products ?? [] as $product)
                        <div class="swiper-slide">
                            <div class="similar__column">
                                <div class="similar__item">
                                    <div class="similar__img">
                                        <img src="{{ asset('storage/' . $product->main_image) }}" alt="Meat Market – {{ $product->name }}">
                                    </div>
                                    <h3 class="similar__name">{{ $product->name }}</h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
