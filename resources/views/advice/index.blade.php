<section class="advice">
    <div class="container">
        <div class="advice__breads">
            <a href="#">Главная</a>
            <span>></span>
            <p>Советы и рецепты</p>
        </div>
        <h1 class="advice__title">Советы и <span>рецепты</span></h1>
        <div class="advice__block">
            <div class="advice__img">
                <img src="img/advice.jpg" alt="МяСо">
                <div class="advice__info">
                    <time>17.05.20</time>
                    <h2>СЭНДВИЧ ТОПСАЙД</h2>
                    <a href="#" class="advice__more more">Подробнее <span class="icon-line"></span></a>
                </div>
            </div>
        </div>
        <div class="advice__block-right">
            <div class="advice__img">
            <img src="img/advice.jpg" alt="МяСо">
            <div class="advice__info">
                <time>17.05.20</time>
                <h2>СТЕЙК МЕРЛО</h2>
                <a href="#" class="advice__more more">Подробнее <span class="icon-line"></span></a>
            </div>
        </div>
        </div>
    </div>
</section>
