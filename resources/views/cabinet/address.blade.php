@extends('layout.app')
@section('content')
    <div class="cabinet">
        <div class="container">
            <div class="cabinet__wrapper">
                <div class="cabinet__menu">
                    <h2 style="text-transform: uppercase">{{ __('content.menu') }}</h2>
                    <div class="cabinet__nav">
                        <div class="cabinet__item ">
                            <a href="{{route('profile')}}"><span class="icon-profile"></span>{{ __('content.personal-info') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('history')}}"><span class="icon-time"></span>{{ __('content.orders-history') }}</a>
                        </div>
                        <div class="cabinet__item cabinet__item_active">
                            <a href="{{route('addresses')}}"><span class="icon-address"></span>{{ __('content.saved-addresses') }}</a>
                        </div>
                        <div class="cabinet__item" style="display: none;">
                            <a href="{{route('refPage')}}"><span class="icon-users"></span>{{ __('content.ref-system') }}</a>
                        </div>
                        {{-- <div class="cabinet__item">
                            <a href="{{route('quests')}}"><span class="icon-wallet"></span>{{ __('content.earn-bonuses') }}</a>
                        </div> --}}
                    </div>
                </div>
                <div class="cabinet__content">
                    <p class="cabinet__bonus">{{ __('content.bonuses') }} {{\Illuminate\Support\Facades\Auth::user()->balance}} ₸</p>
                    <div class="cabinet__content-header-wrapper">
                        <h2 style="text-transform: uppercase">{{ __('content.saved-addresses') }}</h2>
                        <a href="{{ route('logout') }}" class="logout__button">{{ __('content.logout') }}</a>
                    </div>
                    <div class="addresses">
                        <div class="addresses__row" id="address_panel">
                            @foreach($addresses as $address)
                            <div class="addresses__column">
                                <div class="addresses__item">
                                    <h3>{{$address->name}}</h3>
                                    <div class="addresses__func">
                                        <input @if($address->is_main == true )checked @endif type="radio" class="addressMainChooseButton" name="address" id="address-{{$address->id}}" value="{{$address->id}}">
                                        <label for="address-1"><span>{{ __('content.main-address') }}</span></label>
                                        <div class="addresses__icons">
                                            <div class="addresses__edit">
                                                <span class="icon-edit"></span>
                                            </div>
                                            <div class="addresses__delete" data-address="{{$address->id}}">
                                                <span class="icon-delete"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="addresses__btn">
                            <button class="addresses__add show-add-delivery-address-popup-button">{{ __('content.add-new-address') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('/js/address/address.js')}}"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;coordorder=longlat&amp;apikey=07311927-f7d1-4db9-8e33-25781f62904c"></script>
@endsection
