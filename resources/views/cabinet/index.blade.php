
@extends('layout.app')
@section('content')
    <div class="cabinet">
        <div class="container">
            <div class="cabinet__wrapper">
                <div class="cabinet__menu">
                    <h2 style="text-transform: uppercase">{{ __('content.menu') }}</h2>
                    <div class="cabinet__nav">
                        <div class="cabinet__item cabinet__item_active">
                            <a href="#"><span class="icon-profile"></span>{{ __('content.personal-info') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('history')}}"><span class="icon-time"></span>{{ __('content.orders-history') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('addresses')}}"><span class="icon-address"></span>{{ __('content.saved-addresses') }}</a>
                        </div>
                        <div class="cabinet__item" style="display: none;">
                            <a href="{{route('refPage')}}"><span class="icon-users"></span>{{ __('content.ref-system') }}</a>
                        </div>
                        {{-- <div class="cabinet__item">
                            <a href="{{route('quests')}}"><span class="icon-wallet"></span>{{ __('content.earn-bonuses') }}</a>
                        </div> --}}
                    </div>
                </div>
                <div class="cabinet__content">
                    <p class="cabinet__bonus">{{ __('content.bonuses') }}: {{\Illuminate\Support\Facades\Auth::user()->balance}} ₸</p>
                    <div class="cabinet__content-header-wrapper">
                        <h2 style="text-transform: uppercase">{{ __('content.personal-info') }}</h2>
                        <a href="{{ route('logout') }}" class="logout__button">{{ __('content.logout') }}</a>
                    </div>
                    <form class="cabinet__form" id="user_data_form">
                        {{csrf_field()}}

                        <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                        <div class="cabinet__form-item">
                            <input type="text" required minlength="2" value="{{\Illuminate\Support\Facades\Auth::user()->name}}" name="name" id="name">
                            <label for="name">{{ __('content.name') }}</label>
                        </div>
                        <div class="cabinet__form-item">
                            <input type="text" minlength="2" required name="last_name" id="lastname" value="{{\Illuminate\Support\Facades\Auth::user()->last_name}}">
                            <label for="lastname">{{ __('content.last-name') }}</label>
                        </div>
                        <div class="cabinet__form-item">
                            <input type="email" required name="email" id="email" value="{{\Illuminate\Support\Facades\Auth::user()->email}}">
                            <label for="email">{{ __('content.email') }}</label>
                        </div>
                        <div class="cabinet__form-item">
                            <input type="text" required name="phone" id="telephone" value="{{\Illuminate\Support\Facades\Auth::user()->phone}}">
                            <label for="telephone">{{ __('content.phone') }}</label>
                        </div>
                        <div class="cabinet__form-item">
                            <input type="date" required name="birthday" id="date" value="{{\Illuminate\Support\Facades\Auth::user()->birthday}}">
                            <label for="date">{{ __('content.birthday') }}</label>
                        </div>
                        <div class="cabinet__gender">
                            <div class="cabinet__wrapper">
                                <div>
                                    <input  id="female" name="sex" @if(!\Illuminate\Support\Facades\Auth::user()->sex) checked @endif type="radio"  value="0" >
                                    <label for="female">{{ __('content.female') }}</label>
                                </div>
                                <div>
                                    <input name="sex" id="male" type="radio" @if(\Illuminate\Support\Facades\Auth::user()->sex) checked @endif  value="1">
                                    <label for="male">{{ __('content.male') }}</label>
                                </div>
                            </div>
                            <input type="hidden" name="sex" value="{{\Illuminate\Support\Facades\Auth::user()->sex}}" id="sex" >
                            <div>
                                <input type="submit" value="{{ __('content.save') }}" id="update_user_data">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/user/index.js')}}"></script>
@endsection
