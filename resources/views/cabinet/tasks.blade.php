@extends('layout.app')
@section('content')
    <div class="cabinet">
        <div class="container">
            <div class="cabinet__wrapper">
                <div class="cabinet__menu">
                    <h2 style="text-transform: uppercase">{{ __('content.menu') }}</h2>
                    <div class="cabinet__nav">
                        <div class="cabinet__item ">
                            <a href="{{route('profile')}}"><span class="icon-profile"></span>{{ __('content.personal-info') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('history')}}"><span class="icon-time"></span>{{ __('content.orders-history') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('addresses')}}"><span class="icon-address"></span>{{ __('content.saved-addresses') }}</a>
                        </div>
                        <div class="cabinet__item" style="display: none">
                            <a href="{{route('refPage')}}"><span class="icon-users"></span>{{ __('content.ref-system') }}</a>
                        </div>
                        {{-- <div class="cabinet__item cabinet__item_active">
                            <a href="#"><span class="icon-wallet"></span>{{ __('content.earn-bonuses') }}</a>
                        </div> --}}
                    </div>
                </div>
                <div class="cabinet__content">
                    <p class="cabinet__bonus">{{ __('content.bonuses') }}: {{\Illuminate\Support\Facades\Auth::user()->balance}} ₸</p>
                    <div class="cabinet__content-header-wrapper">
                        <h2 style="text-transform: uppercase">{{ __('content.earn-bonuses') }}</h2>
                        <a href="{{ route('logout') }}" class="logout__button">{{ __('content.logout') }}</a>
                    </div>
                    <div class="bonus">
                        <div class="bonus__head">
                            <div class="bonus__head-left">
                                <p>№</p>
                                <p>{{ __('content.task') }}</p>
                            </div>
                            <div class="bonus__head-right">
                                <p>{{ __('content.reward') }}</p>
                            </div>
                        </div>
                        @foreach($quests as $quest)
                            <div class="bonus__content">
                                <div class="bonus__content-left">
                                    <p>{{$quest->id}}</p>
                                    <p>{{$quest->title}}</p>
                                </div>
                                <div class="bonus__content-right">
                                    <span>+ {{ $quest->bonus }} ₸</span>
                                </div>
                                {{-- Undo --}}
                                <button class="{{ $quest->quest_type_id === 3 ? 'order-rate-task' : '' }} {{ $quest->quest_type_id === 1 ? 'share-link' : '' }} @if(\App\Models\UserQuest::where('user_id',\Illuminate\Support\Facades\Auth::id())->where('quest_id',$quest->id)->first()->completed || $loop->index) bonus__btn__success @endif
                                    bonus__btn" @if(\App\Models\UserQuest::where('user_id',\Illuminate\Support\Facades\Auth::id())->where('quest_id',$quest->id)->first()->completed) disabled @endif data-order-id="{{ $quest->order_id }}" data-quest-id="{{ $quest->id }}">
                                    @if(\App\Models\UserQuest::where('user_id',\Illuminate\Support\Facades\Auth::id())->where('quest_id',$quest->id)->first()->completed)
                                        {{ __('content.completed') }}
                                        @else
                                        {{ __('content.complete') }}
                                    @endif
                                </button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal__soc hide">
        <div class="modal__window">
            <button data-close><img class="close" src="{{asset('img/icons/close.svg')}}" alt=""></button>
            <p>{{ __('content.choose-social-net') }}</p>
            <div class="modal__content">
                <a href="#" class="move-to-share" data-quest-id="null" data-social-network="fb"><img src="{{asset('img/icons/facebook.svg')}}" alt="Facebook"></a>
                <a href="#" class="move-to-share" data-quest-id="null" data-social-network="tg"><img src="{{asset('img/icons/telegram.svg')}}" alt="Telegram"></a>
                <a href="#" class="move-to-share" data-quest-id="null" data-social-network="wa"><img src="{{asset('img/icons/whatsapp.svg')}}" alt="WhatsApp"></a>
            </div>
        </div>
    </div>

    <div class="modal modal__box-task hide">
        <div class="modal__window">
            <!-- <button data-close><img class="close" src="img/icons/close.svg" alt=""></button> -->
            <p class="modal__title">{{ __('content.your-bonuses', ['amount' => 100]) }}</p>
            <div class="modal__text">
                <p>{{ __('content.task-usage-message') }}</p>
                <p>{{ __('content.task-completed-message') }}</p>
            </div>
            <button class="modal__close-btn" data-close>{{ __('content.okey') }}</button>
        </div>
    </div>

    <div class="modal modal__order-rate hide">
        <div class="modal__window" style="display: flex; flex-direction: column">
            <button data-close><img class="close" src="{{asset('img/icons/close.svg')}}" alt=""></button>
            <p class="modal__title">{{ __('content.rate-order') }}</p>

            <label style="margin-bottom: 10px">{{ __('content.your-mark') }}:</label>

            <div class="modal__order-rate-star-icons-container" style="display: flex; justify-content: center; margin-bottom: 10px">
                <img class="modal__order-rate-star-icon" data-mark="1" style="margin-right: 5px" src="{{ asset('img/active-star-icon.svg') }}">
                <img class="modal__order-rate-star-icon" data-mark="2" style="margin-right: 5px" src="{{ asset('img/star-icon.svg') }}">
                <img class="modal__order-rate-star-icon" data-mark="3" style="margin-right: 5px" src="{{ asset('img/star-icon.svg') }}">
                <img class="modal__order-rate-star-icon" data-mark="4" style="margin-right: 5px" src="{{ asset('img/star-icon.svg') }}">
                <img class="modal__order-rate-star-icon" data-mark="5" src="{{ asset('img/star-icon.svg') }}">

                <input type="hidden" name="mark">
            </div>

            <input type="hidden" name="order_id">
            <input type="hidden" name="quest_id">

            <div class="modal-control">
                <label style="margin-bottom: 5px">{{ __('content.comment') }}:</label>
                <textarea name="comment" class="feedback__comment"></textarea>
            </div>

            <div class="error-message hide" style="margin-top: 10px"></div>
            <div class="success-message hide" style="margin-top: 10px"></div>

            <input class="send-order-rate"
                   type="submit"
                   value="{{ __('content.send') }}">
        </div>
    </div>
@endsection
