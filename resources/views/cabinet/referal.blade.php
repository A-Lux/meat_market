@extends('layout.app')
@section('content')
    <div class="cabinet">
        <div class="container">
            <div class="cabinet__wrapper">
                <div class="cabinet__menu">
                    <h2 style="text-transform: uppercase">{{ __('content.menu') }}</h2>
                    <div class="cabinet__nav">
                        <div class="cabinet__item ">
                            <a href="{{route('profile')}}"><span class="icon-profile"></span>{{ __('content.personal-info') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('history')}}"><span class="icon-time"></span>{{ __('content.orders-history') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('addresses')}}"><span class="icon-address"></span>{{ __('content.saved-addresses') }}</a>
                        </div>
                        <div class="cabinet__item cabinet__item_active" style="display: none;">
                            <a href="{{route('refPage')}}"><span class="icon-users"></span>{{ __('content.ref-system') }}</a>
                        </div>
                        {{-- <div class="cabinet__item">
                            <a href="{{route('quests')}}"><span class="icon-wallet"></span>{{ __('content.earn-bonuses') }}</a>
                        </div> --}}
                    </div>
                </div>

                <div class="cabinet__content">
                    <p class="cabinet__bonus">{{ __('content.bonuses') }}: {{\Illuminate\Support\Facades\Auth::user()->balance}} ₸</p>
                    <div class="cabinet__content-header-wrapper">
                        <h2 style="text-transform: uppercase">{{ __('content.ref-system') }}</h2>
                        <a href="{{ route('logout') }}" class="logout__button">{{ __('content.logout') }}</a>
                    </div>
                    <div class="refer">
                        <div class="refer__url">
                            <input type="text" name="ref" id="ref-url" value="{{route('home')}}?ref={{\Illuminate\Support\Facades\Auth::user()->id}}#login">
                            <label for="ref">{{ __('content.ref-link') }}</label>
                            <div class="tooltip">
                                <button class="ref-btn"><span class="tooltiptext">{{ __('content.copy') }}</span><span class="icon-ref"></span></button>
                            </div>
                        </div>
                        <div class="refer__table">
                            <div class="refer__header">
                                <div>№</div>
                                <div>{{ __('content.referral') }}</div>
                                <div>{{ __('content.reward') }}</div>
                            </div>
                            @foreach(\Illuminate\Support\Facades\Auth::user()->refers as $refer)
                                <div class="refer__content">
                                    <div>{{$loop->iteration}}</div>
                                    <div>{{$refer->invited_user->email}}</div>
                                    <div><span>+ {{number_format($refer->bonus, 0)}} ₸</span></div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
