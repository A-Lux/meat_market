
@extends('layout.app')
@section('content')
    <div class="cabinet">
        <div class="container">
            <div class="cabinet__wrapper">
                <div class="cabinet__menu">
                    <h2 style="text-transform: uppercase">{{ __('content.menu') }}</h2>
                    <div class="cabinet__nav">
                        <div class="cabinet__item ">
                            <a href="{{route('profile')}}"><span class="icon-profile"></span>{{ __('content.personal-info') }}</a>
                        </div>
                        <div class="cabinet__item cabinet__item_active">
                            <a href="{{route('history')}}"><span class="icon-time"></span>{{ __('content.orders-history') }}</a>
                        </div>
                        <div class="cabinet__item">
                            <a href="{{route('addresses')}}"><span class="icon-address"></span>{{ __('content.saved-addresses') }}</a>
                        </div>
                        <div class="cabinet__item" style="display: none">
                            <a href="{{route('refPage')}}"><span class="icon-users"></span>{{ __('content.ref-system') }}</a>
                        </div>
                        {{-- <div class="cabinet__item">
                            <a href="{{route('quests')}}"><span class="icon-wallet"></span>{{ __('content.earn-bonuses') }}</a>
                        </div> --}}
                    </div>
                </div>
                <div class="cabinet__content">
                    <p class="cabinet__bonus">{{ __('content.bonuses') }}: {{\Illuminate\Support\Facades\Auth::user()->balance}} ₸</p>
                    <div class="cabinet__content-header-wrapper">
                        <h2 style="text-transform: uppercase">{{ __('content.orders-history') }}</h2>
                        <a href="{{ route('logout') }}" class="logout__button">{{ __('content.logout') }}</a>
                    </div>
                    <div class="history">
                        <div class="history__header">
                            <div>№</div>
                            <div>{{ __('content.delivery-address') }}</div>
                            <div>{{ __('content.cost') }}</div>
                            <div>{{ __('content.status') }}</div>
                            <div>{{ __('content.date') }}</div>
                        </div>
                        @foreach($orders as $order)
                            <div class="history__content" data-order-id="{{ $order->id }}" data-order-data="{{ json_encode($order) }}">
                                <div>{{$order->id}}</div>
                                <div>{{!is_null($order->address) ? $order->address->name : $order->street. ' ,'.$order->house}}</div>
                                <div>{{number_format($order->price)}} ₸</div>
                                <div><span>{{$order->delivery_status->name}}</span></div>
                                <div>{{$order->created_at->format('y.m.d')}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/order-details.js') }}"></script>

@endsection
