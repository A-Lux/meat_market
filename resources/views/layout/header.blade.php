<!DOCTYPE html>
<html lang="{{ \App::getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="apple-touch-icon icon" href="{{ asset('favicon.ico') }}">
    <meta name="description" content="Meat Market">

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/tasks.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/add-delivery-address.js') }}"></script>

    <title>Meat Market</title>
</head>
<body>
<a id="site-top"></a>
<!-- Шапка -->
<section class="main" @if(Route::current()->getName() != 'home') style="height:25vh;padding-bottom:40px;" @endif>
    <div class="container">
        <header class="header">
            <nav class="nav__top nav">
                <div class="header__logo">
                    <a href="/">
                        <img src="{{asset('img/logo.png')}}" alt="Meat Market">
                    </a>
                </div>
                <ul class="nav__menu">
                    <li><a href="{{ route('tipsAndRecipes') }}">{{ __('content.tips-and-recipes') }}</a></li>
                    <li><a href="/#where-is-try">{{ __('content.where-is-try') }}</a></li>
                    <li><a class="subscribe" href="#">{{ __('content.subscription') }}</a></li>
                </ul>
                <ul class="nav__city">
                    <div>
                        <li><a class="phone__container" href="tel:+77719365171">+7 771 936 51 71</a></li>
                        <div class="custom-select__wrapper" data-select-name="cities">
                            <select class="custom-select__native-select" name="cities">
                                @foreach(\App\Models\City::all()->translate(Illuminate\Support\Facades\App::getLocale()) as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>

                            <div class="custom-select__container hidden">
                                <div class="custom-select__blur"></div>
                                <div class="custom-select__items-wrapper">
                                    @foreach(\App\Models\City::all()->translate(Illuminate\Support\Facades\App::getLocale()) as $city)
                                        <div class="custom-select__item" data-value="{{ $city->id }}">{{ $city->name }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <li class="nav__city-schedule" style="margin-top: 5px"><a style="white-space: nowrap">09:00 - 23:30</a></li>
                </ul>
                <ul class="nav__profile">
                    <li>
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <a href="{{ route('profile') }}" style="display: flex; align-items: center">
                                <span class="nav__profile-user-name">{{ \Illuminate\Support\Facades\Auth::user()->name }}</span>
                                <span class="icon-user"></span>
                            </a>
                        @else
                            <a class="userModal"><span class="icon-user"></span></a>
                        @endif
                    </li>
{{--                    <li><a @if(!\Illuminate\Support\Facades\Auth::check()) data-modal class="userModal" @else href="{{route('profile')}} @endif "><span class="icon-user"></span></a></li>--}}
                    <li class="nav__favorite">
                        <a href="{{route('favourite')}}">
                            <span class="icon-star" onclick="window.location.href='{{route('favourite')}}'"></span>
                        </a>
                    </li>
                    <li class="nav__cart" id="basket_icon">
                        <a class="nav__cart_a" style="pointer-events: none;" href="#">
                            <span class="icon-shopping-bag"></span>
                            <span class="nav__circle" id="main_total_basket">0</span>
                        </a>
                        <div class="popup hide">
                            <div class="popup__modal">
                                <p class="popup__title">{{ __('content.basket') }}</p>
                                <div class="popup__items" id="basket_panel"></div>
                                <div class="popup__items-btn">
                                    <button class="popup__btn" onclick="window.location.href='{{route('show')}}'">
                                        <span class="icon-shopping-bag"></span>{{ __('content.in-basket') }}
                                    </button>
                                    <p class="popup__price">{{ __('content.total-amount') }}</p>
                                    <span class="total_sum popup__price"></span>
                                </div>
                            </div>
                        </div>
                    </li>

                    <div class="hamburger__mobile">
                        <button class="hamburger--slider">
                            <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </ul>
            </nav>
            <nav class="nav__bottom">
                <div class="nav__scroll">
                    <ul>
                        <li><a href="{{route('catalog', 'sale')}}">{{ __('content.sale-products') }}</a></li>
                        @foreach(\App\Models\ShopCategory::limit(5)->get() as $category)
                            @php $category = $category->translate(\Illuminate\Support\Facades\App::getLocale()) @endphp
                            <li><a href="{{route('catalog',$category->id)}}">{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <ul class="nav__icons">
                    <input type="text" class="search-input" placeholder="{{ __('content.search') }}">
                    <li><a href="javascript:void(0);"><span class="icon-search"></span></a></li>
                    <li class="custom-select__wrapper" data-select-name="language">
                        <select class="custom-select__native-select" name="language">
                            <option value="ru">Рус</option>
                            <option value="en">Eng</option>
                        </select>

                        <div class="custom-select__container hidden">
                            <div class="custom-select__blur"></div>
                            <div class="custom-select__items-wrapper">
                                <div class="custom-select__item" data-value="ru">Рус</div>
                                <div class="custom-select__item" data-value="en">Eng</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="popup_search hide">
                <div class="popup_search__modal">
                    <p class="popup_search__title">{{ __('content.search') }}</p>
                    <div class="popup_search__items">
                        
                    </div>
                </div>
            </div>
            <div class="mobile__menu">
                <nav class="menu__mobile-top">
                    <ul>
                        <li><a href="#">{{ __('content.where-is-try') }}</a></li>
                        <li><a href="#">{{ __('content.advices') }}</a></li>
                        <li><a href="#">{{ __('content.recipes') }}</a></li>
                    </ul>
                </nav>
                <nav class="menu__mobile-mid">
                    <ul>
                        <li><a href="{{route('catalog', 'sale')}}">{{ __('content.sale-products') }}</a></li>
                        @foreach(\App\Models\ShopCategory::limit(5)->get() as $category)
                            @php $category = $category->translate(\Illuminate\Support\Facades\App::getLocale()) @endphp
                            <li><a href="{{ route('catalog', $category->id) }}">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </nav>
                <div class="menu__mobile-city">
                    <a href="#">
                        <span>{{ __('content.your-city') }}:</span>
                        <div class="custom-select__wrapper" data-select-name="cities">
                            <select class="custom-select__native-select" name="cities">
                                @foreach(\App\Models\City::all()->translate(Illuminate\Support\Facades\App::getLocale()) as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>

                            <div class="custom-select__container hidden">
                                <div class="custom-select__blur"></div>
                                <div class="custom-select__items-wrapper">
                                    @foreach(\App\Models\City::all()->translate(Illuminate\Support\Facades\App::getLocale()) as $city)
                                        <div class="custom-select__item" data-value="{{ $city->id }}">{{ $city->name }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#">{{ __('content.logout') }}</a>
                </div>
                <div class="menu__mobile-bottom">
                    <div class="mobile__socials">
                        @foreach(\App\Models\SocialNet::all() as $item)
                            <a href="{{ $item->link }}" target="_blank"><img width="23" height="23" src="{{ asset('storage/' . $item->icon) }}" alt="Meat Market"></a>
                        @endforeach
                    </div>
                    <div class="mobile__tel">
                        <a class="phone__container" href="#">+7 771 936 51 71</a>
                    </div>
                    <div class="mobile__language">
                        <div class="custom-select__wrapper" data-select-name="language">
                            <select class="custom-select__native-select" name="language">
                                <option value="ru">Рус</option>
                                <option value="en">Eng</option>
                            </select>

                            <div class="custom-select__container hidden">
                                <div class="custom-select__blur"></div>
                                <div class="custom-select__items-wrapper">
                                    <div class="custom-select__item" data-value="ru">Рус</div>
                                    <div class="custom-select__item" data-value="en">Eng</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        @if(Route::current()->getName() == 'home')
            @php $mainBanner = \App\Models\MainBanner::get()->translate(\Illuminate\Support\Facades\App::getLocale()) @endphp
            <div class="main__banner">
                <p>{{ $mainBanner[0]->description }}</p>
                <h1 class="main__title">{{ $mainBanner[0]->title }}</h1>
                <a href="{{ $mainBanner[0]->button_link }}" class="btn btn_main"><span class="icon-red_circle"></span>{{ $mainBanner[0]->button_text }}</a>
            </div>
        @endif
    </div>
</section>

<script src="{{ asset('js/js.cookie.min.js') }}"></script>

<script type="text/javascript">
    // Region switching
    const citySwitchers= document.querySelectorAll('.custom-select__wrapper[data-select-name="cities"]'),
          nativeCitySwitchers = document.querySelectorAll('select[name="cities"]'),
          regionInfo = <?php echo json_encode(App\Models\City::orderBy('id')->get()->translate(Illuminate\Support\Facades\App::getLocale())) ?>,
          phoneContainers = document.querySelectorAll('.phone__container');

    citySwitchers.forEach(initSwitcherEvents);

    let currentCityId = 1;

    if (Cookies.get('city_id') === '2') {
        currentCityId = 2;
    }

    document.querySelector('.nav__city-schedule a').innerHTML = regionInfo[currentCityId - 1].schedule;

    phoneContainers[0].innerHTML = regionInfo[currentCityId - 1].phone;
    phoneContainers[1].innerHTML = regionInfo[currentCityId - 1].phone;
    nativeCitySwitchers[0].value = currentCityId;
    nativeCitySwitchers[1].value = currentCityId;

    // citySwitchers.forEach(node => {
    //     node.value = Cookies.get('city_id') || '1';
    //
    //     node.addEventListener('change', _ => {
    //         document.querySelector('.nav__city-schedule a').innerHTML = regionInfo[parseInt(node.value) - 1].schedule;
    //         phoneContainers[0].innerHTML = regionInfo[parseInt(node.value) - 1].phone;
    //         phoneContainers[1].innerHTML = regionInfo[parseInt(node.value) - 1].phone;
    //         Cookies.set('city_id', node.value, { expires: 100 });
    //     });
    // });

    // Language switcher
    const languageSwitchers = document.querySelectorAll('.custom-select__wrapper[data-select-name="language"]');
    const nativeLanguageSelects = document.querySelectorAll('select[name="language"]');

    setValueToNativeLanguageSelect();
    languageSwitchers.forEach(initSwitcherEvents);

    function initSwitcherEvents(switcher) {
        const nativeSelect = switcher.querySelector('.custom-select__native-select');
        const customSelect = switcher.querySelector('.custom-select__container');
        const options = switcher.querySelectorAll('.custom-select__item');
        const selectName = switcher.getAttribute('data-select-name');

        options.forEach(option => {
            option.addEventListener('click', _ => customOptionOnClick(selectName, option.getAttribute('data-value')));
        });

        let isCustomSelectVisible = false,
            wasNativeSelectClickEvent = false;

        window.addEventListener('click', _ => {
            if (isCustomSelectVisible && !wasNativeSelectClickEvent) {
                isCustomSelectVisible = false;
                wasNativeSelectClickEvent = false;

                hiddenCustomSelect(customSelect);
            }

            wasNativeSelectClickEvent = false;
        });

        nativeSelect.addEventListener('mousedown', e => e.preventDefault());
        nativeSelect.addEventListener('click', _ => {
            if (!isCustomSelectVisible) {
                wasNativeSelectClickEvent = true;
                isCustomSelectVisible = true;
                showCustomSelect(customSelect);
            }
        });
    }

    function showCustomSelect(node) {
        node.classList.remove('hidden');
        setTimeout(_ => node.classList.add('visible'), 0);
    }

    function hiddenCustomSelect(node) {
        node.classList.remove('visible');
        setTimeout(_ => node.classList.add('hidden'), 100);
    }

    function customOptionOnClick(selectName, optionValue) {
        switch (selectName) {
            case 'language':
                Cookies.set('locale', optionValue, { expires: 100 });
                location.reload();
                break;
            case 'cities':
                Cookies.set('city_id', optionValue, { expires: 100 });
                location.reload();
                break;
            default:
                console.error('Undefined custom select!');
        }
    }

    function setValueToNativeLanguageSelect() {
        const currentLanguage = Cookies.get('locale') || 'ru';

        nativeLanguageSelects.forEach(nativeSelect => {
            nativeSelect.value = currentLanguage;
        });
    }
</script>
