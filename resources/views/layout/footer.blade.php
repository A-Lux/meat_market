{{-- Модалки --}}

<div class="modal hide modal__order-details">
    <div class="modal__window">
        <button onclick="$('.modal__order-details').addClass('hide');" data-close><img class="close"
                                                                                       src="{{asset('/img/icons/close.svg')}}"
                                                                                       alt=""></button>
        <h1>{{ __('content.order-details') }}:</h1>
        <div class="order-details">
            <!-- Order Details Container -->
            <div class="order-details__items">
                {{--
                <div class="order-details__item">--}}
                    {{-- <p class="order-details__product-name">1. Название стейка №1 х 2 шт</p>--}}
                    {{--                </div>
                --}}
            </div>

            <div class="order-details__total-price-wrapper">
                <span>{{ __('content.total-amount') }}:</span> <span>000 000 тг</span>
            </div>
        </div>
    </div>
</div>


<div class="modal {{$errors->get('model_login')?'show':'hide'}} modal__login">
    <div class="modal__window">
        <button data-close><img class="close" src="{{asset('/img/icons/close.svg')}}" alt=""></button>
        <p>{{ __('content.login') }}</p>
        <form action="{{route('login')}}" method="POST">
            {{csrf_field()}}
            <div class="modal__top">
                <div class="modal-control">
                    <input type="text" required minlength="1" name="email">
                    <label>{{ __('content.email') }}</label>
                </div>
                <div class="modal-control">
                    <input type="password" required name="password" minlength="6">
                    <label>{{ __('content.password') }}</label>
                </div>
                {!!$errors->get('model_login') ? '<p
                    style=" font-size: 12px; margin-top: 29px; margin-bottom: 0; line-height: 144%; ">
                    '.$errors->get('model_message')[0].'</p>':''!!}
                <input type="submit" value="{{ __('content.login') }}" name="joined">
            </div>
            <div class="modal__bottom-link">
                <a href="#" class="password__forget">{{ __('content.forgot-password') }}</a>
                <a href="#" class="modal__register">{{ __('content.register') }}</a>
            </div>
        </form>
    </div>
</div>
<div class="modal {{$errors->get('model_register')?'show':'hide'}} modal-register   ">
    <div class="modal__window">
        <button data-close class="close"><img src="{{asset('img/icons/close.svg')}}" alt=""></button>
        <p>{{ __('content.registration') }}</p>
        <form action="{{route('register')}}" method="POST">
            {{csrf_field()}}
            <div class="modal__top">
                <div class="modal-control">
                    <input type="text" name="name">
                    <label>{{ __('content.name') }}</label>
                </div>
                <div class="modal-control">
                    <input type="text" name="surname">
                    <label>{{ __('content.last-name') }}</label>
                </div>
                <div class="modal-control">
                    <input type="email" name="email">
                    <label>{{ __('content.email') }}</label>
                </div>
                <div class="modal-control">
                    <input type="password" name="password">
                    <label>{{ __('content.password') }}</label>
                </div>
                <div class="modal-control">
                    <input type="password" name="password_confirmation">
                    <label>{{ __('content.repeat-password') }}</label>
                </div>
                {!!$errors->get('model_register') ? '<p
                    style=" font-size: 12px; margin-top: 29px; margin-bottom: 0; line-height: 144%; ">
                    '.$errors->get('model_message')[0].'</p>':''!!}
                <input class="modal__create-account" type="submit" value="{{ __('content.create') }}">
            </div>
            <div class="modal__bottom-link">
                <a class="login-modal" href="#">{{ __('content.already-registered') }}</a>
            </div>
        </form>
    </div>
</div>
<div class="modal modal-password hide">
    <div class="modal__window">
        <button data-close><img class="close" src="{{asset('img/icons/close.svg')}}" alt=""></button>
        <p>{{ __('content.set-password') }}</p>
        <form>
            <div class="modal__top">
                <div class="modal-control">
                    <input required minlength="1" type="password" name="password">
                    <label>{{ __('content.come-up-password') }}</label>
                </div>
                <div class="modal-control">
                    <input required minlength="1" type="password" name="retry-password">
                    <label>{{ __('content.repeat-password') }}</label>
                </div>
                <input type="submit" name="password_in" value="{{ __('content.set') }}">
            </div>
        </form>
    </div>
</div>
<div class="modal modal-forget_password hide">
    <div class="modal__window">
        <button data-close><img class="close" src="{{asset('img/icons/close.svg')}}" alt=""></button>
        <p>{{ __('content.recovery') }}</p>
        <form action="/recover-password" method="POST">
            <div class="modal__top">
                <div class="modal-control">
                    <input required minlength="1" type="text" name="email" value="{{ old('email') }}">
                    @csrf
                    <label>{{ __('content.email') }}</label>
                </div>
            </div>
            {{--@if($errors->any())--}}
            {{--
            <div class="error-message">{{ $errors->all()->first() }}</div>
            --}}
            {{--@endif--}}
            @if(session('password_reset_success'))
            <div class="success-message" style="color:red;">Эл.почта не найдена</div>
            @endif

            @if(session('password_reset_success_send'))
            <div class="success-message">На этот E-mail мы отправили Ваш новый пароль!</div>
            @endif
            <input type="submit" value="{{ __('content.recover') }}">
        </form>
    </div>
</div>
<div class="modal modal__feedback hide">
    <div class="modal__window">
        <button data-close><img class="close" src="{{asset('/img/icons/close.svg')}}" alt=""></button>
        <p>{{ __('content.send-request') }}</p>
        <form class="offer" method="POST">
            <div class="modal__top">
                <div class="modal-control">
                    <input class="offer__name" type="text" required name="text" minlength="1">
                    <label>{{ __('content.name') }}</label>
                </div>
                <div class="modal-control">
                    <input class="offer__email" type="text" required minlength="1" name="email">
                    <label>{{ __('content.email') }}</label>
                </div>
                <div class="modal-control">
                    <label>{{ __('content.comments') }}</label>
                    <textarea class="offer__comment"></textarea>
                </div>
                <input type="submit" value="{{ __('content.send') }}">
            </div>
            <div class="modal__bottom-link">
            </div>
        </form>
    </div>
</div>
<div class="modal modal__sentence hide">
    <div class="modal__window">
        <button data-close><img class="close" src="{{asset('/img/icons/close.svg')}}" alt=""></button>
        <div class="modal__tabs">
            <div class="modal__tabs-one modal__tabs-click">
                Отзыв
            </div>
            <div class="modal__tabs-two">
                Предложение
            </div>
        </div>
        {{-- <p>{{ __('content.send-review') }}</p> --}}
        <form class="feedback" method="POST">
            <div class="modal__top">
                <div class="modal-control">
                    <input class="feedback__name" type="text" required name="title" minlength="1">
                    <label>{{ __('content.name') }}</label>
                </div>
                <div class="modal-control">
                    <input class="feedback__email" type="text" required minlength="1" name="email">
                    <label>{{ __('content.email') }}</label>
                </div>
                <div class="modal-control">
                    <label>{{ __('content.comments') }}</label>
                    <textarea class="feedback__comment"></textarea>
                </div>
                <input type="submit" value="{{ __('content.send') }}">
            </div>
        </form>
    </div>
</div>

<div class="modal modal__waite hide">
    <div class="modal__window">

        <div class="" style="
    text-align: center;
    font-size: 18px;
    width: 100%;
">Заказ обрабатывается.
        </div>
        <div class="" style="
    text-align: center;
    font-size: 18px;
    margin-top: 14px;
    font-weight: bold;
">Ожидайте
        </div>

    </div>
</div>

<div class="modal modal__request hide">
    <div class="modal__window">
        <button data-close><img class="close" src="{{asset('/img/icons/close.svg')}}" alt=""></button>
        <div class="modal__tabs">
            <div class="modal__tabs-one">
                Отзыв
            </div>
            <div class="modal__tabs-two modal__tabs-click">
                Предложение
            </div>
        </div>
        {{-- <p>{{ __('content.send-partner-request') }}</p> --}}
        <form class="application" method="POST">
            <div class="modal__top">
                <div class="modal-control">
                    <input type="text" class="application__name" required name="title" minlength="1">
                    <label>{{ __('content.name') }}</label>
                </div>
                <div class="modal-control">
                    <input type="text" class="application__email" required minlength="1" name="email">
                    <label>{{ __('content.email') }}</label>
                </div>
                <div class="modal-control">
                    <label>{{ __('content.comments') }}</label>
                    <textarea name="comment" class="application__comment"></textarea>
                </div>
                <input type="submit" value="{{ __('content.send') }}">
            </div>
        </form>
    </div>
</div>
<div class="modal hide modal__variations">
    <div class="modal__window">
        <button data-close><img class="close" src="{{asset('/img/icons/close.svg')}}" alt=""></button>
        <p>Выберите вес:</p>

        <div class="variations__container">
            {{--
            <button class="variation__item" data-product="1" data-variation="1">12 кг</button>
            --}}
            {{--
            <button class="variation__item" data-product="1" data-variation="1">12 кг</button>
            --}}
            {{--
            <button class="variation__item" data-product="1" data-variation="1">12 кг</button>
            --}}
        </div>
    </div>
</div>

<div class="modal hide add-delivery-address-popup">
    <div class="modal__window">
        <button data-close class="close"><img src="{{asset('img/icons/close.svg')}}" alt=""></button>
        <h3>{{ __('content.new-address') }}</h3>
        <div id="add_address_form">
            <div class="modal__top">
                <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                <input required minlength="1" type="text" name="title">
                <label>{{ __('content.address-name') }}</label>
            </div>

            <div id="add-delivery-yandex-map" style="width: 100%; height: 200px"></div>

            <input type="hidden" name="full_address" value="">
            <input type="hidden" name="street" value="">
            <input type="hidden" name="house" value="">
            <input type="hidden" name="coords" value="">

            <div class="modal__bottom" style="margin-top: 20px">
                <div class="modal__item">
                    <input required minlength="1" type="text" name="flat">
                    <label>{{ __('content.flat') }}</label>
                </div>
                <div class="modal__item">
                    <input required minlength="1" type="text" name="floor">
                    <label>{{ __('content.floor') }}</label>
                </div>
            </div>

            <div class="modal__item" style="max-width: 100% !important; width: 100%; margin-top: 15px">
                <input required minlength="1" type="text" name="entrance"
                       style="max-width: 100% !important; width: 100%">
                <label>{{ __('content.entrance') }}</label>
            </div>

            <div class="modal__item modal__item__max">
                <textarea minlength="1" type="text" name="comment"></textarea>
                <label>{{ __('content.comment') }}</label>
            </div>
            <input class="add_address_btn" type="submit" value="{{ __('content.create') }}" name="btn-create">
        </div>
    </div>
</div>

{{-- Футер --}}

<footer class="footer">
    <!-- Футер -->
    <div class="container">
        <div class="footer__top">
            <div class="footer__contacts">
                <a href="tel:+77719365171" class="footer__tel">+7 771 936 51 71</a>
                <div class="footer__socials">
                    @foreach(\App\Models\SocialNet::all() as $item)
                    <a href="{{ $item->link }}" target="_blank"><img width="23" height="23"
                                                                     src="{{ asset('storage/' . $item->icon) }}"
                                                                     alt="Meat Market"></a>
                    @endforeach
                </div>
            </div>
            <div class="footer__send send">
                <p class="send__text">{{ __('content.leave-your-email') }}</p>
                <form id="discount-request-form" class="send__input">
                    <input class="send__data" type="text" name="email" placeholder="{{ __('content.enter-email') }}">
                    <button type="submit" name="send_submit"><span class="icon-right"></span></button>
                </form>
            </div>
        </div>
        <div class="footer__middle">
            <div class="footer__logo">
                <img src="{{asset('img/logo.png')}}" alt="Meatmarkest">
            </div>
            <ul>
                @foreach(\App\Models\ShopCategory::where('id','<',5)->get() as $category)
                <?php $category = $category->translate(\Illuminate\Support\Facades\App::getLocale()) ?>
                <li><a href="#">{{$category->name}}</a></li>
                @endforeach

            </ul>
            <p class="footer__corp">
                2015 — 2021. MEATMARKET.KZ
            </p>
        </div>
        <div class="footer__bottom">
            <ul>
                {{--
                <li><a href="#">{{ __('content.where-is-purchase') }}</a></li>
                --}}
                <li><a href="{{route('tipsAndRecipes')}}">{{ __('content.tips-and-recipes') }}</a></li>
                {{--
                <li><a href="{{route('tipsAndRecipes')}}">РЕЦЕПТЫ</a></li>
                --}}
            </ul>

            <ul>
                <li>
                    <a class="modalFeedback" data-modal>{{ __('content.send-request') }}</a>
                </li>
                <li>
                    <a class="modalSentence" data-modal>{{ __('content.send-review') }}</a>
                </li>
                {{--
                <li>
                    <a class="modalRequest" data-modal>{{ __('content.send-offer') }}</a>
                </li>
                --}}
            </ul>
            <a href="#site-top" class="footer__up">
                <span class="icon-arrow_up"></span>
            </a>
        </div>

        <div style="display: flex; justify-content: flex-end; margin-bottom: 10px">
            <a href="https://a-lux.kz" class="alux__link" target="_blank" style="margin: 0">{{ __('content.alux-link')
                }}</a>
        </div>
    </div>
</footer>
<footer class="footer-mobile">
    <div class="container">
        <div class="footer-mobile__top">
            <div class="footer-mobile__logo">
                <img src="{{ asset('img/logo.png') }}" alt="Meat Market">
            </div>
            <div class="footer-mobile__form">
                <form>
                    <div class="footer-mobile__form-texts">
                        <p>{{ __('content.leave-your-email') }}</p>
                        <img src="img/lenta.svg" alt="">
                    </div>
                    <input type="email" name="email" placeholder="{{ __('content.enter-email') }}">
                </form>
            </div>
            <div class="footer-mobile__nav">
                @foreach(\App\Models\ShopCategory::where('id','<',5)->get() as $category)
                <?php $category = $category->translate(\Illuminate\Support\Facades\App::getLocale()) ?>
                <a href="#">{{ $category->name }}</a>
                @endforeach
            </div>
            <div class="footer-mobile__contacts">
                <div class="footer-mobile__social-wrapper">
                    @foreach(\App\Models\SocialNet::all() as $item)
                    <a href="{{ $item->link }}" target="_blank"><img width="23" height="23"
                                                                     src="{{ asset('storage/' . $item->icon) }}"
                                                                     alt="Meat Market"></a>
                    @endforeach
                </div>
                <a href="#">+7 771 936 51 71</a>
            </div>
            <div class="footer-mobile__end">
                <div class="footer-mobile__url">
                    {{-- <a href="#">{{ __('content.where-is-purchase') }}</a>--}}
                    <a href="#">{{ __('content.advices') }}</a>
                    <a href="#">{{ __('content.recipes') }}</a>
                </div>
                <p style="margin-bottom: 15px">2015 — 2021. MEATMARKET.KZ</p>
                <a href="https://a-lux.kz" class="alux__link" target="_blank">{{ __('content.alux-link') }}</a>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="{{ asset('js/jquery-3.5.1.min.js') }}?v=2"></script>
<script src="{{asset('js/order/index.js')}}?v=2"></script>
<script src="{{asset('js/basket/main.js')}}?v=2"></script>
<script src="{{asset('js/swiper-bundle.min.js')}}?v=2"></script>
<script src="{{asset('js/main.js')}}?v=2"></script>
<script src="{{asset('js/discount-request.js')}}?v=2"></script>
</body>
</html>

