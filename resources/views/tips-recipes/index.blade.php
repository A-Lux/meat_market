@extends('layout.app')
@section('content')
    <section class="advice">
        <div class="container">
            <div class="advice__breads">
                <a href="#">{{ __('content.home') }}</a>
                <span>></span>
                <p>{{ __('content.tips-and-recipes') }}</p>
            </div>
            <h1 class="advice__title">
                @if(request()->input('section', 'tips') === 'tips')
                    <span>{{ __('content.tips') }}</span>
                @else
                    <a href="?section=tips">{{ __('content.tips') }}</a>
                @endif
                {{ __('content.and') }}
                @if(request()->input('section', 'tips') === 'recipes')
                    <span>{{ __('content.recipes') }}</span>
                @else
                    <a href="?section=recipes">{{ __('content.recipes') }}</a>
                @endif
            </h1>
            @foreach($articles as $article)
                <div class="@if($loop->odd)advice__block @else advice__block-right @endif">
                    <div class="advice__img">
                        <img src="{{ asset('storage/' . $article->preview_image) }}" alt="МяСо">
                        <div class="advice__info">
                            <time>{{\Carbon\Carbon::parse($article->created_at)->format('y.m.d')}}</time>
                            <h2>{{$article->name}}</h2>
                            <a href="{{route('recipePage',['id' => $article->id, 'section' => request()->input('section')])}}" class="advice__more more">
                                {{ __('content.show-more') }}<span class="icon-line"></span></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
