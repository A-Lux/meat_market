@extends('layout.app')
@section('content')
    <section class="tip">
        <div class="tip__top" style="background-image: url({{ asset('storage/' . $article->first_bg_image) }})">
            <div class="container">
                <h1>{{ $article->name }}</h1>

                <div class="tip__top-content-wrapper">
                    <div class="tip__top-content">
                        <h2>{{ $article->column_01_title }}</h2>
                        <div>{{ $article->column_01_description }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tip__middle" style="background-image: url({{ asset('storage/' . $article->second_bg_image) }})">
            <div class="container">
                <div class="tip__middle-columns">
                    <div class="tip__middle-headers">
                        @if($article->column_02_title && $article->column_02_description)
                            <h2>{{ $article->column_02_title }}</h2>
                        @endif

                        @if($article->column_03_title && $article->column_03_description)
                            <h2>{{ $article->column_03_title }}</h2>
                        @endif

                        @if($article->column_04_title && $article->column_04_description)
                            <h2>{{ $article->column_04_title }}</h2>
                        @endif
                    </div>

                    <div class="tip__middle-descriptions">
                        @if($article->column_02_title && $article->column_02_description)
                            <p>{{ $article->column_02_description }}</p>
                        @endif

                        @if($article->column_03_title && $article->column_03_description)
                            <p>{{ $article->column_03_description }}</p>
                        @endif

                        @if($article->column_04_title && $article->column_04_description)
                            <p>{{ $article->column_04_description }}</p>
                        @endif
                    </div>
                </div>

                <div class="tip__bottom">
                    @if($article->column_05_title && $article->column_05_description)
                        <div class="tip__bottom-content">
                            <h2>{{ $article->column_05_title }}</h2>
                            <p>{{ $article->column_05_description }}</p>
                        </div>
                    @endif

                    @if($article->finally_phrase)
                        <h2 class="tip__finally-phrase">{{ $article->finally_phrase }}</h2>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <style>
        .footer {
            margin: 0 !important;
            padding-top: 60px !important;
        }

        .footer-mobile {
            padding-top: 48px !important;
        }
    </style>
@endsection