<?php

use App\Http\Controllers\Front\ProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Order\BasketController;
use App\Http\Controllers\Order\DiscountController;
use App\Http\Controllers\Order\OrderController;
use App\Http\Controllers\Shop\CatalogController;
use App\Http\Controllers\Shop\SetController;
use App\Http\Controllers\User\ApplicationController;
use App\Http\Controllers\User\FavouriteController;
use App\Http\Controllers\User\FeedbackController;
use App\Http\Controllers\User\MainController;
use App\Http\Controllers\User\OfferController;
use App\Http\Controllers\User\PasswordRecoveryController;
use App\Http\Controllers\User\QuestController;
use App\Http\Controllers\User\RefController;
use App\Http\Controllers\Voyager\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Auth::routes();


Route::get('/api-getsss', function () {



});


Route::prefix('/tips-recipes')->group(function () {
    Route::get('/', [HomeController::class, 'tipsAndRecipes'])->name('tipsAndRecipes');
    Route::get('/{id}', [HomeController::class, 'recipePage'])->name('recipePage');
});

Route::prefix('/catalog')->group(function () {
    Route::post('', [CatalogController::class, 'index']);

    Route::get('/cow-parts/{part}', [ProductController::class, 'cowParts'])->name('cowPart');
    Route::get('/{id}', [ProductController::class, 'index'])->name('catalog');
});

Route::post('/offer', [OfferController::class, 'store']);
Route::post('/feedback', [FeedbackController::class, 'store']);
Route::post('/application', [ApplicationController::class, 'store']);

Route::prefix('/discount')->group(function () {
    Route::post('/', [DiscountController::class, 'index']);
});

Route::prefix('/home')->middleware('auth')->group(function () {
    Route::get('/', [MainController::class, 'profile'])->name('profile');
    Route::post('/update', [MainController::class, 'updateData'])->name('update');
    Route::prefix('/ref')->group(function () {
        Route::get('/', [RefController::class, 'index'])->name('refPage');
    });


    Route::prefix('/favourite')->group(function () {
        Route::get('/show', [FavouriteController::class, 'show'])->name('show');
        Route::get('/', [FavouriteController::class, 'index'])->name('favourite');
        Route::get('/add/{product}', [FavouriteController::class, 'add']);
        Route::get('/delete/{product}', [FavouriteController::class, 'remove']);
    });
    Route::get('/user', [MainController::class, 'getUserData']);

    Route::prefix('/orders')->group(function () {
        Route::get('/', [OrderController::class, 'history'])->name('history');
    });


    Route::prefix('/quests')->group(function () {
        Route::get('/', [QuestController::class, 'index'])->name('quests');
    });

    Route::prefix('/addresses')->group(function () {
        Route::get('/index', [MainController::class, 'getAddresses'])->name('address');
        Route::get('/', [MainController::class, 'addresses'])->name('addresses');
        Route::post('/', [MainController::class, 'store'])->name('storeAddress');
        Route::get('/delete/{address}', [MainController::class, 'deleteAddress'])->name('deleteAddress');
        Route::post('/update/{address}', [MainController::class, 'updateAddress'])->name('addressUpdate');
    });
});

Route::prefix('/product')->group(function () {
    Route::get('/{product}', [ProductController::class, 'show'])->name('product');
});

Route::prefix('order')->group(function () {
    Route::post('/', [OrderController::class, 'order'])->name('order');
    Route::get('/delivery-edit', [OrderController::class, 'orderEdit']);
    Route::post('/create-review', [OrderController::class, 'createReview']);
});

Route::prefix('sets')->group(function () {
    Route::get('', [SetController::class, 'index'])->name('sets');
    Route::get('{id}', [SetController::class, 'show'])->name('set');
});

Route::prefix('basket')->group(function () {
    Route::get('/show', [BasketController::class, 'index'])->name('show');
    Route::post('/add', [BasketController::class, 'addProduct']);
    Route::post('/remove', [BasketController::class, 'removeProduct']);
    Route::post('/change/quantity', [BasketController::class, 'changeQuantityProduct']);
    Route::get('/', [BasketController::class, 'show']);
});

Route::get('public-offer', [HomeController::class, 'showPublicOffer']);
Route::get('privacy-policy', [HomeController::class, 'showPrivacyPolicy']);
Route::get('online-payments', [HomeController::class, 'showOnlinePayments']);

Route::post('recover-password', [PasswordRecoveryController::class, 'index']);

Route::get('logout', [MainController::class, 'logout'])->name('logout');

Route::get('share', [QuestController::class, 'shareToSocialNets'])->name('share');

//Route::get('/clear-quests', function () {
//    DB::table('user_quest')->where('quest_id', '!=', -1)->delete();
//    DB::table('quests')->where('id', '!=', -1)->delete();
//});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('users/{id}/edit', [UserController::class, 'edit'])->name('voyager.users.edit');
});

Route::get('/test', function () {
    dd(\App\Models\Product::find(4)->group_pair);
});

Route::get('info', function () {
    phpinfo();
});

Route::get('list', function () {


});

