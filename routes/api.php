<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Order\DiscountController;
use App\Http\Controllers\Shop\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\MainController;
use App\Http\Controllers\Shop\CatalogController;
use App\Http\Controllers\Order\DeliveryTypeController;
use App\Http\Controllers\Order\BasketController;
use App\Http\Controllers\Order\OrderController;

Route::post('/install', [MainController::class,'install']);

Route::get('/1c_exchange', [MainController::class, 'c_exchange']);
Route::post('/1c_exchange', [MainController::class, 'c_exchange']);

Route::prefix('auth')->group(function(){
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/register', [RegisterController::class, 'register']);
});

Route::prefix('catalog')->group(function(){
    Route::post('/', [CatalogController::class, 'index']);
});

Route::prefix('/user')->group(function(){
    Route::get('/', [MainController::class, 'getUserData']);
    Route::post('/update', [MainController::class, 'updateData']);
});

Route::middleware('auth:sanctum')->group(function (){
    Route::prefix('user')->group(function(){

    });
});

Route::prefix('basket')->group(function(){
    Route::post('/add', [BasketController::class,'addProduct']);
    Route::post('/remove', [BasketController::class,'removeProduct']);
    Route::post('/change/quantity', [BasketController::class,'changeQuantityProduct']);
});

Route::prefix('order')->group(function(){
    Route::post('/', [OrderController::class,'order']);
});

Route::prefix('delivery')->group(function(){
    Route::get('/areas', [DeliveryTypeController::class,'getAreas']);
});

Route::prefix('product')->group(function () {
    Route::get('/by-caw-part/{id}', [ProductController::class, 'getByCategory']);
    Route::get('nested-variations', [ProductController::class, 'getNestedVariations']);
    Route::get('nested-variation', [ProductController::class, 'getNestedVariation']);
});

Route::prefix('discount-request')->group(function () {
    Route::post('create', [DiscountController::class, 'createRequest']);
});
