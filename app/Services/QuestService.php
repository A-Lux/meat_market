<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderReview;
use App\Models\Quest;
use App\Models\User;
use App\Models\UserQuest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class QuestService
{
    public function createReviewQuest(Order $order,User $user)
    {
        $quest = Quest::create([
            'bonus' => 20,
            'title' => 'Оставьте отзыв на заказе номер '. $order->id,
            'order_id' => $order->id,
            'quest_type_id' => 3
        ]);

        $user->quests()->save($quest);
    }

    public function checkReviewForQuest(OrderReview $review)
    {
        $quest = Quest::where('order_id',$review->order_id)
            ->where('quest_type_id',3)
            ->first();
        $userQuest =  UserQuest::where('user_id',$review->user->id)
            ->where('quest_id',$quest->id)
            ->where('completed',0)->first();

        if ($userQuest){
            $review->user->update(['balance' => $review->user->balance + $quest->bonus]);
            $userQuest->update(['completed' => 1]);
        }
    }

    public function giveShareQuest(User $user)
    {
        try{
            $quest = Quest::create([
                'bonus' => 100,
                'title' => 'Поделитесь в соц сетях',
                'order_id' => null,
                'quest_type_id' => 1
            ]);

            $user->quests()->save($quest);
        }catch (\Exception $e){
            Log::error('Error in code '.$e);
        }
    }

    public function generateReturnBoxRequest(Order $order)
    {
//        $quest = Quest::create([
//            'bonus' => setting('bonus.return_bonus'),
//            'title' => 'Возврат коробки с заказа '.$order->id,
//            'order_id' => $order->id,
//            'quest_type_id' => 2
//        ]);
//
//        $order->user->quests()->save($quest);
    }

    public function giveBonusFromReturn(Order $order)
    {
        $quest = Quest::where('order_id',$order->id)
            ->where('quest_type_id',2)
            ->where('completed',0)
            ->first();

        $userQuest = UserQuest::where('quest_id',$quest->id)
            ->where('user_id',$order->user_id)
            ->where('completed',0)
            ->first();
        if ($userQuest){
            $userQuest->update(['completed' => 1]);
            $order->user->update(['balance' => $quest->user->balance+$quest->bonus]);
            $order->update(['return_time' => Carbon::now()]);
        }
    }
}
