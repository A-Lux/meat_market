<?php


namespace App\Services;


use App\Models\User;

class UserService
{

    public function addBonusFromRef($ref)
    {
        $user = User::where('ref',$ref)->first();

        if ($user){
            $user->update(['balance' => $user['balance']+setting('site.bonus')]);
        }

        return true;
    }
}
