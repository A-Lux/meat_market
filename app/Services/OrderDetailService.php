<?php


namespace App\Services;


use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ProductVariation;

class OrderDetailService
{
    public function detailsConstruct($basket, Order $order)
    {
        foreach ($basket as $item){
            if (Product::find($item['id']) && ProductVariation::find($item['variation_id'])) {
                $detail = new OrderDetail([
                    'product_id' => $item['id'],
                    'order_id' => $order['id'],
                    'price' => ProductVariation::find($item['variation_id'])->price*$item['quantity'],
                    'quantity' => $item['quantity']
                ]);

                if ($variation = ProductVariation::find($item['variation_id'])) {
                    $detail->price = $variation->price * $item['quantity'];
                    $detail->product_variation_id = $item['variation_id'];
                } else {
                    $product = Product::find($item['id']);

                    $detail->price = $product->price * $item['quantity'];
                }

                $detail->save();
            }
        }
    }
}
