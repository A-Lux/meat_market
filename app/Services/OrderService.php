<?php

namespace App\Services;

use App\Models\AreaOfDelivery;
use App\Models\DeliveryStatus;
use App\Models\DeliveryType;
use App\Models\DiscountCode;
use App\Models\PackageType;
use App\Models\PaymentStatus;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\User;

class OrderService
{
    public function calculateTotalSum($basket, $delivery_type, $package_type, $discount = null, $deliveryPrice = null)
    {
        $totalSum = 0;
        $totalQuantity = 0;
        foreach ($basket as $item) {
            if (Product::where('id', $item['id'])->exists()) {
                $product = Product::find($item['id']);
                $variation = ProductVariation::find($item['variation_id']);

                if ($variation) {
                    $totalSum += $variation->price;
                } else {
                    $totalSum += $product->price;
                }

                $totalQuantity += $item['quantity'];
            }
        }

        if ($discount) {
            $discountCode = DiscountCode::find($discount);
        }

        if ($deliveryPrice === null) {
            $deliveryPrice = DeliveryType::find($delivery_type)->additional_price;
        }

        $packageType = PackageType::find($package_type);

        return [
            'price' => $totalSum + $deliveryPrice + $packageType->additional_price,
            'total_quantity' => $totalQuantity,
            'delivery_price' => $deliveryPrice,
            'delivery_status_id' => DeliveryStatus::find(1)->id,
            'payment_status_id' => PaymentStatus::find(1)->id
        ];
    }

    public function calculateOnlyProductsTotalSum($basket): int
    {
        $totalSum = 0;

        foreach ($basket ?: [] as $item) {
            if (Product::where('id', $item['id'])->exists()) {
                $product = Product::find($item['id']);
                $variation = ProductVariation::find($item['variation_id']);

                if ($variation) {
                    $totalSum += $variation->price;
                } else {
                    $totalSum += $product->price;
                }
            }
        }

        return $totalSum;
    }

    public function calculateWithBonus(User $user, $data)
    {
        if ($user['balance'] < $data['price']) {
            $data['price'] = $data['price'] - $user['balance'];
            $data['used_balance'] = $user['balance'];

            if ($data['payment_type_id'] == 2) {
                $user['balance'] = 0;
            }
        } else {
            $data['price'] = 0;
            $data['used_balance'] = $data['price'];

            if ($data['payment_type_id'] == 2) {
                $user['balance'] = $user['balance'] - $data['used_balance'];
            }
        }

        return $data;
    }


    public function checkDiscountCode($discountId, $total)
    {
        $discountCode = DiscountCode::find($discountId);
        if ($discountCode->counter > 0) {

            $discountCode->update(['counter' => $discountCode->counter - 1]);

            if ($total > $discountCode->discount_price)
                return  $total-$discountCode->discount_points;
            else
                return  0;
        }
        return $total;

    }

    public function areaOfDelivery($total,$id)
    {
        $area = AreaOfDelivery::find($id);

        if(!$area)
            return $total;

        return  $total+ $area->price;
    }

    protected function getCategory(array $productIds): ?int
    {
        $categoryId = null;

        foreach ($productIds as $index => $id) {
            $product = Product::find($id);

            if ($index !== 0 && ($categoryId !== $product['shop_category_id'] || !$product['is_set'])) {
                $categoryId = null;
                break;
            }

            $categoryId = $product['shop_category_id'];
        }

        return $categoryId;
    }

    public function getFreePackage($basket)
    {
        $categoryId = $this->getCategory(array_map(function ($item) {
            return $item['id'];
        }, $basket ?: []));

        $totalSum = $this->calculateOnlyProductsTotalSum($basket);

        if ($categoryId === 8 && $totalSum >= 10000) {
            return PackageType::find(1)->translate(\App::getLocale());
        } else if ($totalSum >= 15000) {
            return PackageType::find(2)->translate(\App::getLocale());
        }

        return null;
    }
}
