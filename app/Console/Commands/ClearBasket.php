<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearBasket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'basket:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all expired baskets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        DB::table('product_variations')
            ->where(
                'reservation_expired_at',
                '<',
                Carbon::now()->setTimezone('Asia/Almaty')
            )->update([
                'is_reserved' => false,
                'reservation_expired_at' => null
            ]);
    }
}
