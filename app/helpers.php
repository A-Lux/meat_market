<?php

function get_current_region_1c_id()
{
    $regionId = request()->cookie('city_id', 1);

    if ($city = \App\Models\City::find($regionId)) {
        $output = $city['1c_id'];
    } else {
        $output = \App\Models\City::find(1)['1c_id'];
    }

    return $output;
}
