<?php

namespace App\Http\Middleware;

use App\Cicada\CRest;
use Closure;
use Illuminate\Http\Request;

class SetBitrixAuthProperties
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        CRest::$C_REST_CLIENT_ID = config('bitrix.client_id');
        CRest::$C_REST_CLIENT_SECRET = config('bitrix.client_secret');

        return $next($request);
    }
}
