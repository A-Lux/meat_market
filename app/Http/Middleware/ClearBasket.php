<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class ClearBasket
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $basketAlmatyExpiredAt = session()->get('basket_almaty_expired_at');
        $basketNurSultanExpiredAt = session()->get('basket_nur_sultan_expired_at');

        if ($basketAlmatyExpiredAt) {
            if ($basketAlmatyExpiredAt < Carbon::now()) {
                session()->remove('basket_almaty_expired_at');
                session()->remove('basket_almaty');
            }
        }

        if ($basketNurSultanExpiredAt) {
            if ($basketNurSultanExpiredAt < Carbon::now()) {
                session()->remove('basket_nur_sultan_expired_at');
                session()->remove('basket_nur_sultan');
            }
        }

        return $next($request);
    }
}
