<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required:min:8',
            'date_of_delivery' => 'required',
            'last_name' => 'required',
            'user_id' => 'nullable|numeric',
            'delivery_type_id' => 'required',
            'package_type_id' => 'required',
            'payment_type_id' => 'required',
            'comment' => 'nullable'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        return response([
            'message' => $validator->errors()->first()
        ], 422);
    }
}
