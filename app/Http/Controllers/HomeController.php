<?php

namespace App\Http\Controllers;

use App\Models\CowPart;
use App\Models\PolicyPage;
use App\Models\Product;
use App\Models\Recipes;
use App\Models\Set;
use App\Models\SliderBanner;
use App\Models\TastePoint;
use App\Models\Tip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->has('ref')){
            session()->put('ref',$request->ref);
            session()->save();
        }

        $points = TastePoint::get();

        /** @var Product[] $products */
        $products = Product::whereHas('variations')
            ->where('bestseller', 1)
            ->where('is_frozen', false)
            ->orWhere(function ($query) {
                $query->where('is_frozen', true)->whereNull('group_id');
            })->get();

        foreach ($products as $product) {
            $product->pair = $product->group_pair;
        }

        $banners = SliderBanner::get();
        $cowParts = CowPart::get();
        $sets = Set::with('products')->get()->translate(App::getLocale());

        return view('main.index', compact('points', 'products', 'banners', 'cowParts', 'sets'));
    }


    public function tipsAndRecipes(Request $request)
    {
        if (!$request->has('section')) {
            return redirect()->route('tipsAndRecipes', ['section' => 'tips']);
        }

        if ($request->input('section', 'tips') === 'tips') {
            $articles = Tip::orderBy('id', 'asc')->get();
        } else {
            $articles = Recipes::orderBy('id', 'asc')->get();
        }

        $articles = $articles->translate(App::getLocale());

        return view('tips-recipes.index', compact('articles'));
    }

    public function recipePage(Request $request, $id)
    {
        if (!$request->has('section')) {
            return redirect('/tips-recipes/' . $id . '?section=tips');
        }

        if ($request->input('section', 'tips') === 'tips') {
            $article = Tip::findOrFail($id);
            $view = 'tip.index';
        } else {
            $article = Recipes::findOrFail($id);
            $view = 'recipe.index';
        }

        $article = $article->translate(App::getLocale());

        return view($view, compact('article'));
    }

    public function showPublicOffer()
    {
        if (!$privacyPage = PolicyPage::where('page', 'public-offer')->get()->first()) {
            abort(404);
        }

        $privacyPage = $privacyPage->translate(App::getLocale());
        $content = $privacyPage->content;

        return view('public-offer.index', compact('content'));
    }

    public function showPrivacyPolicy()
    {
        if (!$privacyPage = PolicyPage::where('page', 'privacy-policy')->get()->first()) {
            abort(404);
        }

        $privacyPage = $privacyPage->translate(App::getLocale());
        $content = $privacyPage->content;

        return view('privacy-policy.index', compact('content'));
    }

    public function showOnlinePayments()
    {
        if (!$privacyPage = PolicyPage::where('page', 'online-payments')->get()->first()) {
            abort(404);
        }

        $privacyPage = $privacyPage->translate(App::getLocale());
        $content = $privacyPage->content;

        return view('online-payments.index', compact('content'));
    }
}
