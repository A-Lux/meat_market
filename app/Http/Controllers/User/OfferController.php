<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\OfferRequest;
use App\Models\Offer;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    //
    public function store(OfferRequest $request)
    {
        $data = $request->all();

        $offer = Offer::create($data);

        return response(['offer' => $offer],201);
    }
}
