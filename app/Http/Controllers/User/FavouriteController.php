<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\FavoriteProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class FavouriteController extends Controller
{
    //

    public function index(Request $request)
    {
        $products = $request->user()->favourite;

        foreach ($products as $product) {
            $product['pair'] = $product->group_pair;
        }

        return view('favorite.index',compact('products'));
    }

    public function add(Request $request,Product $product)
    {
        $favourite = FavoriteProduct::where('user_id',$request->user()->id)
            ->where('product_id',$product->id)
            ->first();

        if (!$favourite){
            $request->user()->favourite()->save($product);

            return response(['message' => 'Ваш продукт был добавлен в избранное'],201);
        }else{
            $request->user()->favourite()->detach($product);
            return response(['message' => 'Ваш продукт был удален'],200);

        }
    }

    public function remove(Request $request,Product $product)
    {
        $favourite = FavoriteProduct::where('user_id',$request->user()->id)
            ->where('product_id',$product->id)
            ->first();
        if ($favourite){
            $favourite->delete();
            return response(['message' => 'Ваш продукт был удален с избранных'],200);

        }else{

            return response(['message' => 'Вы не добавляли этот продукт в избранное'],200);
        }

    }

    public function show(Request $request)
    {
        $products = $request->user()->favourite;
        foreach ($products as $product){
            $product['variations'] = $product->variations;
        }

        return response(['products' => $products],200);
    }
}
