<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApplicationRequest;
use App\Models\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function store(ApplicationRequest $request)
    {
        $data = $request->all();

        $application = Application::create($data);

        return response(['application' => $application],201);
    }
    //
}
