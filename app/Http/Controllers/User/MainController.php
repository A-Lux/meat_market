<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\DeliveryAddress;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use ZipArchive;
use SimpleXMLElement;
use App\Models\Product;
use DB;

class MainController extends Controller
{
    //
    public function getAddresses(Request $request)
    {
        return response(['addresses' => $request->user()->addresses], 200);

    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function c_exchange(Request $request)
    {

        \Log::info('1c request', compact('request'));

        if ($request->mode == "checkauth") {
            return "success\nCookie\nnull";
        } else if ($request->mode == "init") {
            return "zip=yes\nfile_limit=500000000";
        } else if ($request->mode == "file") {
            if (!file_exists("./bitrix/update/" . $request->filename)) {
                $text = ($request->instance()->getContent());
                $fp = fopen("./bitrix/update/" . $request->filename, "w");
                fwrite($fp, $text);
                fclose($fp);
            }
            $zip = new ZipArchive();
            $res = $zip->open("./bitrix/update/" . $request->filename);
            $url = "./bitrix/update/" . $request->filename;
            if ($res === TRUE) {
                $nameDirEnd = explode(".", $request->filename);
                $dir = '../storage/app/public/unzip/' . $nameDirEnd[0];
                if (!is_dir($dir)) {
                    mkdir($dir);
                }
                $zip->extractTo($dir);
                $zip->close();
                if (file_exists($dir . "/import0_1.xml")) {
                    DB::table("product_variations")->update(["in_stock" => false]);
                }


                if (file_exists($dir . "/import0_1.xml") || file_exists($dir . "/import1_1.xml") || file_exists($dir . "/import1_1.xml")) {


                    $isFrozen = file_exists($dir . "/import0_1.xml") ? true : false;

                    if ($isFrozen) {
                        foreach (\App\Models\Product::get() as $prod) {
                            $prod->delete();
                        }
                    }


                    $movies = new SimpleXMLElement(file_get_contents($dir . "/" . (file_exists($dir . "/import0_1.xml") ? "import0_1.xml" : "import1_1.xml")));
                    $movies = json_decode(json_encode($movies), true);


                    foreach ($movies["Каталог"]["Товары"]["Товар"] as $product) {
                        Product::withTrashed()->where('nomenclatura', $product["Ид"])->restore();
                        $productNew = Product::where("nomenclatura", $product["Ид"])->first();
                        if (is_null($productNew)) {
                            $productNew = new Product;
                        }
                        $catalogs = null;
                        if (isset($product["ЗначенияРеквизитов"]["ЗначениеРеквизита"]["Наименование"])) {
                            if ($product["ЗначенияРеквизитов"]["ЗначениеРеквизита"]["Наименование"] == "ВидНоменклатуры") {
                                $catalogs = \App\Models\ShopCategory::where("name", "LIKE", "%" . $product["ЗначенияРеквизитов"]["ЗначениеРеквизита"]["Значение"] . "%")->first();
                                if (is_null($catalogs)) {
                                    $catalogs = new \App\Models\ShopCategory;
                                }
                                $catalogs->name = $rqst["Значение"];
                                $catalogs->save();
                            }
                        }
                        $ang_name = "";
                        $rus_name = "";
                        foreach ($product["ЗначенияРеквизитов"]["ЗначениеРеквизита"] as $rqst) {
                            if (isset($rqst["Наименование"])) {
                                if ($rqst["Наименование"] == "ВидНоменклатуры") {
                                    $array = [
                                        "Мраморная говядина" => "Marbled Beef",
                                        "Порционные стейки" => "Individual Steaks",
                                        "Субпродукты" => "By-products",
                                    ];
                                    $is_ehere = null;
                                    if (isset($array[$rqst["Значение"]])) {
                                        $is_ehere = $array[$rqst["Значение"]];
                                    }


                                    $catalogs = \App\Models\ShopCategory::where("name", "LIKE", "%" . $rqst["Значение"] . "%")->first();
                                    if (is_null($catalogs)) {
                                        if (!is_null($is_ehere)) {
                                            $catalogs = \App\Models\ShopCategory::where("name", "LIKE", "%" . $is_ehere . "%")->first();
                                        }
                                    }
                                    if (is_null($catalogs)) {
                                        $catalogs = new \App\Models\ShopCategory;
                                        $catalogs->name = $rqst["Значение"];
                                        $catalogs->save();
                                    }
                                }

                                if ($rqst["Наименование"] == "НаименованиеАнгл") {
                                    $ang_name = $rqst["Значение"];
                                }

                                if ($rqst["Наименование"] == "Полное наименование") {
                                    $rus_name = $rqst["Значение"];
                                }
                            }
                        }


                        if (!is_null($catalogs)) {
                            $productNew->nomenclatura = $product["Ид"];
                            $productNew->name = $ang_name;
//                            $productNew->name = $product["Наименование"];
                            $productNew->shop_category_id = !is_null($catalogs) ? $catalogs->id : "";
                            $productNew->breed_id = "7";
                            $productNew->price = 0;
                            $productNew->storage_type_id = "1";

                            $productNew->is_frozen = $isFrozen;

                            if (isset($product["Группы"]) && isset($product["Группы"]["Ид"])) {
                                $productNew->group_id = $product["Группы"]["Ид"];
                            }

                            if (isset($product["Картинка"])) {

                                if (is_array($product["Картинка"])) {
                                    $productNew->main_image = str_replace("import_files/", "unzip/" . $nameDirEnd[0] . "/import_files/", $product["Картинка"][0]);
                                    $productNew->images = str_replace("import_files/", "unzip/" . $nameDirEnd[0] . "/import_files/", $product["Картинка"][0]);
//                                    $arrayImgReturn = [];
//                                    foreach ($product["Картинка"] as $imgArray) {
//                                        array_push($arrayImgReturn, str_replace("import_files/", "unzip/" . $nameDirEnd[0] . "/import_files/", $imgArray));
//                                    }
//                                    $productNew->images = json_encode($arrayImgReturn);

                                } else {
                                    $productNew->main_image = str_replace("import_files/", "unzip/" . $nameDirEnd[0] . "/import_files/", $product["Картинка"]);
                                    $productNew->images = str_replace("import_files/", "unzip/" . $nameDirEnd[0] . "/import_files/", $product["Картинка"]);
                                }
                            }

                            $productNew->save();

                            DB::table("translations")->where("table_name", "products")->where("column_name", "name")->where("locale", "ru")->where("foreign_key", $productNew->id)->delete();
                            DB::table("translations")->insert(["table_name" => "products", "column_name" => "name", "locale" => "ru", "foreign_key" => $productNew->id, "value" => $rus_name]);
                        }
                    }
                }

                //

                if (file_exists($dir . "/import3_1.xml")) {
//                    \App\Models\PackageType::truncate();
                    $movies = new SimpleXMLElement(file_get_contents($dir . "/import3_1.xml"));
                    $movies = json_decode(json_encode($movies), true);
                    foreach ($movies["Каталог"]["Товары"]["Товар"] as $product) {
                        $productNew = \App\Models\PackageType::where("numenclaturas", "" . $product["Ид"])->first();
                        if (is_null($productNew)) {
                            $productNew = new \App\Models\PackageType;
                        }
                        $catalogs = null;
                        $ang_name = "";
                        $rus_name = "";
                        foreach ($product["ЗначенияРеквизитов"]["ЗначениеРеквизита"] as $rqst) {
                            if (isset($rqst["Наименование"])) {
                                if ($rqst["Наименование"] == "НаименованиеАнгл") {
                                    $ang_name = $rqst["Значение"];
                                }
                                if ($rqst["Наименование"] == "Полное наименование") {
                                    $rus_name = $rqst["Значение"];
                                }
                            }
                        }
                        $productNew->numenclaturas = "" . $product["Ид"];
                        $productNew->name = $ang_name;
//                        $productNew->additional_price = 0;
                        $productNew->save();
                        DB::table("translations")->where("table_name", "package-types")->where("column_name", "name")->where("locale", "ru")->where("foreign_key", $productNew->id)->delete();
                        DB::table("translations")->insert(["table_name" => "package-types", "column_name" => "name", "locale" => "ru", "foreign_key" => $productNew->id, "value" => $rus_name]);
                    }
                }

                if (file_exists($dir . "/offers3_1.xml")) {
                    $movies = new SimpleXMLElement(file_get_contents($dir . "/offers3_1.xml"));
                    $movies = json_decode(json_encode($movies), true);
                    foreach ($movies["ПакетПредложений"]["Предложения"]["Предложение"] as $catalog) {
                        $ves = null;
                        if (isset($catalog["Ид"])) {
                            $prod = \App\Models\PackageType::where("numenclaturas", $catalog["Ид"])->first();
                            if (!is_null($prod)) {
//                                $prod->additional_price = ($catalog["Цены"]["Цена"]["ЦенаЗаЕдиницу"]);
                                $prod->save();
                            }
                        }
                    }
                }

                if (file_exists($dir . "/import4_1.xml")) {
                    $movies = new SimpleXMLElement(file_get_contents($dir . "/import4_1.xml"));
                    $movies = json_decode(json_encode($movies), true);
                    foreach ($movies["Каталог"]["Товары"]["Товар"] as $product) {
                        $productNew = Product::where("nomenclatura", $product["Ид"])->first();
                        if (is_null($productNew)) {
                            $productNew = new Product;
                        }
                        $catalogs = null;
                        $ang_name = "";
                        $rus_name = "";
                        foreach ($product["ЗначенияРеквизитов"]["ЗначениеРеквизита"] as $rqst) {
                            if (isset($rqst["Наименование"])) {
                                if ($rqst["Наименование"] == "НаименованиеАнгл") {
                                    $ang_name = $rqst["Значение"];
                                }
                                if ($rqst["Наименование"] == "Полное наименование") {
                                    $rus_name = $rqst["Значение"];
                                }
                            }
                        }

                        $productNew->nomenclatura = $product["Ид"];
                        $productNew->name = $ang_name;
                        $productNew->is_extra = true;
                        $productNew->price = 0;
                        $productNew->save();
                        DB::table("translations")->where("table_name", "products")->where("column_name", "name")->where("locale", "ru")->where("foreign_key", $productNew->id)->delete();
                        DB::table("translations")->insert(["table_name" => "products", "column_name" => "name", "locale" => "ru", "foreign_key" => $productNew->id, "value" => $rus_name]);
                    }
                }


                if (file_exists($dir . "/offers4_1.xml")) {
                    $movies = new SimpleXMLElement(file_get_contents($dir . "/offers4_1.xml"));
                    $movies = json_decode(json_encode($movies), true);
                    foreach ($movies["ПакетПредложений"]["Предложения"]["Предложение"] as $catalog) {
                        $ves = null;
                        if (isset($catalog["Ид"])) {
                            $prod = Product::where("nomenclatura", $catalog["Ид"])->first();
                            if (!is_null($prod)) {
                                $prod->price = ($catalog["Цены"]["Цена"]["ЦенаЗаЕдиницу"]);
                                $prod->save();
                            }
                        }
                    }
                }

                if (file_exists($dir . "/offers0_1.xml") || file_exists($dir . "/offers1_1.xml")) {
                    $movies = new SimpleXMLElement(file_get_contents($dir . "/" . (file_exists($dir . "/offers0_1.xml") ? "offers0_1.xml" : "offers1_1.xml")));
                    $movies = json_decode(json_encode($movies), true);
                    foreach ($movies["ПакетПредложений"]["Предложения"]["Предложение"] as $catalog) {
                        $is_sale = false;
                        $ves = null;
                        $expired_at = null;
                        if (isset($catalog["ЗначенияСвойств"])) {
                            foreach ($catalog["ЗначенияСвойств"]["ЗначенияСвойства"] as $rqst) {
                                if ($rqst["Наименование"] == "Вес") {
                                    $ves = $rqst["Значение"];
                                }
                                if ($rqst["Наименование"] == "Срок годности") {
                                    $expired_at = $rqst["Значение"];
                                }
                                if ($rqst["Наименование"] == "Размер скидки") {
                                    $is_sale = true;
                                }
                            }
                        }
                        if (!is_null($ves)) {

                            $date = explode("#", $catalog["Ид"]);
                            $prod = Product::where("nomenclatura", $date[0])->first();
                            if (!is_null($prod)) {

                                $variation = \App\Models\ProductVariation::where("nomenclatura", $catalog["Ид"])->first();
                                if (is_null($variation)) {
                                    $variation = new \App\Models\ProductVariation;
                                }
                                $variation->name = $ves;
                                $variation->nomenclatura = $catalog["Ид"];
                                $variation->price = (int)($catalog["Цены"]["Цена"]["ЦенаЗаЕдиницу"]);
                                $variation->product_id = $prod->id;
                                $variation->is_reserved = false;
                                $variation->reservation_expired_at = null;
                                $variation->in_stock = true;
                                if ($is_sale) {
                                    $variation->is_sale = true;
                                    $prod->has_sale = true;
                                }
                                $variation->{'1c_region_id'} = $catalog["Склад"]["@attributes"]["ИдСклада"];
                                if (!is_null($expired_at)) {
                                    $variation->expired_at = date("Y-m-d H:i:s", strtotime($expired_at));
                                }
                                $variation->save();
                                $prod->price = ($catalog["Цены"]["Цена"]["ЦенаЗаЕдиницу"]);
                                $prod->save();

                            }
                        }

                    }
                }
                unlink($url);
            } else {
                return "failure";
            }

            return "success";
        } else if ($request->mode == "import") {
//            Log::debug(($request->instance()->getContent()));
//            Log::debug(json_encode($_FILES));
            if (file_exists("./bitrix/update/" . $request->filename)) {
                return "success";
            }

            return "success";
        }
    }

    public function install(Request $request)
    {

        $result = \App\Cicada\CRest::installApp();
        $returs_s = '';
        if ($result['rest_only'] === false) {
            $returs_s .= '<head>
                <script src="//api.bitrix24.com/api/v1/"></script>';
            if ($result['install'] == true) {
                $returs_s .= ' <script>
                        BX24.init(function(){
                            BX24.installFinish();
                        });
                    </script>';
            }
            $returs_s .= '
            </head>
            <body>
            ';
            if ($result['install'] == true) {
                $returs_s .= 'installation has been finished';
            } else {
                $returs_s .= 'installation error';
            }
            $returs_s .= '</body>';
        }
        return $returs_s;
    }

    public function updateData(Request $request)
    {
        $data = $request->all();
        $user = User::find($request->user_id);
        $user->update($data);


        return response(['user' => $user], 200);
    }

    public function profile()
    {
        return view('cabinet.index');
    }

    public function addresses(Request $request)
    {
        $addresses = $request->user()->addresses;
        return view('cabinet.address', compact('addresses'));
    }


    public function store(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'user_id' => 'bail|required',
                    'address' => 'bail|required',
                    'street' => 'bail|required',
                    'house' => 'bail|required',
                    'flat' => 'bail|required',
                    'entrance' => 'bail|required',
                    'floor' => 'bail|required',
                    'title' => 'bail|required',
                    'coords' => 'bail|required|json'
                ],
                [
                    'user_id.required' => __('content.user_id.required'),
                    'address.required' => __('content.address.required'),
                    'street.required' => __('content.street.required'),
                    'house.required' => __('content.house.required'),
                    'flat.required' => __('content.flat.required'),
                    'entrance.required' => __('content.entrance.required'),
                    'floor.required' => __('content.floor.required'),
                    'title.required' => __('content.title.required'),
                    'coords.required' => __('content.coords.required'),
                    'coords.json' => __('content.coords.json')
                ]
            );

            if ($validator->fails()) {
                throw new \Exception($validator->errors()->first());
            }

            /** @var User|null $user */
            if (!$user = User::find($request->input('user_id'))) {
                throw new \Exception(__('content.user-not-found'));
            }

            $addressFullName = $request->input('address') . ', квартира ' . $request->input('flat')
                . ', подъезд ' . $request->input('entrance') . ', этаж ' . $request->input('floor');

            $deliveryAddress = new DeliveryAddress([
                'name' => $addressFullName,
                'street' => $request->input('street'),
                'house' => $request->input('house'),
                'user_id' => $user->id,
                'flat' => $request->input('flat'),
                'entrance' => $request->input('entrance'),
                'floor' => $request->input('floor'),
                'comment' => $request->input('comment'),
                'title' => $request->input('title'),
                'coords' => $request->input('coords')
            ]);

            if ($user->addresses()->count() > 0) {
                $deliveryAddress['is_main'] = false;
            } else {
                $deliveryAddress['is_main'] = true;
            }

            $deliveryAddress->save();

            return response(['addresses' => $request->user()->addresses], 201);
        } catch (\Throwable $e) {
            return response(['message' => $e->getMessage()], 422);
        }
    }

    public function deleteAddress(DeliveryAddress $address, Request $request)
    {
        $address->delete();
        return response(['addresses' => $request->user()->address]);
    }

    public function updateAddress(DeliveryAddress $address, Request $request)
    {
        $data = $request->all();
        $address->update($data);

        foreach ($request->user()->addresses as $apartments) {
            if ($apartments['id'] != $address['id']) {
                $apartments->update([
                    'is_main' => 0
                ]);
            }
        }

        return response(['address' => $address]);
    }

    public function getUserData(Request $request)
    {
        return response(['user' => $request->user()]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        // Hi there
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
}
