<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackRequest;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    //
    public function store(FeedbackRequest $request)
    {
        $data = $request->all();

        $feedback = Feedback::create($data);

        return response(['feedback' => $feedback],201);
    }
}
