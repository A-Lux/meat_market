<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\PasswordRecovery;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class PasswordRecoveryController extends Controller
{
    public function index(Request $request): \Illuminate\Http\RedirectResponse
    {
        $request->flashOnly(['email']);

        if ($user = User::where('email', $request->input('email'))->limit(1)->get()->first()) {
            $newPassword = '';

            while (true) {
                $newPassword .= mt_rand(0, 9);

                if (strlen($newPassword) >= 8) {
                    break;
                }
            }

            Mail::to($user->email)
                ->send(new PasswordRecovery($newPassword));

            $user->password = Hash::make($newPassword);
            $user->save();
        }

        if (User::where('email', $request->input('email'))->count() > 0) {
            return redirect('/#reset-password-modal')
                ->with(['password_reset_success_send' => true]);
        }

        return redirect('/#reset-password-modal')
            ->with(['password_reset_success' => true]);
    }
}
