<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Quest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuestController extends Controller
{
    //
    public function index(Request $request)
    {
        $quests = \Auth::user()->quests()->wherePivot('completed', false)->get();

        return view('cabinet.tasks', compact('quests'));
    }

    public function shareToSocialNets(Request $request)
    {
        if (!$user = Auth::user()) {
            abort(403);
        }

        if (!$quest = Quest::find($request->input('quest_id'))) {
            abort(404);
        }

        $socialNetwork = $request->input('social_network');

        switch ($socialNetwork) {
            case 'fb':
                $link = 'https://www.facebook.com/sharer/sharer.php?u=' . $request->getHttpHost();
                break;
            case 'wa':
                $link = 'https://wa.me/?text=Самое лучшее мясо в Казахстане: https://meat-market.a-lux.dev';
                break;
            case 'tg':
                $link = 'https://telegram.me/share/url?url='
                    . $request->getHttpHost()
                    . '&amp;text=Самое лучшее мясо в Казахстане';
                break;
            default:
                abort(404);
                break;
        }

        $currentUserBonuses = $user->balance ?: 0;

        $user->balance = $currentUserBonuses + 20;
        $user->save();

        DB::table('user_quest')
            ->where([
                ['user_id', '=', $user->id],
                ['quest_id', '=', $quest->id]
            ])
            ->update(['completed' => true]);

        return redirect($link);
    }
}
