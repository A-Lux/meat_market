<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\CowPart;
use App\Models\Product;
use App\Models\ShopCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function cowParts(CowPart $part)
    {
        $categories = ShopCategory::where('id','<',5)->get();

        return view('category.index',compact('part','categories'));
    }

    public function index($id)
    {
        $categories = ShopCategory::where('id','<',5)->get();

        $isSale = $id === 'sale';

        $id = $isSale ? null : ShopCategory::find($id);

        return view('category.index',compact('id','categories', 'isSale'));
    }

    public function show(Product $product)
    {
        $parts = CowPart::get();
        $orders = $product->orders()->get();
        $reviews = [];

        foreach ($orders as $order) {
            foreach ($order->reviews()->get() as $review) {
                $reviews[] = $review;
            }
        }

        $product['variations'] = $product->variations()
            ->orderBy('expired_at')
            ->where('1c_region_id', get_current_region_1c_id())
            ->where('in_stock', true)
            ->where('is_reserved', false)
            ->limit(3)->get();

        $product = $product->translate(\App::getLocale());

        return view('product.index', compact('product','parts', 'reviews'));
    }
}
