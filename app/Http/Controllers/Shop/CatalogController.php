<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\CatalogFilterRequest;
use App\Models\Product;
use DB;

class CatalogController extends Controller
{
    public function index(CatalogFilterRequest $request)
    {
        $products = Product::
        select(
            'products.*',
            DB::raw('(select count(*) from product_variations where product_variations.product_id = products.id AND product_variations.is_reserved=false AND product_variations.in_stock=true  GROUP BY product_id) as count_sort'),
        )
            ->where(function ($query) use ($request) {
                if ($request->has('search')) {
                    if (strlen($request->search) > 3) {
                        $request->search = '%' . $request->search . '%';
                    } else {
                        $request->search = $request->search . '%';
                    }

                    DB::table("translations")->where("table_name", "package-types")->where("column_name", "name")->where("locale", "ru");
//                foreign_key

                    $talbes = DB::table("translations")->where("table_name", "products")->where("column_name", "name")->where("locale", "ru")
                        ->where("value", "iLIKE", $request->search)
                        ->get()->groupby("foreign_key")->toarray();
                    $talbes = array_keys($talbes);

                    $query->where(function ($querySearchLang) use ($request, $talbes) {
                        $querySearchLang->where('name', 'iLIKE', $request->search);
                        $querySearchLang->orWhereIn('id', $talbes);
                    });
                }

                if ($request->has('category') && $request->category !== '' && $request->category !== 'sale') {
                    $query->where('shop_category_id', $request->category);
                }

                if ($request->category === 'sale') {
                    $query->where('has_sale', true);
                }

                if ($request->has('cow_part') && $request->cow_part != '') {
                    $query->where('cow_part_id', $request->cow_part);
                }

                $query->where(function ($query) {
                    $query->where('is_frozen', false)
                        ->orWhere(function ($query) {
                            $query->where('is_frozen', true)->whereNull('group_id');
                        });
                });
            })
            ->whereNull('set_id')
            ->with(['category', 'breed', 'storage', 'variations' => function ($query) use ($request) {
                $query->orderBy('expired_at')
                    ->where('1c_region_id', get_current_region_1c_id())
                    ->where('in_stock', true)
                    ->where('is_reserved', false)
                    ->where(function ($query) use ($request) {
                        if ($request->category === 'sale' && !$request->input('search', false)) {
                            $query->where('is_sale', true);
                        } else if ($request->category !== 'sale') {
                            $query->where('is_sale', false);
                        }
                    });
            }])
            ->where('is_extra', false)
            ->withCount('variations')
            ->orderBy('variations_count', 'desc')
            ->limit($request->input('limit', 15))
            ->get();



        foreach ($products as $index => $product) {
            $product->name = $product->translate(\App::getLocale())->name;

            $product['pair'] = $product->group_pair;

            if (count($product->variations) === 0 and $request->category === 'sale') {
                unset($products[$index]);
            }
        }

//        $products = collect($products)->sortByDesc(function ($product) {
//            if (count($product['variations']) > 0 && $product['pair']) {
//                return 2;
//            } else if (count($product['variations']) === 0 && $product['pair']) {
//                return 1;
//            } else {
//                return 0;
//            }
//        })->values();

        return response()->json(compact('products'));
    }
}
