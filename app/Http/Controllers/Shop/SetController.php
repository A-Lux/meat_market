<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Set;
use Illuminate\Support\Facades\App;

class SetController extends Controller
{
    public function index()
    {
        $sets = Set::whereHas('fakeProduct')->get()->translate(App::getLocale());

        return view('set.index', compact('sets'));
    }

    public function show($id)
    {
        $set = Set::with('products')->findOrFail($id)->translate(App::getLocale());

        return view('set.set-info', compact('set'));
    }
}
