<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\CowPart;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function show($id)
    {

    }

    public function getByCategory($id)
    {
        $products = CowPart::findOrFail($id)->products()->where(function ($query) {
            $query->where(function ($query) {
                $query->where('is_frozen', false)
                    ->orWhere(function ($query) {
                        $query->where('is_frozen', true)->whereNull('group_id');
                    });
            });
        })->get();

        foreach ($products as $product) {
            $product->name = $product->translate(\App::getLocale())->name;

            $product['pair'] = $product->group_pair;
            $product['variations'] = $product->variations()->orderBy('expired_at')
                ->where('1c_region_id', get_current_region_1c_id())
                ->where('in_stock', true)
                ->where('is_reserved', false)
                ->limit(3)->get();
        }

        return response()->json($products->toArray());
    }

    public function getNestedVariations(Request $request)
    {
        $excludeVariationId = $request->input('excluded_variation_id', 0);

        /** @var Product $product */
        $product = Product::find($request->input('product_id'));

        return $product->variations()
            ->where('id', '!=', $excludeVariationId)
            ->where('1c_region_id', get_current_region_1c_id())
            ->where('in_stock', true)
            ->where('is_reserved', false)
            ->orderBy('expired_at')
            ->limit(3)
            ->get();
    }

    public function getNestedVariation(Request $request)
    {
        /** @var Product $product */
        if (!$product = Product::find($request->input('product_id'))) {
            abort(404);
        }

        return $product->variations()
            ->where('in_stock', true)
            ->where('1c_region_id', get_current_region_1c_id())
            ->where('is_reserved', false)
            ->orderBy('expired_at')
            ->offset(2)
            ->limit(1)
            ->get();
    }
}
