<?php

namespace App\Http\Controllers;

use App\Models\OrderReview;
use Illuminate\Http\Request;

class OrderReviewController extends Controller
{
    //

    public function store(Request $request)
    {
        $data = $request->only([
            'comments',
            'order_id',
            'service_level',
            'site_design',
            'delivery_quality',
            'quality',
        ]);
        $data['user_id'] = $request->user()->id;

        $review = OrderReview::create($data);
        $current = date('Y-m-d H:i:s');
        $nextMonth = date('Y-m-d H:i:s');
        $addTask = \App\Cicada\CRest::call(
            'crm.deal.add',
            ["fields" =>
                [
                    "TITLE"=>  "Мраморный стейк \n Телефон:asd",
                    "TYPE_ID"=>  "GOODS",
                    "STAGE_ID"=>  "NEW",
                    "COMPANY_ID"=>  3,
                    "CONTACT_ID"=>  3,
                    "OPENED"=>  "Y",
                    "ASSIGNED_BY_ID"=>  1,
                    "PROBABILITY"=>  30,
                    "CURRENCY_ID"=>  "KZT",
                    "OPPORTUNITY"=>  5000,
                    "CATEGORY_ID"=>  5,
                    "BEGINDATE"=> ($current),
                    "CLOSEDATE"=> ($nextMonth)
                ]
            ]
        );

        dd($review);

        return response(['review' => $review],201);
    }
}
