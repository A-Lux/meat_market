<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\AreaRequest;
use App\Models\AreaOfDelivery;
use Illuminate\Http\Request;

class DeliveryTypeController extends Controller
{
    //
    public function getAreas(AreaRequest $request)
    {
        $area_of_deliveries = AreaOfDelivery::where('delivery_type_id', $request->delivery_type)
            ->where('city_id', $request->input('city_id', 1))->get();

        return response(['area_of_deliveries' => $area_of_deliveries],200);
    }
}
