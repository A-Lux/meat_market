<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\DiscountCode;
use App\Models\DiscountRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class DiscountController extends Controller
{
    //
    public function index(Request $request)
    {
        $request->validate([
            'code' => 'required'
        ]);

        $discount_code = DiscountCode::where('code',$request->code)
            ->where('counter','>',0)
            ->first();


        if ($discount_code){
            session()->put('discount_code',$discount_code);

            session()->save();
            return response(['discount_code' => $discount_code]);
        }
        return response(['discount_code' => []],404);
    }

    /**
     * Create discount request.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException|Exception
     */
    public function createRequest(Request $request): JsonResponse
    {
        $this->validate(
            $request,
            [
                'email' => 'bail|required|email|max:255'
            ],
            [
                'email.required' => 'Email required!',
                'email.email' => 'Invalid email!',
                'email.max' => 'Email must not be more than 255 characters!'
            ]
        );

        $discountRequest = new DiscountRequest([
            'email' => $request->input('email')
        ]);

        if (!$discountRequest->save()) {
            throw new Exception('Failed to save discount request!');
        }

        return response()->json([
            'message' => __('content.request-sent-successfully')
        ], 201);
    }
}
