<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Models\DeliveryAddress;
use App\Models\DeliveryType;
use App\Models\Order;
use App\Models\PackageType;
use App\Models\PaymentType;
use App\Models\Quest;
use App\Models\Set;
use App\Services\OrderDetailService;
use App\Services\OrderService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    //
    protected $orderService;
    /**
     * @var OrderDetailService
     */
    protected $orderDetailService;

    public function __construct()
    {
        $this->orderService = new OrderService();
        $this->orderDetailService = new OrderDetailService();
    }

    public function orderEdit(Request $request)
    {
        if (get_current_region_1c_id() === config('regions_1c.almaty')) {
            $basketMain = session('basket_almaty');
        } else if (get_current_region_1c_id() === config('regions_1c.nur-sultan')) {
            $basketMain = session('basket_nur_sultan');
        } else {
            return back();
        }

        $package_type = PackageType::get();
        $user = $request->user();
        $delivery_types = DeliveryType::get();
        $basket = new BasketController();

        $totals = $basket->getTotals($basketMain);


        $yandexMapGeoJson = get_current_region_1c_id() === config('regions_1c.almaty') ?
            setting('obycnaya-dostavka.general-delivery-almaty') :
            setting('obycnaya-dostavka.general-delivery-nursultan');

        $yandexMapGeoJson = \Storage::get('public/' . json_decode($yandexMapGeoJson)[0]->download_link);

        if (get_current_region_1c_id() === config('regions_1c.almaty')) {
            $yandexMapCenterJson = '[76.889709, 43.238949]';
        } else {
            $yandexMapCenterJson = '[71.425134, 51.149605]';
        }

        return view('basket.delivery-edit', [
            'package_type' => $package_type,
            'delivery_types' => $delivery_types,
            'user' => $request->user(),
            'totals' => $totals,
            'basket' => $basketMain,
            'yandexMapGeoJson' => $yandexMapGeoJson,
            'yandexMapCenterJson' => $yandexMapCenterJson
        ]);
    }


    /**
     * @throws Exception
     */
    public function order(OrderRequest $request)
    {
        $currentRegion = get_current_region_1c_id();


        if ($currentRegion === config('regions_1c.almaty')) {
            $basket = session()->get('basket_almaty');
        } else if ($currentRegion === config('regions_1c.nur-sultan')) {
            $basket = session()->get('basket_nur_sultan');
        } else {
            throw new Exception('Регион не найден!');
        }

        if ($basket == null) {

            return abort(500, 'Корзина пуста');
        }

        $data = $request->all();

        $emails = "";

        if (isset($data["email"])) {
            $emails = $data["email"];
        }

        $totals = $this->orderService->calculateTotalSum(
            $basket,
            $data['delivery_type_id'],
            $data['package_type_id'],
            null,
            $data['delivery_price']
        );

        if (isset($data['discount_code_id'])) {
            $totals['price'] = $this->orderService->checkDiscountCode($data['discount_code_id'], $totals['price']);
            session()->forget('discount_code');
            session()->save();
        }

//        if (isset($data['area_of_delivery_id'])) {
//            $totals['price'] = $this->orderService->areaOfDelivery($totals['price'], $data['area_of_delivery_id']);
//        }

        $data = array_merge($data, $totals);

        $deliveryAddress = false;

        if (isset($data['address']) && intval($data['address'])) {
            $deliveryAddress = DeliveryAddress::find($data['address']);
            $data["delivery_address_id"] = $deliveryAddress->id;
        }

        if ($request->use_bonus && $request->user()) {
            $user = $request->user();

            $data = $this->orderService->calculateWithBonus($user, $data);

            $user->balance = 0;
            $user->save();
        }

        $order = Order::create($data);

        if ($order)
            $this->orderDetailService->detailsConstruct($basket, $order);


        $city_id = [
            "0" => '',
            "1" => ['Алматы', "ee051ca7-b2f7-11eb-810a-afad71147bc7"],
            "2" => ['Нур-Султан', "f6c22fc0-b2f7-11eb-810a-afad71147bc7"],
        ];
        $type_select = [
            "0" => '',
            "1" => ['Express', '08acb0d8-d7d6-11eb-8142-00155d01310d'],
            "2" => ['Обычный', 'eebcdbb7-d7d5-11eb-8142-00155d01310d']
        ];
        $text = '';
        $text .= "№: " . $order->id . "  ";
        $sklad_id = null;
        if (isset($data["city_id"])) {
            if (isset($city_id[$data["city_id"]])) {
                $text .= $city_id[$data["city_id"]][0];
                $sklad_id = $city_id[$data["city_id"]][1];
            }
        }

        $dealProductsRows = [];
        foreach ($basket as $basInfo) {
            $productBitrix = \App\Models\Product::find($basInfo["id"]);
            $productVariation = \App\Models\ProductVariation::find($basInfo["variation_id"]);

            if (!is_null($productBitrix) && (!is_null($productVariation) || $basInfo["variation_id"] === '-1')) {
                $set = null;

                if ($basInfo["variation_id"] === '-1') {
                    /** @var Set|null $set */
                    if (!$set = Set::find($productBitrix['set_id'])) {
                        throw new Exception('Set not found!');
                    }

                    $setProducts = $set->products()->get();
                }

                if ($set) {
                    // Set adding
                    $crestFields = [
                        'NAME' => $productBitrix->name,
                        'PRICE' => $productBitrix->price,
                        "SORT" => 500,
                        "CURRENCY_ID" => "KZT",
//                        "PROPERTY_107" => $productVariation ? $productVariation->expired_at : '', //годность
//                        "PROPERTY_109" => $productVariation ? $productVariation->name : '', //вест
                        "PROPERTY_131" => $sklad_id, //вест
//                        "PROPERTY_113" => ($productVariation && $productVariation->frozen_price ? "143" : "141"),
                    ];

                    $response = \App\Cicada\CRest::call(
                        "crm.product.add",
                        ["fields" => $crestFields]
                    );

                    $dealProductsRows[] = [
                        'PRODUCT_ID' => $response['result'],
                        'PRODUCT_NAME' => $productBitrix->name,
                        'PRICE' => $productBitrix->price,
                        'QUANTITY' => $basInfo["quantity"],
                        "TAX_RATE" => 12,
                        "TAX_INCLUDED" => "Y",
                    ];

                    foreach ($setProducts as $product) {
                        // Set's product adding
                        $crestFields = [
                            'NAME' => $product->name,
                            'PRICE' => 0,
                            "SORT" => 500,
                            "CURRENCY_ID" => "KZT",
                            'XML_ID' => $product->nomenclatura,
//                            "PROPERTY_107" => $productVariation ? $productVariation->expired_at : '', //годность
//                            "PROPERTY_109" => $productVariation ? $productVariation->name : '', //вест
                            "PROPERTY_131" => $sklad_id, //вест
//                           "PROPERTY_113" => ($productVariation && $productVariation->frozen_price ? "143" : "141"),
                        ];

                        $response = \App\Cicada\CRest::call(
                            "crm.product.add",
                            ["fields" => $crestFields]
                        );

                        $dealProductsRows[] = [
                            'PRODUCT_ID' => $response['result'],
                            'PRODUCT_NAME' => $product->name,
                            'PRICE' => 0,
                            'QUANTITY' => 1,
                            "TAX_RATE" => 12,
                            "TAX_INCLUDED" => "Y",
                        ];
                    }
                } else {
                    // Product adding
                    $response = \App\Cicada\CRest::call(
                        "crm.product.add",
                        [
                            "fields" => [
                                'NAME' => $productBitrix->name,
                                'PRICE' => $productBitrix->price,
                                "SORT" => 500,
                                "CURRENCY_ID" => "KZT",
                                'XML_ID' => $productVariation->nomenclatura,
                                "PROPERTY_107" => $productVariation->expired_at, //годность
                                "PROPERTY_109" => $productVariation ? $productVariation->name : '', //вест
                                "PROPERTY_131" => $sklad_id, //вест
                                "PROPERTY_113" => ($productVariation && $productVariation->frozen_price ? "143" : "141"),
                            ]
                        ]
                    );

                    $productId = $response['result'];

                    $productName = $productBitrix->name;

                    if ($productVariation) {
                        $productName .= ' :: ' . $productVariation->expired_at . ' - ' . $productVariation->name . " кг";
                    }

                    $price_outs = $productVariation ? $productVariation->price : $productBitrix->price;
                    $dealProductsRows[] = [
                        'PRODUCT_ID' => $productId,
                        'PRODUCT_NAME' => $productName,
                        'PRICE' => $price_outs,
                        'QUANTITY' => $basInfo["quantity"],
                        "TAX_RATE" => 12,
                        "TAX_INCLUDED" => "Y",
                    ];

                    if ($productVariation) {
                        $productVariation->in_stock = false;
                        $productVariation->save();
                    }
                }
            }
        }


        $text .= "\n\n";

        $fields = \App\Cicada\CRest::call(
            'crm.deal.userfield.list',
            ["fields" =>
                [
                    "order" => ["SORT" => "ASC"],
                    "filter" => ["MANDATORY" => "N"]
                ]
            ]
        );


        $filesIs = [];
        foreach ($fields["result"] as $field) {
            array_push($filesIs, $field["FIELD_NAME"]);
        }

        $arratb = [
            ["DATEOFDELIVERY", "Дата доставки", "", $data["date_of_delivery"]],
            ["COMMENTCUSTOM", "Комментарий", "", $data["comment"]]
        ];


        if (isset($data["delivery_type_id"])) {
            if (isset($type_select[$data["delivery_type_id"]])) {
                array_push($arratb,
                    ["DELIVERYTYPE", "Способ доставки", "", $type_select[$data["delivery_type_id"]][0]]
                );


                $pack1c = \App\Cicada\CRest::call(
                    'crm.product.list',
                    [
                        "order" => ["NAME" => "DESC"],
                        "filter" => [
                            "XML_ID" => $type_select[$data["delivery_type_id"]][1],
                        ],
                        "select" => ["ID", "NAME", "CURRENCY_ID", "PRICE"]
                    ]
                );

                $productId = 0;
                if ($pack1c["total"] == 0) {
                    $response = \App\Cicada\CRest::call(
                        "crm.product.add",
                        ["fields" =>
                            [
                                'NAME' => $type_select[$data["delivery_type_id"]][0],
                                'PRICE' => $data['delivery_price'],
                                "SORT" => 500,
                                "CURRENCY_ID" => "KZT",
                                "XML_ID" => $type_select[$data["delivery_type_id"]][1],
                            ]
                        ]
                    );
                    $productId = $response['result'];
                } else {
                    $productId = $pack1c["result"][0]["ID"];
                }

                $dealProductsRows[] = [
                    'PRODUCT_ID' => $productId,
                    'PRODUCT_NAME' => $type_select[$data["delivery_type_id"]][0],
                    'PRICE' => $data["delivery_price"],
                    'QUANTITY' => 1,
                    "TAX_RATE" => 12,
                    "TAX_INCLUDED" => "Y",
                ];
            }
        }

        if (isset($data["package_type_id"])) {
            $packageTypeName = PackageType::find($data["package_type_id"]);
            if (!is_null($packageTypeName)) {
                array_push($arratb,
                    ["PACKAGETYPEID", "Выбранная упаковка", "", $packageTypeName->name]
                );

                if ($packageTypeName->additional_price > 0) {


                    $pack1c = \App\Cicada\CRest::call(
                        'crm.product.list',
                        [
                            "order" => ["NAME" => "DESC"],
                            "filter" => [
                                "XML_ID" => $packageTypeName->numenclaturas,
                            ],
                            "select" => ["ID", "NAME", "CURRENCY_ID", "PRICE"]
                        ]
                    );

                    $productId = 0;
                    if ($pack1c["total"] == 0) {
                        $response = \App\Cicada\CRest::call(
                            "crm.product.add",
                            ["fields" =>
                                [
                                    'NAME' => $packageTypeName->name,
                                    'PRICE' => $packageTypeName->additional_price,
                                    "SORT" => 500,
                                    "CURRENCY_ID" => "KZT",
                                    "XML_ID" => $packageTypeName->numenclaturas,
                                ]
                            ]
                        );
                        $productId = $response['result'];
                    } else {
                        $productId = $pack1c["result"][0]["ID"];
                    }

                    $dealProductsRows[] = [
                        'PRODUCT_ID' => $productId,
                        'PRODUCT_NAME' => $packageTypeName->name,
                        'PRICE' => $packageTypeName->additional_price,
                        'QUANTITY' => 1,
                        "TAX_RATE" => 12,
                        "TAX_INCLUDED" => "Y",
                    ];
                }

            }
        }

        $freePackage = (new OrderService())->getFreePackage($basket);


        $paymentType = PaymentType::find($data['payment_type_id'] ?? -1);

        if ($paymentType) {
            array_push($arratb,
                ["PAYMENTTYPE", "Тип оплаты", "", $paymentType->name]
            );
        }

        if (isset($data["delivery-info"])) {
            array_push($arratb,
                ["DELIVEREINFOTYPE", "Кому доставить", "", $data["delivery-info"]]
            );
        }

        $del_price = 0;

        if (isset($data["delivery_price"])) {
            array_push($arratb,
                ["DELIVERYPRICE", "Стоимость доставки", "", $data["delivery_price"]]
            );
            $del_price = $data["delivery_price"];
        }

        if (isset($data["delivery-time"])) {
            array_push($arratb,
                ["DELIVERYTIME", "Выбранное время доставки", "", $data["delivery-time"]]
            );
        }

        if (!is_null($sklad_id)) {
            array_push($arratb,
                ["SKLADNUM", "Склад отгрузки", "", $sklad_id]
            );
        }

        if ($deliveryAddress) {
            array_push($arratb,
                ["STREET", "Улица", "", $deliveryAddress['street']]
            );

            array_push($arratb,
                ["HOME", "Дом", "", $deliveryAddress['house']]
            );

            array_push($arratb,
                ["APARTMENT", "Квартира", "", $deliveryAddress["flat"]]
            );

            array_push($arratb,
                ["ENTRANCE", "Подъезд", "", $deliveryAddress["entrance"]]
            );

            array_push($arratb,
                ["FLOOR", "Этаж", "", $deliveryAddress["floor"]]
            );
        } else {
            if (isset($data["street"])) {
                array_push($arratb,
                    ["STREET", "Улица", "", $data["street"]]
                );
            }
            if (isset($data["house"])) {
                array_push($arratb,
                    ["HOME", "Дом", "", $data["house"]]
                );
            }
            if (isset($data["apartment"])) {
                array_push($arratb,
                    ["APARTMENT", "Квартира", "", $data["apartment"]]
                );
            }
            if (isset($data["entrance"])) {
                array_push($arratb,
                    ["ENTRANCE", "Подъезд", "", $data["entrance"]]
                );
            }
            if (isset($data["floor"])) {
                array_push($arratb,
                    ["FLOOR", "Этаж", "", $data["floor"]]
                );
            }
        }


        $contactList = \App\Cicada\CRest::call(
            'crm.contact.list',
            [
                "order" => ["DATE_CREATE" => "DESC"],
                "filter" => [
                    "TYPE_ID" => "CLIENT",
                    "PHONE" => $data["phone"],
//                    "EMAIL" => $emails,
                ],
                "select" => ["ID", "NAME", "EMAIL", "PHONE", "LAST_NAME", "TYPE_ID", "SOURCE_ID"]
            ]
        );


        $contact = ["result" => ""];

        if ($contactList["total"] > 0) {
            $contact["result"] = $contactList["result"][0]["ID"];
        } else {
            $contact = \App\Cicada\CRest::call(
                'crm.contact.add',
                [
                    "fields" => [
                        "NAME" => $data["name"],
                        "SECOND_NAME" => "",
                        "LAST_NAME" => $data["last_name"],
                        "OPENED" => "Y",
                        "ASSIGNED_BY_ID" => 1,
                        "TYPE_ID" => "CLIENT",
                        "SOURCE_ID" => "SELF",
                        "EMAIL" => [["VALUE" => $emails]],
                        "PHONE" => [["VALUE" => $data["phone"], "VALUE_TYPE" => "WORK"]]
                    ]
                ]
            );
        }

        $current = date('Y-m-d H:i:s');
        $nextMonth = date('Y-m-d H:i:s');
        $filesReturn = [
            "TITLE" => $text,
            "TYPE_ID" => "GOODS",
            "STAGE_ID" => "NEW",
//            "COMPANY_ID" => 3,
            "CONTACT_ID" => $contact["result"],
            "OPENED" => "Y",
            "ASSIGNED_BY_ID" => 1,
            "PROBABILITY" => 30,
            "CURRENCY_ID" => "KZT",
            "OPPORTUNITY" => $totals['price'],
            "CATEGORY_ID" => 5,
            "BEGINDATE" => ($current),
            "CLOSEDATE" => ($nextMonth)
        ];
        foreach ($arratb as $sdsa) {
            if (!in_array($sdsa[0], $filesIs)) {
                \App\Cicada\CRest::call(
                    'crm.deal.userfield.add',
                    ["fields" =>
                        [
                            "FIELD_NAME" => $sdsa[0],
                            "EDIT_FORM_LABEL" => $sdsa[1],
                            "LIST_COLUMN_LABEL" => $sdsa[1],
                            "USER_TYPE_ID" => "string",
                            "SETTINGS" => ["DEFAULT_VALUE" => $sdsa[2]]
                        ]
                    ]
                );
                $filesReturn["UF_CRM_" . $sdsa[0]] = $sdsa[3];
            } else {
                $filesReturn["UF_CRM_" . $sdsa[0]] = $sdsa[3];
            }
        }

        if (count($dealProductsRows) > 0) {
            $addTask = \App\Cicada\CRest::call(
                'crm.deal.add',
                ["fields" =>
                    $filesReturn
                ]
            );

            \App\Cicada\CRest::call("crm.deal.productrows.set", [
                'id' => $addTask["result"],
                'rows' => $dealProductsRows,
            ]);
        }

        session()->put('basket_almaty', []);
        session()->put('basket_nur_sultan', []);
        session()->save();

        return response(['order' => $order->details], 201);
    }

    public function history(Request $request)
    {
        $orders = Order::with('details.product', 'details.productVariation')
            ->where('user_id', $request->user()->id)
            ->orderBy('id', 'desc')
            ->get();

        /** @var Order $order */
        foreach ($orders as $order) {
            $order['details'] = $order->products()->withPivot(['price', 'quantity']);
        }

        return view('cabinet.history', compact('orders'));
    }

    public function createReview(Request $request): array
    {
        $error = null;

        $validation = Validator::make(
            $request->all(),
            [
                'mark' => 'bail|required|in:1,2,3,4,5',
                'comment' => 'bail|required|string',
                'order_id' => 'bail|required|numeric'
            ],
            [
                'mark.required' => __('content.mark-required'),
                'mark.in' => __('content.mark-in'),
                'comment.required' => __('content.message-required'),
                'order_id.required' => __('content.order-id-required'),
                'order_id.numeric' => __('content.order-id-numeric')
            ]
        );

        if ($validation->fails()) {
            $error = $validation->errors()->first();
        }

        if (!$user = Auth::user()) {
            $error = __('content.user-not-auth');
        }

        /** @var Order|null $order */
        if (!$order = Order::find($request->input('order_id'))) {
            $error = __('content.order-not-found');
        }

        /** @var Quest|null $quest */
        if (!$quest = Quest::find($request->input('quest_id'))) {
            $error = __('content.quest-not-fount');
        }

        if ($error === null) {
            $order->reviews()->create([
                'user_id' => $user->id,
                'comments' => $request->input('comment'),
                'service_level' => $request->input('mark')
            ]);

            $currentUserBonuses = $user->balance ?: 0;

            $user->balance = $currentUserBonuses + 20;
            $user->save();

            DB::table('user_quest')
                ->where([
                    ['user_id', '=', $user->id],
                    ['quest_id', '=', $quest->id]
                ])
                ->update(['completed' => true]);
            return [
                'success' => true,
                'message' => __('content.review-saved-successfully')
            ];
        } else {
            return [
                'error' => true,
                'message' => $error
            ];
        }
    }
}
