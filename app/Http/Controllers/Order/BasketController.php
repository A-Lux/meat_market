<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\BasketRequest;
use App\Models\Product;
use App\Models\ProductVariation;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    /**
     * @param $id
     * @param $variation_id
     * @param int $quantity
     * @param int $all
     * @return array|false|mixed
     * @throws Exception
     */
    protected function changeBasket($id, $variation_id, $quantity = 1, $all = 0)
    {
        $variation_id_save = $variation_id;
        $currentRegion = get_current_region_1c_id();

        if ($currentRegion === config('regions_1c.almaty')) {
            $basket = session()->get('basket_almaty');
        } else if ($currentRegion === config('regions_1c.nur-sultan')) {
            $basket = session()->get('basket_nur_sultan');
        } else {
            throw new Exception('Регион не найден!');
        }
        $productMain = Product::find($id);

        $productInBasket = false;
        if (!$productMain)
            return false;

        if (!$basket) {
            $basket = [];
            $basket[] = [
                'id' => $productMain->id,
                'variation_id' => $variation_id,
                'quantity' => $quantity
            ];
        } else {
            foreach ($basket as $key => $product) {
                if ($product['id'] == $productMain->id && $product['variation_id'] == $variation_id) {

                    if ($all == 0) {
                        session()->put('basket', $basket);

                        $basket[$key]['quantity'] += $quantity;

                        if ($basket[$key]['quantity'] == 0) {
                            unset($basket[$key]);
                            break;
                        }
                    } else {
                        unset($basket[$key]);
                        break;
                    }

                    $productInBasket = true;
                    break;
                }
            }

            if ($quantity > 0 && $all != 1) {
                if ($productInBasket == false) {

                    $basket[] = [
                        'id' => $productMain->id,
                        'variation_id' => $variation_id,
                        'quantity' => 1
                    ];
                }
            }
        }

        ksort($basket);

        $variationIds = [];

        foreach ($basket as $item) {
            $variationIds[] = $item['variation_id'];
        }


        if ($quantity == 0) {
            ProductVariation::setReservedToManyDelete([$variation_id_save]);
        } else {
            ProductVariation::setReservedToMany($variationIds);
        }

        if ($currentRegion === config('regions_1c.almaty')) {
            session()->put('basket_almaty', $basket);
            session()->put('basket_almaty_expired_at', Carbon::now()->setTimezone('Asia/Almaty')->addHours(12));
        } else if ($currentRegion === config('regions_1c.nur-sultan')) {
            session()->put('basket_nur_sultan', $basket);
            session()->put('basket_nur_sultan_expired_at', Carbon::now()->setTimezone('Asia/Almaty')->addHours(12));
//            session()->put('basket_nur_sultan_expired_at', Carbon::now()->setTimezone('Asia/Almaty')->addMinutes(10));
        } else {
            throw new Exception('Регион не найден!');
        }

        session()->save();

        return $basket;
    }


    public function addProduct(BasketRequest $request)
    {
        $basket = $this->changeBasket($request->id, $request->variation);

        return response(['basket' => $basket, 'message' => 'Продукт был успешно добавлен в корзину'], 201);
    }


    public function removeProduct(BasketRequest $request)
    {

        $basket = $this->changeBasket($request->id, $request->variation, 0, 1);
        return response(['basket' => $basket], 200);
    }


    public function changeQuantityProduct(Request $request)
    {
        $basket = $this->changeBasket($request->id, $request->variation, $request->quantity);

        return response(['basket' => $basket], 200);
    }


    public function getTotals($basket)
    {
        $currentRegion = get_current_region_1c_id();

        if ($currentRegion === config('regions_1c.almaty')) {
            $basket = session()->get('basket_almaty') ?: [];
        } else if ($currentRegion === config('regions_1c.nur-sultan')) {
            $basket = session()->get('basket_nur_sultan') ?: [];
        } else {
            throw new Exception('Регион не найден!');
        }

        $totalSum = 0;
        $totalQuantity = 0;

        foreach ($basket as $key => $product) {
            $mainProduct = Product::find($product['id']);
            $variation = ProductVariation::find($product['variation_id']);

            if ($mainProduct) {
                if ($variation) {
                    $totalSum += $variation->price * $product['quantity'];
                } else {
                    $totalSum += $mainProduct->price * $product['quantity'];
                }

                $totalQuantity += $product['quantity'];
            }
        }

        return ['total_sum' => $totalSum, 'total_quantity' => $totalQuantity];
    }


    public function show()
    {
        $currentRegion = get_current_region_1c_id();

        if ($currentRegion === config('regions_1c.almaty')) {
            $basket = session()->get('basket_almaty') ?: [];
        } else if ($currentRegion === config('regions_1c.nur-sultan')) {

            $basket = session()->get('basket_nur_sultan') ?: [];
        } else {
            throw new Exception('Регион не найден!');
        }

        $mainBasket = collect([]);


        foreach ($basket as $product) {
                $mainBasket[] = [
                    'product_id' => $product['id'],
                    'product' => Product::find($product['id'])->translate(\App::getLocale()),
                    'variation_id' => ProductVariation::find($product['variation_id']),
                    'quantity' => $product['quantity']
                ];
        }

        $totals = $this->getTotals($basket);

        return response([
            'basket' => $mainBasket,
            'grouped_basket' => $mainBasket->groupBy('product_id')->values(),
            'totals' => $totals
        ]);
    }


    public function index()
    {
        return view('basket.index');
    }
}
