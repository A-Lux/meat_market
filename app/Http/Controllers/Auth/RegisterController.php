<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Refer;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */
    public function register(Request $request)
    {

        $validator = $this->validator($request->all());

        if ($validator->fails()) {

            $inputs_box = $request->all();

            $errors = [];
            $errors["model_register"] = 1;

            if (strlen($inputs_box["password"]) < 8) {
                $errors["model_message"] = "Длина пароля не может быть меньше 8 символов";
            } else {
                $errors["model_message"] = "Вы указали некорректные данные регистрации не завершена";
            }
            if($inputs_box["password"] != $inputs_box["password_confirmation"]){
                $errors["model_message"] = "Пароль не совпадает, регистрация невозможна";
            }
            $errors["message"] = 1;
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($errors);
        }


        event(new Registered($user = $this->create($request->all())));

        if (session()->has('ref')) {
            $ref_user = User::where('ref_code', session()->get('ref'))->first();

            if (is_null($ref_user)) {
                $ref_user = User::where('id', session()->get('ref'))->first();
            }

            $ref = Refer::create([
                'user_id' => $ref_user->id,
                'invited_user_id' => $user->id,
                'bonus' => setting('bonus.refer')
            ]);

            if ($ref) {
                $ref_user->update([
                    'balance' => $ref_user->balance + $ref->bonus
                ]);
            }
        }


        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }


    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'last_name' => $data['surname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'ref_code' => strtoupper($data['email'])
        ]);
    }

}
