<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class MainBanner extends Model
{
    use HasFactory, Translatable;

    public $timestamps = false;

    protected $translatable = ['title', 'description', 'button_text'];

    protected $table = 'main_banner';
}
