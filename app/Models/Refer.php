<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Refer extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'invited_user_id',
        'bonus'
    ];


    public function invited_user()
    {
        return $this->belongsTo(User::class,'invited_user_id','id');
    }
}
