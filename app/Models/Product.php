<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * @property string|null group_id
 * @property bool is_frozen
 * @property Product|null group_pair
 */
class Product extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    public $translatable = ['name', 'description', 'fattening'];

    public function variations()
    {
        return $this->hasMany(ProductVariation::class);
    }

    public function category()
    {
        return $this->belongsTo(ShopCategory::class, 'shop_category_id', 'id');
    }

    public function breed()
    {
        return $this->belongsTo(Breed::class);
    }

    public function storage()
    {
        return $this->belongsTo(StorageType::class, 'storage_type_id', 'id');
    }

    public function additional_products()
    {
        return $this->belongsToMany(Product::class, 'additional_product', 'product_id', 'additional_product_id');
    }

    public function related_products()
    {
        return $this->belongsToMany(Product::class, 'related_product', 'product_id', 'related_product_id');
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'order_details');
    }

    public function parentProducts(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'additional_product',
            'additional_product_id',
            'product_id'
        );
    }

    public function getGroupPairAttribute()
    {
        if (!$this->group_id) {
            return null;
        }

        $pairProduct = Product::where('group_id', $this->group_id)
            ->where('is_frozen', !$this->is_frozen)
            ->limit(1)->first();

        if (!$pairProduct) {
            return null;
        }

        if (count($pairProduct->variations) === 0) {
            return null;
        }

        $variations = $pairProduct
            ->variations()
            ->where('1c_region_id', get_current_region_1c_id())
            ->where('in_stock', true)
            ->where('is_reserved', false)
            ->orderBy('expired_at')
            ->get()->toArray();

        $pairProduct['variations'] = array_slice((array) $variations, 0, 3);

        return $pairProduct->translate(\App::getLocale());
    }
}
