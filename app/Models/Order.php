<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable =  [
        'name',
        'email',
        'date_of_delivery',
        'comment',
        'last_name',
        'price',
        'total_quantity',
        'delivery_price',
        'delivery_type_id',
        'package_type_id',
        'payment_type_id',
        'user_id',
        'payment_status_id',
        'delivery_status_id',
        'phone',
        'delivery_address_id',
        'street',
        'flat',
        'floor',
        'apartment',
        'house',
        'return_time'

    ];



    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->belongsTo(DeliveryAddress::class,'delivery_address_id','id');
    }

    public function delivery_status()
    {
        return $this->belongsTo(DeliveryStatus::class);
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(OrderReview::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'order_details');
    }
}
