<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQuest extends Model
{
    use HasFactory;

    protected $table = 'user_quest';

    protected $fillable = [
        'user_id',
        'quest_id',
        'completed'
    ];

    public function quest()
    {
        return $this->belongsTo(Quest::class);
    }
}
