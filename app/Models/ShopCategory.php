<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use TCG\Voyager\Traits\Translatable;

class ShopCategory extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected $translatable = ['name', 'description'];
}
