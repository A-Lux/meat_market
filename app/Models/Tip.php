<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Tip extends Model
{
    use Translatable;

    protected $translatable = [
        'name',
        'column_01_title',
        'column_01_description',
        'column_02_title',
        'column_02_description',
        'column_03_title',
        'column_03_description',
        'column_04_title',
        'column_04_description',
        'column_05_title',
        'column_05_description',
        'finally_phrase'
    ];
}
