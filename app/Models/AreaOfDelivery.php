<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AreaOfDelivery extends Model
{
    use HasFactory;

    protected $casts = [
        'area' => 'array'
    ];
}
