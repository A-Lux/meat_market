<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quest extends Model
{
    use HasFactory;

    protected $fillable = [
        'bonus',
        'title',
        'order_id',
        'quest_type_id'
    ];

    public function users()
    {
        return $this->belongsTo(User::class,'user_quest');
    }


}
