<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class Set extends Model
{
    use Translatable;
    use SoftDeletes;

    public $timestamps = false;

    protected $translatable = ['name', 'description', 'short_description', 'contains_text'];


    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'set_has_product');
    }

    public function fakeProduct(): HasOne
    {
        return $this->hasOne(Product::class, 'set_id');
    }
}
