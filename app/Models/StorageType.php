<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class StorageType extends Model
{
    use Translatable;

    public $translatable = [
        'name'
    ];
}
