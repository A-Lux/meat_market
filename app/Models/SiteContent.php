<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use TCG\Voyager\Traits\Translatable;

class SiteContent extends Model
{
    use Translatable;

    public $timestamps = false;

    protected $table = 'site_content';

    protected $translatable = ['value'];


    public static function getTrans(string $key)
    {
        if (!$row = SiteContent::where('key', $key)->get()->first()) {
            return null;
        }

        return $row->translate(App::getLocale())->value;
    }
}
