<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductVariation extends Model
{
    use HasFactory;

    public static function setReservedToMany($ids = [])
    {
        DB::table('product_variations')
            ->whereIn('id', $ids)
            ->update([
                'is_reserved' => true,
                'reservation_expired_at' => Carbon::now()->setTimezone('Asia/Almaty')->addMinutes(10)
            ]);
    }
    public static function setReservedToManyDelete($ids = [])
    {
        DB::table('product_variations')
            ->whereIn('id', $ids)
            ->update([
                'is_reserved' => false,
                'reservation_expired_at' => null
            ]);
    }
}
