<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'last_name',
        'birthday',
        'phone',
        'sex',
        'ref_code',
        'balance'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function quests()
    {
        return $this->belongsToMany(Quest::class,'user_quest');
    }

    public function addresses()
    {
        return $this->hasMany(DeliveryAddress::class)->orderBy('id','desc');
    }

    public function refers()
    {
        return $this->hasMany(Refer::class);
    }

    public function favourite()
    {
        return $this->belongsToMany(Product::class,'favorite_products');
    }


}
