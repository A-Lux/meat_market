<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountRequest extends Model
{
    public $timestamps = false;

    protected $guarded = [];
}
