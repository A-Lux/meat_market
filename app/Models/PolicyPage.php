<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class PolicyPage extends Model
{
    use Translatable;

    public $timestamps = false;
    protected $translatable = ['content'];
}
