<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Recipes extends Model
{
    use Translatable;

    protected $translatable = ['name', 'content'];
}
