<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialNet extends Model
{
    public $timestamps = false;
}
