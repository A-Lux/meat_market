<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordRecovery extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var string
     */
    private $newPassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): PasswordRecovery
    {
        return $this->view('mail.password-recovery', ['password' => $this->newPassword]);
    }
}
