<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Services\QuestService;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param \App\Models\Order $order
     * @param QuestService $questService
     * @return void
     */
    public function created(Order $order)
    {
        //
        if ($order->user){
            $questService = new QuestService();
            $questService->createReviewQuest($order,$order->user);
            $questService->generateReturnBoxRequest($order);
        }

    }

    /**
     * Handle the Order "updated" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function updated(Order $order)
    {
        //
        if ($order->box_returned == 1 && $order->return_time == null) {
            $questService = new QuestService();
            $questService->giveBonusFromReturn($order);
        }

    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
