<?php

namespace App\Observers;

use App\Models\OrderReview;
use App\Services\QuestService;

class OrderReviewObserver
{
    /**
     * Handle the OrderReview "created" event.
     *
     * @param \App\Models\OrderReview $orderReview
     * @return void
     */
    public function created(OrderReview $orderReview)
    {
        $questService = new QuestService();
        //
        $questService->checkReviewForQuest($orderReview);
    }

    /**
     * Handle the OrderReview "updated" event.
     *
     * @param  \App\Models\OrderReview  $orderReview
     * @return void
     */
    public function updated(OrderReview $orderReview)
    {
        //
    }

    /**
     * Handle the OrderReview "deleted" event.
     *
     * @param  \App\Models\OrderReview  $orderReview
     * @return void
     */
    public function deleted(OrderReview $orderReview)
    {
        //
    }

    /**
     * Handle the OrderReview "restored" event.
     *
     * @param  \App\Models\OrderReview  $orderReview
     * @return void
     */
    public function restored(OrderReview $orderReview)
    {
        //
    }

    /**
     * Handle the OrderReview "force deleted" event.
     *
     * @param  \App\Models\OrderReview  $orderReview
     * @return void
     */
    public function forceDeleted(OrderReview $orderReview)
    {
        //
    }
}
