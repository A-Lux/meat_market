<?php

return [
    'client_id' => env('C_REST_CLIENT_ID', ''),
    'client_secret' => env('C_REST_CLIENT_SECRET', '')
];