<?php

return [
    'almaty' => env('ALMATY_REGION_1C_ID'),
    'nur-sultan' => env('NUR_SULTAN_REGION_1C_ID')
];