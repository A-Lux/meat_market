<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\ProductVariation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductAddToBasketTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $product = Product::whereHas('variations')->inRandomOrder()->first();
        $response = $this->withSession([
            'basket' => [
                [
                    'id' => 21,
                    'variation_id' => 5,
                    'quantity' => 1
                ]
            ]

        ])->post('/api/basket/add', [
        'id' => $product->id,
        'variation' => ProductVariation::where('product_id', $product->id)->inRandomOrder()->first()->id
    ]);

        $response->assertStatus(201);
    }
}
