<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductRemoveFromBasketTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->withSession([
            'basket' => [
                [
                    'id' => 21,
                    'variation_id' => 5,


                ]
            ]

        ])->post('/api/basket/remove',[
            'id' => 21,
            'variation' => 5
        ]);

        $response->assertStatus(200);
    }
}
