<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChangeQuantityToBasketTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->withSession([
            'basket' => [
                [
                    'id' => 21,
                    'variation_id' => 5,
                    'quantity' => 1
                ]
            ]
        ])->post('/api/basket/change/quantity',[
            'id' => 21,
            'variation' => 5,
            'quantity' => 10
        ]);

        $response->dumpSession();
        $response->assertStatus(200);
    }
}
