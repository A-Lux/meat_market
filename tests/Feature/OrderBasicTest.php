<?php

namespace Tests\Feature;

use Carbon\Traits\Date;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderBasicTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->withSession([
            'basket' => [
                [
                    'id' => 21,
                    'variation_id' => 5,
                    'quantity' => 1
                ]
            ]
        ])->post('/api/order',[
            'name' => 'Асыл',
            'email' => 'tomboffos@gmail.com',
            'date_of_delivery' => '20.02.2020',
            'last_name' => 'Тасбулатов',
            'delivery_type_id' => 1,
            'package_type_id' => 1,
            'payment_type_id' => 1,
            'phone' => '87073039917'
        ]);

        $response->dump();
        $response->assertStatus(201);
    }
}
