const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.css('resources/css/main.css','public/css/')
    .js('resources/js/main.js', 'public/js/')
    .js('resources/js/basket/main.js','public/js/basket/')
    .js('resources/js/order/index.js','public/js/order/')
    .js('resources/js/product/main.js','public/js/product/')
    .js('resources/js/set/main.js','public/js/set/')
    .js('resources/js/user/index.js','public/js/user/')
    .js('resources/js/user/address.js','public/js/address/')
    .js('resources/js/product-mode-switching.js','public/js/product-mode-switching.js')
    .js('resources/js/tasks.js', 'public/js/tasks.js')
    .js('resources/js/add-delivery-address.js', 'public/js/add-delivery-address.js')
    .js('resources/js/order-details.js', 'public/js/order-details.js')
    .js('resources/js/discount-request.js', 'public/js/discount-request.js')
    .js('resources/js/favorite/index.js','public/js/favorite/');
