/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/config/lang.js":
/*!*************************************!*\
  !*** ./resources/js/config/lang.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  'coming-soon': {
    'ru': 'Скоро',
    'en': 'Coming soon'
  },
  'not-in-stock': {
    'ru': 'Нет в наличии',
    'en': 'Not in stock'
  },
  'success': {
    'ru': 'Успех',
    'en': 'Success'
  },
  'error-happen': {
    'ru': 'Произошла ошибка',
    'en': 'Error happen'
  },
  'product-adding-to-cart-success': {
    'ru': 'Товар добавлен в корзину',
    'en': 'Product was added to cart'
  },
  'add-to-cart': {
    'ru': 'В корзину',
    'en': 'Add to cart'
  },
  'total': {
    'ru': 'Всего',
    'en': 'Total'
  },
  'cost': {
    'ru': 'Стоимость',
    'en': 'Cost'
  },
  'order-more': {
    'ru': 'Заказать ещё',
    'en': 'Order more'
  },
  'bonus': {
    'ru': 'Бонус',
    'en': 'Bonus'
  },
  'weight': {
    'ru': 'Вес',
    'en': 'Weight'
  },
  'kg': {
    'ru': 'кг',
    'en': 'kg'
  },
  'products': {
    'ru': 'Товаров',
    'en': 'Products'
  },
  'main-address': {
    'ru': 'Основной адрес',
    'en': 'Main address'
  },
  'searching': {
    'ru': 'Идёт поиск',
    'en': 'Searching'
  },
  'invalid-address': {
    'ru': 'Некорректный адрес',
    'en': 'Invalid address'
  },
  'not-specified-street': {
    'ru': 'Не указана улица!',
    'en': 'Not specified street!'
  },
  'not-specified-house-number': {
    'ru': 'Не указан номер дома!',
    'en': 'Not specified house number!'
  },
  'address-adding-failed': {
    'ru': 'Не удалось создать адрес!',
    'en': 'Adding of address was failed!'
  },
  'address-adding-success': {
    'ru': 'Адрес успешно создан!',
    'en': 'Address was created successfully!'
  },
  'address-is-not-included-to-delivery-zone': {
    'ru': 'Выбранный адрес не входит в зону доставки! Выберите другой адрес!',
    'en': 'Chosen address is not included to delivery zone! Choose another address!'
  },
  'must-specify-address': {
    'ru': 'Необходимо указать адрес доставки!',
    'en': 'Must specify delivery address!'
  },
  'in-developing': {
    'ru': 'Сайт в разработке',
    'en': 'Site in the developing'
  },
  'in-developing-message': {
    'ru': '<p style="margin-top: 15px">Уважаемые клиенты, обновленный сайт сейчас находится в тестовом ' + 'режиме и <b style="font-size: 18px; color: #B41F21">вы можете заказывать продукцию</b> как и раньше предварительно ' + 'согласовав все с кол центром, во избежания ошибок при заказе. В ближайшее ' + 'время сайт будет доступен с полной автоматизацией заказа, что бы сэкономить ' + 'ваше время. С уважением команда meatmarket.kz</p>',
    'en': '<p style="margin-top: 15px">Dear customers, the updated website is now in test mode and <b style="font-size: 18px; color: #B41F21">you can order products</b> ' + 'as before, having previously agreed on everything with the call center, in order ' + 'to avoid mistakes when ordering. In the near future, the site will be available with ' + 'full order automation to save your time. Best regards meatmarket.kz team</p>'
  },
  'address-required': {
    'ru': 'Укажите адрес!',
    'en': 'Must specify address!'
  },
  'delivery-type-required': {
    'ru': 'Выберите тип доставки!',
    'en': 'Choose delivery type!'
  },
  'package-type-required': {
    'ru': 'Выберите тип упаковки!',
    'en': 'Choose package type!'
  },
  'delivery-person-required': {
    'ru': 'Выберите для кого доставка!',
    'en': 'Choose person for delivering!'
  },
  'delivery-time-required': {
    'ru': 'Выберите время доставки!',
    'en': 'Choose delivery time!'
  },
  'first-name-required': {
    'ru': 'Укажите имя!',
    'en': 'Specify first name!'
  },
  'last-name-required': {
    'ru': 'Укажите фамилию!',
    'en': 'Specify last name!'
  },
  'email-required': {
    'ru': 'Укажите E-mail!',
    'en': 'Specify E-mail!'
  },
  'phone-required': {
    'ru': 'Укажите номер телефона!',
    'en': 'Specify phone number!'
  },
  'delivery-date-required': {
    'ru': 'Выберите дату доставки!',
    'en': 'Specify delivery date!'
  }
});

/***/ }),

/***/ "./resources/js/helpers/Lang.js":
/*!**************************************!*\
  !*** ./resources/js/helpers/Lang.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _config_lang__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../config/lang */ "./resources/js/config/lang.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var Lang = /*#__PURE__*/function () {
  function Lang() {
    _classCallCheck(this, Lang);
  }

  _createClass(Lang, null, [{
    key: "getLocale",
    value: function getLocale() {
      return document.documentElement.getAttribute('lang');
    }
  }, {
    key: "translate",
    value: function translate(key) {
      if (!_config_lang__WEBPACK_IMPORTED_MODULE_0__.default[key]) {
        return null;
      }

      if (!_config_lang__WEBPACK_IMPORTED_MODULE_0__.default[key][Lang.getLocale()]) {
        return null;
      }

      return _config_lang__WEBPACK_IMPORTED_MODULE_0__.default[key][Lang.getLocale()];
    }
  }]);

  return Lang;
}();

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Lang);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!***************************************!*\
  !*** ./resources/js/order-details.js ***!
  \***************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_Lang__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers/Lang */ "./resources/js/helpers/Lang.js");

document.addEventListener('DOMContentLoaded', function (_) {
  var orderHistoryItems = document.querySelectorAll('.history__content');
  var orderDetailsPopup = document.querySelector('.modal__order-details');
  orderHistoryItems.forEach(function (item) {
    var orderData = JSON.parse(item.getAttribute('data-order-data'));
    item.addEventListener('click', function (_) {
      orderDetailsPopup.classList.remove('hide');
      renderDetailsToPopup(orderDetailsPopup, orderData);
    });
  });
});

var renderDetailsToPopup = function renderDetailsToPopup(popup, data) {
  popup.querySelectorAll('.order-details__total-price-wrapper span')[1].innerText = "".concat(data.price, " \u20B8");
  var details = data['details'];

  if (details) {
    var container = popup.querySelector('.order-details__items');
    container.innerHTML = '';
    details.forEach(function (item, index) {
      var productData = item['product'];
      var variationData = item['product_variation'];
      container.innerHTML += "\n                <div class=\"order-details__item\">\n                    <p class=\"order-details__product-name\">".concat(++index, ". ").concat(productData['name'], " \u2013 ").concat(variationData['name'], " ").concat(_helpers_Lang__WEBPACK_IMPORTED_MODULE_0__.default.translate('kg'), " \u0445 ").concat(item['quantity'], "</p>\n                </div>\n            ");
    });
  } else {
    console.warn('Order\'s details are not defined!');
  }
};
})();

/******/ })()
;