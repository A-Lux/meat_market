<?php

namespace Database\Factories;

use App\Models\StorageType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class StorageTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StorageType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->word
        ];
    }
}
