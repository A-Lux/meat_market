<?php

namespace Database\Factories;


use App\Models\Product;
use App\Models\ProductVariation;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductVariationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductVariation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->title,
            'price' => rand(1000,5000),
            'in_stock' => $this->faker->boolean,
            'product_id' => Product::all()->random()->id
        ];
    }
}
