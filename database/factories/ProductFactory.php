<?php

namespace Database\Factories;

use App\Models\Breed;
use App\Models\Product;
use App\Models\ShopCategory;
use App\Models\StorageType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'shop_category_id' => ShopCategory::all()->random()->id,
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'fattening' => $this->faker->name,
            'action_price' => rand(1000,100000),
            'price' => rand(10000,100000),
            'storage_type_id' => StorageType::all()->random()->id,
            'breed_id' => Breed::all()->random()->id,
        ];
    }
}
