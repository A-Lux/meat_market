<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliveryStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('delivery_statuses')->insert([
           'name' => 'Новый заказ'
        ]);

        DB::table('delivery_statuses')->insert([
            'name' => 'Заказ закончен'
        ]);
    }
}
