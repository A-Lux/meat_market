<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliveryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('delivery_types')->insert([
            'name' => 'Express',
            'is_fast' => 1
        ]);
        DB::table('delivery_types')->insert([
            'name' => 'Обычный',
            'is_fast' => 0
        ]);
    }
}
