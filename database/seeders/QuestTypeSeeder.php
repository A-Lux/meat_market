<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('quest_types')->insert([
            'name' => 'Share'
        ]);

        DB::table('quest_types')->insert([
            'name' => 'Box_return'
        ]);

        DB::table('quest_types')->insert([
            'name' => 'Give_review'
        ]);
    }
}
