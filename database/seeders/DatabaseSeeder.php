<?php

namespace Database\Seeders;

use App\Models\Breed;
use App\Models\City;
use App\Models\PackageType;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\ShopCategory;
use App\Models\StorageType;
use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class DatabaseSeeder extends Seeder
{
    use Seedable;


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ShopCategory::factory(10)->create();
        Breed::factory(10)->create();
        StorageType::factory(10)->create();
        Product::factory(20)->create();
        ProductVariation::factory(30)->create();

        $this->call(DeliveryTypeSeeder::class);
        $this->call(DeliveryStatusSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(PaymentStatusSeeder::class);
        $this->call(PackageTypeSeeder::class);
        $this->call(QuestTypeSeeder::class);
        $this->call(CitySeeder::class);

//        // \App\Models\User::factory(10)->create();
    }
}
