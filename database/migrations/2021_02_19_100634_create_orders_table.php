<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->date('date_of_delivery');
            $table->text('comment')->nullable();
            $table->string('last_name');
            $table->integer('price');
            $table->integer('total_quantity');
            $table->integer('delivery_price');
            $table->foreignId('user_id')->nullable()->constrained();
            $table->foreignId('delivery_type_id')->constrained();
            $table->foreignId('package_type_id')->constrained();
            $table->foreignId('delivery_status_id')->constrained();
            $table->foreignId('payment_type_id')->constrained();
            $table->foreignId('payment_status_id')->constrained();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
