<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_variations', function (Blueprint $table) {
//            $table->renameColumn('price', 'frozen_price');
            $table->integer('price')->nullable()->change();
//            $table->integer('cooled_price')->nullable()->after('frozen_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_variations', function (Blueprint $table) {
            $table->renameColumn('frozen_price', 'price');
            $table->integer('frozen_price')->change();
            $table->dropColumn('cooled_price');
        });
    }
}
