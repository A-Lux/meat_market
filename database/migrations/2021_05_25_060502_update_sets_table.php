<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sets', function (Blueprint $table) {
            \Illuminate\Support\Facades\DB::table('sets')->delete();

            $table->dropForeign('sets_product_id_foreign');
            $table->dropColumn('product_id');
            $table->dropForeign('sets_set_component_product_id_foreign');
            $table->dropColumn('set_component_product_id');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');

            $table->id();
            $table->string('name');
            $table->string('description', 2048)->nullable();
            $table->integer('price')->unsigned();
            $table->integer('person_numbers');
            $table->string('image');
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sets', function (Blueprint $table) {
            $table->foreignId('product_id')->constrained();
            $table->foreignId('set_component_product_id')->constrained('products');
            $table->timestamps();

            $table->dropColumn('id');
            $table->dropColumn('name');
            $table->dropColumn('description');
            $table->dropColumn('price');
            $table->dropColumn('person_numbers');
            $table->dropColumn('image');
            $table->dropColumn('deleted_at');
        });
    }
}
