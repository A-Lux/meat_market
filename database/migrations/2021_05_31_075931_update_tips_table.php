<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tips', function (Blueprint $table) {
            $table->dropColumn('content');
            $table->dropColumn('ingredients');
            $table->dropColumn('images');

            $table->renameColumn('image', 'preview_image');
            $table->string('first_bg_image')->nullable();
            $table->string('second_bg_image')->nullable();
            $table->string('column_01_title')->nullable();
            $table->text('column_01_description')->nullable();
            $table->string('column_02_title')->nullable();
            $table->text('column_02_description')->nullable();
            $table->string('column_03_title')->nullable();
            $table->text('column_03_description')->nullable();
            $table->string('column_04_title')->nullable();
            $table->text('column_04_description')->nullable();
            $table->string('column_05_title')->nullable();
            $table->text('column_05_description')->nullable();
            $table->string('finally_phrase')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tips', function (Blueprint $table) {
            $table->text('ingredients')->nullable();
            $table->text('images')->nullable();
            $table->text('content')->nullable();

            $table->renameColumn('preview_image', 'image');
            $table->dropColumn('first_bg_image');
            $table->dropColumn('second_bg_image');
            $table->dropColumn('column_01_title');
            $table->dropColumn('column_01_description');
            $table->dropColumn('column_02_title');
            $table->dropColumn('column_02_description');
            $table->dropColumn('column_03_title');
            $table->dropColumn('column_03_description');
            $table->dropColumn('column_04_title');
            $table->dropColumn('column_04_description');
            $table->dropColumn('column_05_title');
            $table->dropColumn('column_05_description');
            $table->dropColumn('finally_phrase');
        });
    }
}
