<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCowPartHasProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cow_part_has_product', function (Blueprint $table) {
            $table->bigInteger('cow_part_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();


            $table->foreign('cow_part_id')
                ->references('id')
                ->on('cow_parts');

            $table->foreign('product_id')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cow_part_has_product');
    }
}
