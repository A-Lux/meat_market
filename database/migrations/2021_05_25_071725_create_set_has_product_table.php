<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetHasProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('set_has_product', function (Blueprint $table) {
            $table->bigInteger('set_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();


            $table->foreign('set_id')
                ->references('id')
                ->on('sets');

            $table->foreign('product_id')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('set_has_product');
    }
}
